{
    "id": "4e76d02d-76a5-4f3f-9703-61c1988f832d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonSiguienteDia",
    "eventList": [
        {
            "id": "5304b417-963a-4e6f-a0aa-69985bae2ae0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4e76d02d-76a5-4f3f-9703-61c1988f832d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8331f90d-09ad-4222-a768-23ec9acde2d0",
    "visible": false
}