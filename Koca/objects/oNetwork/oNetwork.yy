{
    "id": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNetwork",
    "eventList": [
        {
            "id": "5f59e85d-1290-45bf-8861-5fc891f04083",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "d1461fdc-d0e8-441a-b688-03ee1fb8ccd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "9d27a350-e657-442b-bbf1-b1a5ce2197c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "de906a8a-2f11-4ce5-9651-da287f4fe433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "793ce4c9-08d0-4aa8-8fd6-590d80396ce5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "7bcb17f2-151e-496d-a408-9b84f3cdfb52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "57983d16-9a66-41b9-8c5a-1492069cf7a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "5873b650-e025-44e1-b059-943ce0a48743",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        },
        {
            "id": "7d54de36-0d8e-43f0-9017-3229d6b569b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d25253c6-a0c9-4cb6-bd09-5ec004ff6d77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": true
}