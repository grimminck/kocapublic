// Delete buffer
buffer_delete(buff)

// Delete ticket lists
ds_list_destroy_and_free(ticketsSBAvailable)
ds_list_destroy_and_free(ticketsFasaAvailable)
ds_list_destroy_and_free(ticketsCVAvailable)
ds_list_destroy_and_free(ticketsSBUnavailable)
ds_list_destroy_and_free(ticketsFasaUnavailable)
ds_list_destroy_and_free(ticketsCVUnavailable)

if server()
{
	// Delete save structures
	ds_map_destroy(serverSaveMap)
	ds_map_destroy(serverAccountsRegistered)
	ds_map_destroy(ticketsSBAvailable)
	ds_map_destroy(ticketsFasaAvailable)
	ds_map_destroy(ticketsCVAvailable)
	
	ds_list_destroy_and_free(socketsSB)
	ds_list_destroy_and_free(rutsClientesSB)
	ds_map_destroy(mapSocketClienteSB)
	
	ds_list_destroy_and_free(socketsFasa)
	ds_list_destroy_and_free(rutsClientesFasa)
	ds_map_destroy(mapSocketClienteFasa)
	
	ds_list_destroy_and_free(socketsCV)
	ds_list_destroy_and_free(rutsClientesCV)
	ds_map_destroy(mapSocketClienteCV)
}
else
{
	// Delete save map
	ds_map_destroy(clientSaveMap)
	
	// Delete ping buffer
	buffer_delete(pingBuffer)
}