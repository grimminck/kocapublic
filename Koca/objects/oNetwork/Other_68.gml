/// @description  Take care for: Data Transmission, Connection, Disconnection or Trying to Connect
var type_event = async_load[? "type"]

switch(type_event)
{       
	// Client is trying to connect
    case network_type_non_blocking_connect:
		// Get succeded variable
		var succeeded = async_load[? "succeeded"]
			
		// Use succeded variable
		if succeeded == 0 {
			msg("Connection to Server failed")
			clientConnected = false
			alarm[2]=room_speed*6 }
		else { 
			msg("Connection to Server Successful")
			clientConnected = true
			if scr_client_load_data_general("registered") scr_transmission_login(false, rut, clave)
			alarm[0] = room_speed*2	// Reset ping alarm
			alarm[1] = -1			// Cancel disconnected alarm
			waitingForPing = false
			disconnected = false
			it_was_connected = true }
			
		losingConnection = false
		clientConnecting = false
		
		break
		
    // Lost connection
    case network_type_disconnect: 
	    var socket = async_load[? "socket"]
		// search for client in maps and lists with socket and delete it
		scr_remove_client_from_structs(socket)
		break   
	
	// Data Incoming
    case network_type_data:
        // Handle the data
        var buffer = async_load[? "buffer"]
        var socket = async_load[? "id"]
        buffer_seek(buffer, buffer_seek_start, 0) // nos aseguramos que el buffer se lea desde el principio (tal como poner el pointer de una lista)
        scr_handle_packet(buffer, socket) // let the script "scr_handle_packet" handle it
        break
}
