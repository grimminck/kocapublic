var myPublicIp = "200.104.94.141"
//var pabloPublicIp = "190.162.28.205"
var localHostIp = "127.0.0.1"
var port = 8001
var ip = myPublicIp

// Create Server
if server() scr_initialize_server(port)

// Or connect client
else scr_initialize_client(port, ip)

// Create sending buffer
globalvar buff; buff = buffer_create(1024, buffer_grow, 1)

depth = -100