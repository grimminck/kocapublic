if client() && room!=rmIntro
{
	var xx = room_width - 167
	var yy = 35
	// Draw connected (green) circle
	if clientConnected draw_sprite(sConnected, 1, xx, yy)
	else 
	{
		// Draw re-connecting (green and orange) circle
		if !disconnected && losingConnection {
			if timer>15 draw_sprite(sConnected, 1, xx, yy)
			else draw_sprite(sConnected, 2, xx, yy)
		}
		
		// Draw unconnected (red) circle
		else draw_sprite(sConnected, 0, xx, yy)
	}
}