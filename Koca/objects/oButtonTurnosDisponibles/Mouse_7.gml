event_inherited()
if !position_meeting(mouse_x, mouse_y, oButtonPayTurnAlert) and
	!position_meeting(mouse_x, mouse_y, oPanel1Ofrecidos) and
	!position_meeting(mouse_x, mouse_y, oPanel2) and
	!position_meeting(mouse_x, mouse_y, oPanel3) and
	!position_meeting(mouse_x, mouse_y, oPanel4)
{
	ofreciendoTurnos = false
	if oTurnosDisponibles.number > 0
		oPanel5.locked = true
}