{
    "id": "1df6ad4d-a5f3-460d-8061-6884cba666bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonTurnosDisponibles",
    "eventList": [
        {
            "id": "35baa3ed-9ad9-487a-b557-efdd5ffd2b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "1df6ad4d-a5f3-460d-8061-6884cba666bb"
        },
        {
            "id": "75ed5680-7a3e-4ef0-bddf-9428feff6041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1df6ad4d-a5f3-460d-8061-6884cba666bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6103a9cd-6b83-4dee-9014-3c895e2511a2",
    "visible": true
}