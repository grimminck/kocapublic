{
    "id": "72db9872-1f05-4e2b-8211-f1e4c3fd9a31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAnnoDown",
    "eventList": [
        {
            "id": "44fae6fa-ab8d-4f19-9d23-42743ef8eaec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "72db9872-1f05-4e2b-8211-f1e4c3fd9a31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df4a5dd9-02d1-4aeb-9579-90f1bedb5c53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
    "visible": true
}