{
    "id": "dec2a50e-5d24-4ebf-96b2-549e91dec19c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRueda",
    "eventList": [
        {
            "id": "d4475a65-272b-4943-bf74-5697757bfcc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dec2a50e-5d24-4ebf-96b2-549e91dec19c"
        },
        {
            "id": "e6d90c1e-e2cd-46e3-9d35-3495925eb8f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dec2a50e-5d24-4ebf-96b2-549e91dec19c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d486b09c-193f-4127-8ea9-3a92859541e6",
    "visible": true
}