{
    "id": "2cc7649b-f6d8-418d-89db-d4318e98218d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinutoFinalNumberFront",
    "eventList": [
        {
            "id": "404b9dca-86a5-47c6-b358-a0e491d9d51d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2cc7649b-f6d8-418d-89db-d4318e98218d"
        },
        {
            "id": "11c73159-5ce4-4e07-b63b-c9b623c94fe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2cc7649b-f6d8-418d-89db-d4318e98218d"
        },
        {
            "id": "0d0b956c-5c17-4783-9366-47c3631d6352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2cc7649b-f6d8-418d-89db-d4318e98218d"
        },
        {
            "id": "51d89e6a-dbbc-45c8-887f-47887dfaac6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2cc7649b-f6d8-418d-89db-d4318e98218d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}