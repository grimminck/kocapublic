// Initialize systems
scr_initialize_global_variables()
scr_initialize_general_saving_system()
scr_initialize_screen()
enum farmacias
{
	sb,
	fasa,
	cv,
	total
}
instance_create_depth(x, y, depth, oNetwork)
instance_create_depth(x, y, depth, oUserController)

gpu_set_tex_filter(true)

msg("Last update on memory: "+gLastUpdate)