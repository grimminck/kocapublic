{
    "id": "bbb4b021-9603-405a-855e-8ca24475cc4e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGeneralInit",
    "eventList": [
        {
            "id": "d3598d52-ad1f-473c-90b2-a341c120c8c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbb4b021-9603-405a-855e-8ca24475cc4e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": false
}