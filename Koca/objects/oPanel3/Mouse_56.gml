var downThreshold = room_height - 100
if (mouseActivated and mouseLastY > mouse_y) locked = true
else
{
	if ((mouse_y < downThreshold) and (y < room_height) and !hover and mouseActivated) locked = true
	else 
	{
		if (mouseLastY < mouse_y) locked = false
	}

	if hover
	{
		if (mouseLastY < mouse_y or mouse_y > downThreshold) locked = false	
		else locked = true
	}
}

mouseActivated = false