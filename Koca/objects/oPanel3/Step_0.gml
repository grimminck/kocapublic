var upThreshold = room_height-sprite_height
if !locked
{
	if mouseActivated
	{
		scr_move_panel_3(clamp(lastPosition - mouseLastPressedY + mouse_y, upThreshold, room_height))
	}
	else
	{
		if (y < room_height)
			scr_move_panel_3(y + 25)
	}
}
else
{
	if (y > upThreshold) scr_move_panel_3(y - 25) 
	scr_move_panel_3(max(y, upThreshold))
}