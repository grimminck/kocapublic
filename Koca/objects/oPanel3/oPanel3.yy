{
    "id": "97277c02-2360-4d92-a66a-c0328a799c18",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel3",
    "eventList": [
        {
            "id": "1b51574d-2dc8-4e6c-8d50-f93803c4498b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        },
        {
            "id": "5166a121-3218-4f85-bbbc-323c4ebd56ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        },
        {
            "id": "2a5d9bfd-0775-4274-b3dc-f52cd162bb0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        },
        {
            "id": "7443f578-f1c8-498e-8dc9-f8980d5c2515",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        },
        {
            "id": "788c2e11-7c4e-4fdb-bcfa-7ba6c6f61a20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        },
        {
            "id": "b746380d-67b1-46ca-a7cd-79e102b577e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "97277c02-2360-4d92-a66a-c0328a799c18"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a84cab97-eb91-4959-b9e7-cd4cc9336eae",
    "visible": true
}