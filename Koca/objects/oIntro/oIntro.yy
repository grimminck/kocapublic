{
    "id": "b980e085-65aa-4c81-9c5d-0646986bea66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oIntro",
    "eventList": [
        {
            "id": "359ffc98-0bb4-46f5-901a-1e67a22268ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b980e085-65aa-4c81-9c5d-0646986bea66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": false
}