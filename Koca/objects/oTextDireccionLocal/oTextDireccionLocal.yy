{
    "id": "c8049d8b-adcc-4cf5-b438-5c8caa675f3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextDireccionLocal",
    "eventList": [
        {
            "id": "7927bf05-79e8-43c8-905f-8d8bfd118693",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8049d8b-adcc-4cf5-b438-5c8caa675f3c"
        },
        {
            "id": "54baf896-d70e-405a-a092-f505a16416cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8049d8b-adcc-4cf5-b438-5c8caa675f3c"
        },
        {
            "id": "92b26d5d-386c-453b-b223-d245bb9de46e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "c8049d8b-adcc-4cf5-b438-5c8caa675f3c"
        },
        {
            "id": "6b1fd664-7c80-4338-a976-85db719b7f82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c8049d8b-adcc-4cf5-b438-5c8caa675f3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9c06f062-07bd-41d1-883e-533175443659",
    "visible": true
}