{
    "id": "426d9b9d-56c3-415d-839e-72078db8393c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDiaDown",
    "eventList": [
        {
            "id": "f1268ef4-5ef7-4a30-b441-df1867fa34d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "426d9b9d-56c3-415d-839e-72078db8393c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df4a5dd9-02d1-4aeb-9579-90f1bedb5c53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
    "visible": true
}