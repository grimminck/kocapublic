/// @description Panel de turnos disponibles para pedir
posY = 0

mouseActivated = false
locked = false
hover = false
lastPosition = 0

depth = -200

var listSize = ds_list_size(myList)
msg("Mostrando tickets, cantidad en la lista: "+string(listSize))
var k = 0
for (var i = 0; i<listSize; i++)
{
	var ticket = myList[| i]
	// Compara string con integer y funciona!
	if ticket[? "rut"] != miRut
	{
		scr_create_ticket_panel_3(ticket, id)
		k++
	}
}