{
    "id": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel5",
    "eventList": [
        {
            "id": "a763c264-4f6f-49c4-8c1f-6dda16691b73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        },
        {
            "id": "ab3648a4-fd20-4b13-a939-526a2020df55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        },
        {
            "id": "7ef75bf6-7be4-4806-a9a6-9489f5527343",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        },
        {
            "id": "c9820cf1-3684-44f8-a1d1-2fee032555cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        },
        {
            "id": "2b97de33-1528-4e9c-a39f-7bc7c3c8489d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        },
        {
            "id": "2aecfadf-6113-4168-aa5e-c1922fb8f10b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "bc6ae9b3-c872-4dcd-b16a-0b5b471844d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7805e54-6715-49f2-bb85-d33957befa7f",
    "visible": true
}