{
    "id": "721934cc-5b6d-4e1e-8ec7-f1110d2359f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHoraInicialNumberFront",
    "eventList": [
        {
            "id": "72dcaf29-aaf9-488e-9a23-044fb5eba811",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "721934cc-5b6d-4e1e-8ec7-f1110d2359f3"
        },
        {
            "id": "14b078fa-e93c-4216-ab7b-46bc3be1fba8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "721934cc-5b6d-4e1e-8ec7-f1110d2359f3"
        },
        {
            "id": "2105f2f2-33ea-425d-a3e1-96e9b13e7ddd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "721934cc-5b6d-4e1e-8ec7-f1110d2359f3"
        },
        {
            "id": "a579cb7b-207c-40c3-b9da-33fd3b70a5ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "721934cc-5b6d-4e1e-8ec7-f1110d2359f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}