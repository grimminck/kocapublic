{
    "id": "21533a95-ac79-4805-812b-b3cf6fab3544",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonDiaFrontAccept",
    "eventList": [
        {
            "id": "b77c9885-e6e5-479f-82f5-8d067b32d6c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "21533a95-ac79-4805-812b-b3cf6fab3544"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df4a5dd9-02d1-4aeb-9579-90f1bedb5c53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8b12165-888c-4719-9eeb-3933675dc032",
    "visible": true
}