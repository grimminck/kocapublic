{
    "id": "3b8f9c26-3b0f-452b-9681-266c0d9096a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMesDown",
    "eventList": [
        {
            "id": "bd1dd801-3df9-478a-8866-a28616b712a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3b8f9c26-3b0f-452b-9681-266c0d9096a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df4a5dd9-02d1-4aeb-9579-90f1bedb5c53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
    "visible": true
}