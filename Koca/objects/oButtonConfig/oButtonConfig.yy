{
    "id": "de4a15de-dd41-41cc-b919-41511154e211",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonConfig",
    "eventList": [
        {
            "id": "996fba90-6df8-41ae-b4a8-86c8c0e89ac4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de4a15de-dd41-41cc-b919-41511154e211"
        },
        {
            "id": "183af593-b1fd-4835-83d1-11ef83573bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "de4a15de-dd41-41cc-b919-41511154e211"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87bc8354-eb26-4208-b849-f7b9c8fdc6cc",
    "visible": true
}