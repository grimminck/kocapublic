{
    "id": "d26dff5b-e2bf-4071-ad9d-99a45de0325d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlackScreen",
    "eventList": [
        {
            "id": "02eedecb-2792-435d-85d4-829447a55512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d26dff5b-e2bf-4071-ad9d-99a45de0325d"
        },
        {
            "id": "dfc75c8e-2ba7-4d34-b6fc-de690cd4c277",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d26dff5b-e2bf-4071-ad9d-99a45de0325d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "754ba542-9440-48d8-8c46-b1bf2c1e2b26",
    "visible": true
}