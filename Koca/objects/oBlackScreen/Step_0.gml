timer++

var maxTimer = 240
var alpha = 0.03
if room!=rmIntro alpha = 0.07

if timer >= 120 && timer < maxTimer image_alpha += alpha
if timer >= 245 {
	image_alpha -= alpha
	if image_alpha <= 0 instance_destroy()
}

if image_alpha >= 1 and timer < maxTimer+1
{
	timer = maxTimer
	room=destinationRoom
}