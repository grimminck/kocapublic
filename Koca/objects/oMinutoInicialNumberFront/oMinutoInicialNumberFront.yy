{
    "id": "acf71006-36a9-455b-840a-edb04b91eeae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinutoInicialNumberFront",
    "eventList": [
        {
            "id": "29b8f1f7-9c89-40a8-abf4-e480ccfcc0a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "acf71006-36a9-455b-840a-edb04b91eeae"
        },
        {
            "id": "9ca21445-c36c-4f44-aff0-6b452b00e9d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "acf71006-36a9-455b-840a-edb04b91eeae"
        },
        {
            "id": "c0b50f88-cedb-4245-a5b8-ad4d7a0b0e2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "acf71006-36a9-455b-840a-edb04b91eeae"
        },
        {
            "id": "5959cad7-7f1c-49de-9a8e-8f9ef576296a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "acf71006-36a9-455b-840a-edb04b91eeae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}