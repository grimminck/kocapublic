{
    "id": "a22c8a91-05d6-49a1-8dd0-a03325765ccb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonHorarioFront",
    "eventList": [
        {
            "id": "a6bd4018-67cd-46f1-b8f4-a2467afe341c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "fccd6783-168f-41c1-b63a-884ed22d0332",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "c86a6b6b-b06e-458e-bf9b-d90ba760efad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "435794c3-2bb0-4451-83a6-c59825339ea8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "999bab19-3c9b-4a95-8d24-abacf486b755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "690ce28a-5a3e-44b2-bac9-34f41f199e6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        },
        {
            "id": "44ae5d08-cbb8-4bdf-b889-872750f1caae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a22c8a91-05d6-49a1-8dd0-a03325765ccb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef08d6ff-8765-4c7d-8fc0-a0846343a637",
    "visible": true
}