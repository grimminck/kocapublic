timeAlive++

var al = 0.04
if !destroying
{
	whiteSquareAlpha = min(whiteSquareAlpha+al/1.5, 0.65)
	image_alpha = min(image_alpha+al, 1)
}
else
{
	whiteSquareAlpha = max(whiteSquareAlpha-al/1.5, 0)
	image_alpha-=al
	if image_alpha<=0 and timeAlive>30 
	{
		timeAlive = 0
		instance_deactivate_object(id)
	}
}