{
    "id": "60b704aa-c4e9-42d3-8fa2-4a2d8a2818b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonConfirmTurnRequest",
    "eventList": [
        {
            "id": "07d82444-d63b-44c1-878c-af8ac879b9c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "60b704aa-c4e9-42d3-8fa2-4a2d8a2818b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b52479a7-710a-43bc-84a9-8388d9e2581f",
    "visible": true
}