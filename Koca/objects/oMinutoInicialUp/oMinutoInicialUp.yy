{
    "id": "6868d0d1-0f86-4383-aeeb-0b072f2709de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinutoInicialUp",
    "eventList": [
        {
            "id": "f64f35dd-ed4a-4d53-b7e6-dece1dbb060b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6868d0d1-0f86-4383-aeeb-0b072f2709de"
        },
        {
            "id": "3e8893e3-9cdf-46aa-bced-e31635240696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6868d0d1-0f86-4383-aeeb-0b072f2709de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94c44984-5c38-4f65-9231-c901474a8733",
    "visible": true
}