event_inherited()

if !frontPanelActive
{
	activated = true

	// Deactivate all other buttons
	var _id = id
	with oButtonInput if (_id != id) activated = false
}