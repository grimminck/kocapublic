{
    "id": "73ae1f31-a21f-4ab8-93e1-d1338cf033de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonInput",
    "eventList": [
        {
            "id": "e7b5ee26-1eb8-49dc-bb57-432b0d1f7c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "73ae1f31-a21f-4ab8-93e1-d1338cf033de"
        },
        {
            "id": "1be36e81-f325-42b2-9ad0-9d5ede08dee2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "73ae1f31-a21f-4ab8-93e1-d1338cf033de"
        },
        {
            "id": "ff982e27-e00b-4249-98db-2a86c4f48ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73ae1f31-a21f-4ab8-93e1-d1338cf033de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": true
}