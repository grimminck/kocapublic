with obhf event_user(1)

// Inicial
with ohiu event_user(0)
with ohid event_user(0)
with ohinf event_user(0)
with omid event_user(0)
with omiu event_user(0)
with ominf event_user(0)
with ohiAMPMf event_user(0)

// Final
with ohfu event_user(0)
with ohfd event_user(0)
with ohfnf event_user(0)
with omfd event_user(0)
with omfu event_user(0)
with omfnf event_user(0)
with ohfAMPMf event_user(0)

// Confirm button
with obhaf event_user(0)