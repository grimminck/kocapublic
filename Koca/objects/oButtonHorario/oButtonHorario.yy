{
    "id": "0a5b251d-1b2b-4702-8e2f-b2542f023fde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonHorario",
    "eventList": [
        {
            "id": "df50d0e7-e27a-4bcb-aecd-7a815951b716",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a5b251d-1b2b-4702-8e2f-b2542f023fde"
        },
        {
            "id": "704f02cd-4a6c-4635-8b23-c6c02be52d60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0a5b251d-1b2b-4702-8e2f-b2542f023fde"
        },
        {
            "id": "09295c28-4f4a-4dd3-8cbd-8bda0bbe7555",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "0a5b251d-1b2b-4702-8e2f-b2542f023fde"
        },
        {
            "id": "761f6e58-bfa3-47eb-bcab-8be032e62044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0a5b251d-1b2b-4702-8e2f-b2542f023fde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "700674dc-296e-4db9-9658-943005d93df9",
    "visible": true
}