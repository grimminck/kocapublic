event_inherited()

if !frontPanelActive
{
	alarm[0] = 1
	instance_activate_object(obhf)
	instance_activate_object(ohiu)
	instance_activate_object(ohid)
	instance_activate_object(ohinf)
	instance_activate_object(omiu)
	instance_activate_object(omid)
	instance_activate_object(ominf)
	instance_activate_object(ohiAMPMf)
	instance_activate_object(ohfu)
	instance_activate_object(ohfd)
	instance_activate_object(ohfnf)
	instance_activate_object(omfu)
	instance_activate_object(omfd)
	instance_activate_object(omfnf)
	instance_activate_object(ohfAMPMf)
	instance_activate_object(obhaf)
}