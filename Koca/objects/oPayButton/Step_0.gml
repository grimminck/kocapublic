if destroying
{
	image_alpha = min(image_alpha-0.09, 1) // Fade out
	if image_alpha <= 0 instance_destroy()	// And Destroy
}
else image_alpha = min(image_alpha+0.09, 1) // Fade in