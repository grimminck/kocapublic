{
    "id": "11925829-5c96-4d5f-982a-5d939a23b80f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPayButton",
    "eventList": [
        {
            "id": "ad3bcd5b-2797-4e4d-b1cf-1cae6678c803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "11925829-5c96-4d5f-982a-5d939a23b80f"
        },
        {
            "id": "3c747439-dd1c-4239-81cf-d5c1fa1c0de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11925829-5c96-4d5f-982a-5d939a23b80f"
        },
        {
            "id": "7dff7d4d-499e-4f8b-8f36-635cb3f1c856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11925829-5c96-4d5f-982a-5d939a23b80f"
        },
        {
            "id": "1700896c-ecd5-40d4-8a45-2d44972684e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "11925829-5c96-4d5f-982a-5d939a23b80f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0b2ac52-9720-495e-8235-0d3f688cdf2e",
    "visible": true
}