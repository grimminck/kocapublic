{
    "id": "46716b37-14d2-49d6-bf1b-bba3d55d2973",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOnePixel",
    "eventList": [
        {
            "id": "496577b1-80eb-4f6c-955e-014c8cf4d982",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46716b37-14d2-49d6-bf1b-bba3d55d2973"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e54e59e-fbcb-4924-acc5-bdf6ce66ebe2",
    "visible": true
}