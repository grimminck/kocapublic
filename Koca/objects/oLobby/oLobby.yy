{
    "id": "99a096d1-35c0-4232-8a93-70bd328bf748",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLobby",
    "eventList": [
        {
            "id": "dd270299-0585-4ab3-b9d4-705eeb3eeaad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99a096d1-35c0-4232-8a93-70bd328bf748"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": false
}