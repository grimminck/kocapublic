event_inherited()

if (registroRut != "" and
	registroClave != "" and
	registroNombre != "" and
	registroNumeroTel != "" and
	registroCorreo != "" and
	registroFarmacia != "" and
	registroNumeroLocal != "" and
	registroDireccionLocal != "" )
{
	scr_transmission_register(false)
}
else
{
	scr_show_alert("Registro invalido")
}