{
    "id": "27c14dcb-2771-4c8f-bc6f-87b0158af170",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonAnteriorDia",
    "eventList": [
        {
            "id": "a7aead53-fde2-4d19-85f4-8d8ebedd50b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "27c14dcb-2771-4c8f-bc6f-87b0158af170"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c1ae6e5-d1ae-4add-9956-bdd64886bfbe",
    "visible": false
}