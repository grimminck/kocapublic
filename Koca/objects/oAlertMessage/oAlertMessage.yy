{
    "id": "d3649ed6-e9a1-47d8-a4e7-63c0605f9174",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAlertMessage",
    "eventList": [
        {
            "id": "d8ae6bce-ef8f-4abf-8b07-75dd2e18a330",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3649ed6-e9a1-47d8-a4e7-63c0605f9174"
        },
        {
            "id": "46096961-5b65-432b-a0e6-e0e11a4c120d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3649ed6-e9a1-47d8-a4e7-63c0605f9174"
        },
        {
            "id": "9c31f554-d325-49b8-b3b9-4341982cdc85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d3649ed6-e9a1-47d8-a4e7-63c0605f9174"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13a09d3e-ba25-4345-8ba6-2bd8b4bcce19",
    "visible": true
}