timer++

var _max = 55
if timer>_max {
	timer=0
	alertCount++
}

var fade = 0.022
if alertCount>=3 {
	image_alpha -= fade
	if image_alpha <= 0 instance_destroy() }
else if timer<_max/2 image_alpha += fade
else				image_alpha -= fade