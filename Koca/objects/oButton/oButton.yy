{
    "id": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButton",
    "eventList": [
        {
            "id": "394490bf-c472-41ce-99fb-9b3b98672b54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "1d1325f9-6b3f-402f-8ec8-673fe812c934",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "98b81486-9923-43d0-bddb-0b1d6df2808e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "73d2ee28-62f6-441a-bcc5-79e97f45ac32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "b38c85db-d1e5-4f8a-a93e-00ffa7fd9879",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "4c5c21a7-52b0-4337-8a36-a525484b0334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        },
        {
            "id": "28c27ac9-458b-4572-8869-fd2789100966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": true
}