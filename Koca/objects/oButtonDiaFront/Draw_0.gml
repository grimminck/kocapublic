draw_set_alpha(whiteSquareAlpha)
draw_rectangle_color(0, 0, room_width, room_height, c_white, c_white, c_white, c_white, false)
draw_set_alpha(1)

draw_self()
draw_set_alpha(image_alpha)
draw_set_parameters(fa_center, fa_middle, fArial25, c_gray)
draw_text(x+50, y, string(ofrecerDia))
draw_text(x+140, y, string(ofrecerMes))
draw_text(x+260, y, string(ofrecerAnno))
draw_set_alpha(1)

