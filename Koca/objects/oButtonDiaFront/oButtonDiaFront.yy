{
    "id": "78fed927-0d27-4901-9144-0853ae4946db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonDiaFront",
    "eventList": [
        {
            "id": "e22631e8-eb31-42c7-9584-2413e7445dd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78fed927-0d27-4901-9144-0853ae4946db"
        },
        {
            "id": "1b95e82d-3dd1-4f62-b118-e176d2786f86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "78fed927-0d27-4901-9144-0853ae4946db"
        },
        {
            "id": "b1c73fbd-d66b-44f5-8691-3adb291f7938",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78fed927-0d27-4901-9144-0853ae4946db"
        },
        {
            "id": "50ae2464-577e-4bce-a7e4-04b2f7bb1ac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "78fed927-0d27-4901-9144-0853ae4946db"
        },
        {
            "id": "68331475-6f2e-4bdd-bf96-d59236a298c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "78fed927-0d27-4901-9144-0853ae4946db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb1645d1-9cad-47fa-823f-b182ad42b867",
    "visible": true
}