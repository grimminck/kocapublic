var al = 0.04
if !destroying
{
	whiteSquareAlpha = min(whiteSquareAlpha+al/2, 0.5)
	image_alpha = min(image_alpha+al, 1)
}
else
{
	whiteSquareAlpha = max(whiteSquareAlpha-al/2, 0)
	image_alpha-=al
	if image_alpha<=0 instance_destroy() 
}