{
    "id": "be87619f-b02f-4833-aa63-76a3628cc57a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketsSave",
    "eventList": [
        {
            "id": "7fa8ca15-6d68-4eed-b5d0-75600fd9c4c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be87619f-b02f-4833-aa63-76a3628cc57a"
        },
        {
            "id": "84739acb-6a64-43fe-8583-481174143490",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "be87619f-b02f-4833-aa63-76a3628cc57a"
        },
        {
            "id": "74427cf3-0725-470e-a120-9aa101f418dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "be87619f-b02f-4833-aa63-76a3628cc57a"
        },
        {
            "id": "a72a330e-6ae0-4a7d-8512-09d1cecb87f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "be87619f-b02f-4833-aa63-76a3628cc57a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": false
}