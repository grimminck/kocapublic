{
    "id": "939e5531-bc77-4433-8a41-50aaa6fa68f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketToTake",
    "eventList": [
        {
            "id": "02f9bb59-59a3-4d53-bdc8-b4a3a7080863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "939e5531-bc77-4433-8a41-50aaa6fa68f4"
        },
        {
            "id": "d2f631af-2155-41de-9a42-204af7419851",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "939e5531-bc77-4433-8a41-50aaa6fa68f4"
        },
        {
            "id": "58bfd98e-baef-4aef-9dea-3c89e19f5917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "939e5531-bc77-4433-8a41-50aaa6fa68f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b7f0644-c315-49d5-b570-90dd0120665a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
    "visible": true
}