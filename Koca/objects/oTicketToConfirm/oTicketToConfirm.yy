{
    "id": "e00b70dd-5dc9-4ccc-963b-748b4df00b03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketToConfirm",
    "eventList": [
        {
            "id": "d19f951f-9558-4f06-aba3-9c54e4f1a710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e00b70dd-5dc9-4ccc-963b-748b4df00b03"
        },
        {
            "id": "2bb74347-fda8-416b-991f-0e806bc3d7ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e00b70dd-5dc9-4ccc-963b-748b4df00b03"
        },
        {
            "id": "bd7c867e-ef36-4627-9d5f-5e9013027eb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e00b70dd-5dc9-4ccc-963b-748b4df00b03"
        },
        {
            "id": "40701bf1-65b0-4786-a813-ad9b6153862b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e00b70dd-5dc9-4ccc-963b-748b4df00b03"
        },
        {
            "id": "c193c6cd-2812-45c5-8fa6-2c902c3c375c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e00b70dd-5dc9-4ccc-963b-748b4df00b03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b7f0644-c315-49d5-b570-90dd0120665a",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
    "visible": true
}