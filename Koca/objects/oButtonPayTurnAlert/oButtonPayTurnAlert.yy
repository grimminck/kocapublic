{
    "id": "d027fcaf-5a8b-4752-916e-51868b6a3fcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonPayTurnAlert",
    "eventList": [
        {
            "id": "994609e0-9343-4157-8973-67d4330ceb8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d027fcaf-5a8b-4752-916e-51868b6a3fcd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bacf1224-cb80-464f-b104-f71d6d5346d9",
    "visible": true
}