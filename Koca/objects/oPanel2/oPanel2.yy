{
    "id": "20696d41-c80a-4517-8b46-a78b76a1c1ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel2",
    "eventList": [
        {
            "id": "0446f64b-b060-4e5b-ab99-b8bbe6de081a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        },
        {
            "id": "a4eeacd3-5c00-4848-8d45-f8274ad7b0c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        },
        {
            "id": "2c165a63-45c5-4c3d-a67b-47e904698ab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        },
        {
            "id": "686f7f11-bfc4-4e79-8019-109a03c884d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        },
        {
            "id": "645c4315-0fcd-43b7-96d9-dde5deb97fd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        },
        {
            "id": "22c2172d-b75e-4cf0-b053-0216e2fdc308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "20696d41-c80a-4517-8b46-a78b76a1c1ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "51a441fa-9979-4a3d-bead-222ae08537e7",
    "visible": true
}