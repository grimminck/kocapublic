var rightThreshold = sprite_width-1
if !locked
{
	if mouseActivated
	{
		scr_move_panel_2(clamp(lastPosition - mouseLastPressedX + mouse_x, -1, rightThreshold))
	}
	else
	{
		if (x > -1)
			scr_move_panel_2(max(x - 21, -1))
	}
}
else
{
	if (x < rightThreshold) scr_move_panel_2(x + 21) 
	scr_move_panel_2(min(x, rightThreshold))
}