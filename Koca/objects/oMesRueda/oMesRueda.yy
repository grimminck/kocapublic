{
    "id": "dfb1a23c-5c70-4c4b-9aab-9d77cdea4d7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMesRueda",
    "eventList": [
        {
            "id": "3fe9eb5c-d737-470e-b2f0-bc44b1be3ce4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dfb1a23c-5c70-4c4b-9aab-9d77cdea4d7d"
        },
        {
            "id": "a0a4951f-491d-4120-a3c3-8614cfb3cc93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dfb1a23c-5c70-4c4b-9aab-9d77cdea4d7d"
        },
        {
            "id": "cd0ba5dc-74eb-42a5-aee0-a06c04d98d26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dfb1a23c-5c70-4c4b-9aab-9d77cdea4d7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}