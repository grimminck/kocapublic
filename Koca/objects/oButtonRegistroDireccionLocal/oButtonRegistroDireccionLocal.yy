{
    "id": "3954ad4c-b879-4bd5-b9dd-7bcc25c8a586",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonRegistroDireccionLocal",
    "eventList": [
        {
            "id": "c20618bb-83fc-414a-8ef6-fa1bd73a7ef2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "3954ad4c-b879-4bd5-b9dd-7bcc25c8a586"
        },
        {
            "id": "b83eeedb-4351-4fe8-a5dc-376d8a3a1646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3954ad4c-b879-4bd5-b9dd-7bcc25c8a586"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73ae1f31-a21f-4ab8-93e1-d1338cf033de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "49fde1f9-e51a-4743-add4-641d5b9337a5",
    "visible": true
}