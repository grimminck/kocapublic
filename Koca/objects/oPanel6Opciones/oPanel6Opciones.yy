{
    "id": "7c2a86d5-c226-4419-94f7-48591ad00400",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel6Opciones",
    "eventList": [
        {
            "id": "cf5f863c-390e-40c5-9411-abb4163ba167",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        },
        {
            "id": "5efdb1e1-ab5c-4ddb-b7bb-fb79b4acabb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        },
        {
            "id": "e99f0de3-7e59-4ced-aab4-5e38aa7f5f71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        },
        {
            "id": "3cd75425-6497-4a1d-85db-4b741555142b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        },
        {
            "id": "0c4ca79d-29f0-4d86-8a70-663c134f5250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        },
        {
            "id": "a9f091fb-b1b9-42ed-8731-b1577793993d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7c2a86d5-c226-4419-94f7-48591ad00400"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb3b7dd1-94e2-4966-bcf9-84886ca78851",
    "visible": true
}