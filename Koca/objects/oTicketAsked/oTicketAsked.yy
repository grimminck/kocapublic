{
    "id": "bc4a27b8-4a1c-47a6-808a-fca17d239bb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketAsked",
    "eventList": [
        {
            "id": "209f29aa-b0d2-49b7-81f5-74c4f5e0c8ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bc4a27b8-4a1c-47a6-808a-fca17d239bb8"
        },
        {
            "id": "1f7e62c0-bbf5-4af5-9af1-f970849a465b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc4a27b8-4a1c-47a6-808a-fca17d239bb8"
        },
        {
            "id": "a12ab38e-c7a5-4a27-bfd1-a4ff2c8a60ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bc4a27b8-4a1c-47a6-808a-fca17d239bb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b7f0644-c315-49d5-b570-90dd0120665a",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "679bff1d-f96b-42be-bb4c-e5b8fd82c09a",
    "visible": true
}