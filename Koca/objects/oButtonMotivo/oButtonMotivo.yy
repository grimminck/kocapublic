{
    "id": "7c8a2c88-fb4d-40c5-8eb4-ea631754ba73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonMotivo",
    "eventList": [
        {
            "id": "4c674426-dc66-4493-bd00-bb91e7fa9734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7c8a2c88-fb4d-40c5-8eb4-ea631754ba73"
        },
        {
            "id": "8430381f-128f-41f4-94d2-ec2ed7e625cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c8a2c88-fb4d-40c5-8eb4-ea631754ba73"
        },
        {
            "id": "1dd081cc-056c-4fd0-a8b8-2ed554520fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "7c8a2c88-fb4d-40c5-8eb4-ea631754ba73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73ae1f31-a21f-4ab8-93e1-d1338cf033de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25c61d94-601e-4d5b-a066-d4a10477c70e",
    "visible": true
}