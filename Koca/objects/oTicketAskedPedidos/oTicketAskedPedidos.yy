{
    "id": "37fe6097-7742-426f-934e-78fb40619b22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketAskedPedidos",
    "eventList": [
        {
            "id": "0871068f-a87b-4567-91a8-cf2ed03aee18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "37fe6097-7742-426f-934e-78fb40619b22"
        },
        {
            "id": "c4d5c92e-ebb5-4379-81c1-781fb6b9373b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37fe6097-7742-426f-934e-78fb40619b22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b7f0644-c315-49d5-b570-90dd0120665a",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "679bff1d-f96b-42be-bb4c-e5b8fd82c09a",
    "visible": true
}