timeAlive++

var al = 0.04
if !destroying
{
	image_alpha = min(image_alpha+al, 1)
}
else
{
	image_alpha-=al
	if image_alpha<=0 and timeAlive>30 
	{
		timeAlive = 0
		instance_deactivate_object(id)
	}
}