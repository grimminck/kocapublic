{
    "id": "3fb3f254-faed-49dc-b2cc-aaddfc9d1ee6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDebug",
    "eventList": [
        {
            "id": "04e3b45d-0655-4f0b-bf7a-d5a9220ede4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "3fb3f254-faed-49dc-b2cc-aaddfc9d1ee6"
        },
        {
            "id": "8dd4c393-54c3-4cc9-8054-a2f409ee3e6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 9,
            "m_owner": "3fb3f254-faed-49dc-b2cc-aaddfc9d1ee6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "741b7d9b-dbd7-4e5b-b212-da738d44a798",
    "visible": false
}