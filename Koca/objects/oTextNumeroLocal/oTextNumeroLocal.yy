{
    "id": "14a400a3-d701-44f9-b9be-658ea4281f67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextNumeroLocal",
    "eventList": [
        {
            "id": "ee63bf76-2b35-4b97-b535-8dd1ff096be4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14a400a3-d701-44f9-b9be-658ea4281f67"
        },
        {
            "id": "34fb5e72-9d7e-4f31-a696-9331a0f97d6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "14a400a3-d701-44f9-b9be-658ea4281f67"
        },
        {
            "id": "70e62d17-35eb-401a-93fa-c0ff240be0ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "14a400a3-d701-44f9-b9be-658ea4281f67"
        },
        {
            "id": "ca7a3dae-7a7d-4dc0-bb0a-1fccc4141699",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "14a400a3-d701-44f9-b9be-658ea4281f67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3fc7aafd-eccc-4535-b940-4228fd6f74ec",
    "visible": true
}