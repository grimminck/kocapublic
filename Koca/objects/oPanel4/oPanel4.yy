{
    "id": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel4",
    "eventList": [
        {
            "id": "18b5ed5d-5077-477e-8ab6-a4a74b9b8071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c"
        },
        {
            "id": "60884fb1-e43a-489b-ad68-2abad0482d9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c"
        },
        {
            "id": "05736831-3160-44db-be1b-becb7144887b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c"
        },
        {
            "id": "064c01a9-d94b-47bb-8eae-6e3ecc988039",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c"
        },
        {
            "id": "9d427656-06eb-4243-bfde-b5d4e03a7658",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b0ce33d0-8fe9-44b7-b971-5f2b4cd91c4c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46f4b200-6505-4964-8645-cfaa08eec4fc",
    "visible": true
}