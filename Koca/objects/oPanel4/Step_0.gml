var upThreshold = room_height-sprite_height
if !locked
{
	if (y < room_height)
		scr_move_panel_4(clamp(y + 35, upThreshold, room_height))
}
else
{
	if (y > upThreshold) scr_move_panel_4(max(y - 35, upThreshold))
}