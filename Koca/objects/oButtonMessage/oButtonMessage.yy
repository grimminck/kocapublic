{
    "id": "f3accbad-976e-4457-bff1-87d2bad8465b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonMessage",
    "eventList": [
        {
            "id": "6539e726-f226-44cc-ba25-d374be0e122c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f3accbad-976e-4457-bff1-87d2bad8465b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54607d73-d9a8-4517-bd33-3416d4ae776d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a6557b6-fd86-47a8-b9b5-e65992bdd165",
    "visible": true
}