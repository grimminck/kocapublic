/// @desc Ofrece turno
event_inherited()

buffer_seek(buff, buffer_seek_start, 0)

var command = 2
var d = get_string("Dia", "")
var m = get_string("mes", "")
var a = get_string("anno", "")
buffer_write(buff, buffer_u16, command)
buffer_write(buff, buffer_u16, d)
buffer_write(buff, buffer_u8, m)
buffer_write(buff, buffer_u16, a)

network_send_packet(Sock, buff, buffer_tell(buff))