{
    "id": "4c89446a-cec6-4488-b725-a2998fcd2793",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinutoInicialDown",
    "eventList": [
        {
            "id": "e706d152-29d1-4649-bec8-626807b14e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4c89446a-cec6-4488-b725-a2998fcd2793"
        },
        {
            "id": "abb8ea9a-f053-4360-9497-b472540b5259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c89446a-cec6-4488-b725-a2998fcd2793"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
    "visible": true
}