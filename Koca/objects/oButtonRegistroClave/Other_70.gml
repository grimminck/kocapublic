var type = string(async_load[? "type"])
var data = string(async_load[? "data"]) // String written by the user

// Check for INPUTTEXT type of event
if type == "INPUTTEXT"
{
	if activated
	{
		text = data
		registroClave = data
		activated = false
	}
}