{
    "id": "b068e3f7-bbc4-4f34-b1ad-19bfaf287026",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonRegistroClave",
    "eventList": [
        {
            "id": "e59c2758-9bf0-4d11-9cf7-f0b1d3a4b738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "b068e3f7-bbc4-4f34-b1ad-19bfaf287026"
        },
        {
            "id": "49e7af2e-af15-4f3b-85be-9270b9b40ab3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b068e3f7-bbc4-4f34-b1ad-19bfaf287026"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73ae1f31-a21f-4ab8-93e1-d1338cf033de",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "49fde1f9-e51a-4743-add4-641d5b9337a5",
    "visible": true
}