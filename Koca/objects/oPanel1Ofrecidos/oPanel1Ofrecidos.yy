{
    "id": "891e6a6c-3334-4e74-b406-a54687b2a792",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel1Ofrecidos",
    "eventList": [
        {
            "id": "2ebf8df4-357d-4b8b-94ae-a982c54fab05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        },
        {
            "id": "a3f4b421-4871-45b8-9fa4-da97d413176d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        },
        {
            "id": "4b2e17fd-d4bc-4d58-b46f-70929b1425bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        },
        {
            "id": "e26bb8b0-98aa-488f-addc-1fd0aa56af33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        },
        {
            "id": "bf0fe44e-bea6-4579-94ba-b1d062eafc66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        },
        {
            "id": "43b09d82-1c0d-4952-87c7-1118d0092920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "891e6a6c-3334-4e74-b406-a54687b2a792"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7805e54-6715-49f2-bb85-d33957befa7f",
    "visible": true
}