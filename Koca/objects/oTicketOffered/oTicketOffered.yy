{
    "id": "5c30b6ea-620b-4a6d-908f-81011d624c66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTicketOffered",
    "eventList": [
        {
            "id": "790f6cb9-0ef1-4155-85d6-fb61ea06736b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5c30b6ea-620b-4a6d-908f-81011d624c66"
        },
        {
            "id": "a76fa35c-50bb-47cd-a2ea-133ce03fbb0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c30b6ea-620b-4a6d-908f-81011d624c66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b7f0644-c315-49d5-b570-90dd0120665a",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
    "visible": true
}