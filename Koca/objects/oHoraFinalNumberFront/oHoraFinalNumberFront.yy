{
    "id": "ddeb118b-ee7d-4cde-90fa-32e4dcaef67f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHoraFinalNumberFront",
    "eventList": [
        {
            "id": "2fc97dc3-3504-42c8-9f21-6b2afb5c96b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "ddeb118b-ee7d-4cde-90fa-32e4dcaef67f"
        },
        {
            "id": "1eaa8f04-595b-4452-b875-317907a8cff1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ddeb118b-ee7d-4cde-90fa-32e4dcaef67f"
        },
        {
            "id": "f42daacd-8299-4eba-950a-9243a07a8a19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ddeb118b-ee7d-4cde-90fa-32e4dcaef67f"
        },
        {
            "id": "4f07a2a3-e109-4491-9fb8-fbff620da60e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddeb118b-ee7d-4cde-90fa-32e4dcaef67f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}