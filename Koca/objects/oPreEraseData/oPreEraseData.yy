{
    "id": "2cb43cfa-8847-4dda-9a4f-316750567622",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPreEraseData",
    "eventList": [
        {
            "id": "7788e33e-5c35-4de9-acc4-6e85ccc4557d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2cb43cfa-8847-4dda-9a4f-316750567622"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c43816f5-832b-4004-96f3-3e58d427837d",
    "visible": true
}