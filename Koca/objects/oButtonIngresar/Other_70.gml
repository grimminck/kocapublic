var type = string(async_load[? "type"])
var data = string(async_load[? "data"])

//check for INPUTTEXT type of event
if type == "INPUTTEXT"
{
	//Here you should receive the string containing the text written by the user
	//which is stored inside the variable called "data"
	//now use it as you please
	if activated
	{
		switch string_index
		{
			case 0: rut = data
				showInputText("Ingrese su Clave", "OK", "Cancel") break
			case 1: clave = data
				// Send buffer
				scr_transmission_login(false, rut, clave)
				activated = false break
		}
		
		string_index++
	}
}