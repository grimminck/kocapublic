{
    "id": "3684ce86-d91d-4f1c-84da-69d765ea63d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonRegistroSB",
    "eventList": [
        {
            "id": "6f8ce5a4-1192-45cb-b0d5-b9a58e865fc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3684ce86-d91d-4f1c-84da-69d765ea63d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cfaa1ef0-8352-49eb-9eed-29ffcbea37da",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "69a8421a-a4b1-4d3e-aeaf-c32e0ced9628",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b71ed7fc-4aa2-477b-9193-ebe5b5d1ae54",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "3b94829b-7121-4587-ab9f-358b560d97dc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "19073d52-b8ee-4728-b0b6-d201355c54ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e94489d9-c222-47e4-af85-e8319c4fb681",
    "visible": true
}