{
    "id": "ad3b5295-da6c-4ab9-84a9-1d9f475f2e80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonDia",
    "eventList": [
        {
            "id": "b545c9f8-9c30-41a6-8e45-fb3622463880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad3b5295-da6c-4ab9-84a9-1d9f475f2e80"
        },
        {
            "id": "7bbc36af-c60a-4b23-bb67-0a965a5a3e2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ad3b5295-da6c-4ab9-84a9-1d9f475f2e80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "89d1f0c2-1ec1-4230-b370-7fc7a81575b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e6990a8-97e5-401a-bf56-8f2cb4e0dd4d",
    "visible": true
}