{
    "id": "fde4d675-5929-41c1-a71b-32eef932567d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHoraInicialUp",
    "eventList": [
        {
            "id": "35b437c9-3143-451a-b40e-212591ce5e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fde4d675-5929-41c1-a71b-32eef932567d"
        },
        {
            "id": "423f001e-e033-4a91-b4c2-7de74269b587",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fde4d675-5929-41c1-a71b-32eef932567d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94c44984-5c38-4f65-9231-c901474a8733",
    "visible": true
}