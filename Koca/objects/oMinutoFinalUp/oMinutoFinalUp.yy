{
    "id": "4086826d-7765-40ec-ac72-64f37a6e30e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMinutoFinalUp",
    "eventList": [
        {
            "id": "0053234e-f134-4d0e-a10e-1bb7e769ea53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4086826d-7765-40ec-ac72-64f37a6e30e4"
        },
        {
            "id": "fbdb0fed-54cf-4f2e-84f0-2c296788073e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4086826d-7765-40ec-ac72-64f37a6e30e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7811aca8-e3a6-4f6a-9970-143653e68650",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94c44984-5c38-4f65-9231-c901474a8733",
    "visible": true
}