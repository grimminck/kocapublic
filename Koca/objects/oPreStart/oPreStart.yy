{
    "id": "89c08efd-1636-4100-b887-6b854b672768",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPreStart",
    "eventList": [
        {
            "id": "1ccbb6fc-1c77-489f-8084-e00092aebe70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89c08efd-1636-4100-b887-6b854b672768"
        },
        {
            "id": "7f5f5bfa-27a8-4ed8-951a-e469cf2164c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "89c08efd-1636-4100-b887-6b854b672768"
        },
        {
            "id": "dc3506d5-0fa4-4e61-bfa9-c705c2b75d04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89c08efd-1636-4100-b887-6b854b672768"
        },
        {
            "id": "8dea445d-fcbb-4958-8a4e-b92c8ea7fd69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 69,
            "eventtype": 9,
            "m_owner": "89c08efd-1636-4100-b887-6b854b672768"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": false
}