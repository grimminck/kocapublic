{
    "id": "68f8eb6f-5b02-45a6-bb66-8a6148447759",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPanel7Pedidos",
    "eventList": [
        {
            "id": "e52e4dbf-0671-4240-b784-df5b0c4c6a97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        },
        {
            "id": "15a3fbf0-5060-4e15-8591-c6009524e4ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        },
        {
            "id": "d8fe48d2-2d81-493c-aef1-2ffe18b527ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        },
        {
            "id": "7dc1c4f8-4e2a-47ce-8b2c-1ca8b3597366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        },
        {
            "id": "0c99b5d5-165e-4563-9eee-d8afba2ddefa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        },
        {
            "id": "47d28656-4c58-4ee4-b2d7-bf818dc65058",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "68f8eb6f-5b02-45a6-bb66-8a6148447759"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7805e54-6715-49f2-bb85-d33957befa7f",
    "visible": true
}