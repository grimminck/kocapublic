if (mouseActivated and mouseLastX < mouse_x) locked = true
else
{
	// Cuando suelta el panel en pasado el limite
	if ((mouse_x > 100) and (x > 0) and !hover and mouseActivated) locked = true
	else 
	{
		if (mouseLastX > mouse_x) locked = false
	}

	if hover
	{
		if (mouseLastX > mouse_x or mouse_x < 100) locked = false
		else locked = true
	}
}

mouseActivated = false