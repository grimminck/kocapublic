{
    "id": "eb773346-419a-4170-8ae5-385a8ce613b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonFront",
    "eventList": [
        {
            "id": "f19d2360-98de-4376-bc14-981aba07d53d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "39664f04-ef19-47db-92d3-318c3f35e7ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "3d09cc3a-22de-40ca-a4cd-2dfb1c90ebe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "c7b67b25-7fe2-494c-a1bf-6d438db50e2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "1a0979bf-a355-425e-9fa4-6748b6e9e7da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "f97a6a14-0847-4021-9178-d6f1ea74d1b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        },
        {
            "id": "f6290b51-fcc0-4a34-aedc-c89f9aca4206",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eb773346-419a-4170-8ae5-385a8ce613b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "visible": true
}