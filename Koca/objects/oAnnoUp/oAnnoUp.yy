{
    "id": "eaeb0dc3-2c10-4083-8065-6eda58b68cb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAnnoUp",
    "eventList": [
        {
            "id": "07aebc13-42aa-4253-97e9-3ebec209b6a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "eaeb0dc3-2c10-4083-8065-6eda58b68cb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df4a5dd9-02d1-4aeb-9579-90f1bedb5c53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "94c44984-5c38-4f65-9231-c901474a8733",
    "visible": true
}