{
    "id": "02282441-f1d9-4ca7-a8df-34110933cf8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 188,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96096e39-28b9-4afe-9ec1-741712fd0be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02282441-f1d9-4ca7-a8df-34110933cf8c",
            "compositeImage": {
                "id": "68c7f9a9-ee09-478c-9543-82306b4a9c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96096e39-28b9-4afe-9ec1-741712fd0be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1397cbd8-179b-4ffc-af40-0f29dc36c231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96096e39-28b9-4afe-9ec1-741712fd0be2",
                    "LayerId": "958773d0-b26d-4ea8-b9c1-6381124521ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "958773d0-b26d-4ea8-b9c1-6381124521ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02282441-f1d9-4ca7-a8df-34110933cf8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 189,
    "xorig": 0,
    "yorig": 0
}