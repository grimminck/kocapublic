{
    "id": "d486b09c-193f-4127-8ea9-3a92859541e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRueda",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 595,
    "bbox_left": 0,
    "bbox_right": 653,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cc9c900-b908-43af-8c3f-e54341248cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d486b09c-193f-4127-8ea9-3a92859541e6",
            "compositeImage": {
                "id": "a9bcb39e-6f42-4c22-907a-8ec16ba27dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cc9c900-b908-43af-8c3f-e54341248cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d2259b-398b-4f2a-856a-963aa754fd37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cc9c900-b908-43af-8c3f-e54341248cdb",
                    "LayerId": "8193bd7a-a3e3-49a0-b8b2-d44cc3576bf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 643,
    "layers": [
        {
            "id": "8193bd7a-a3e3-49a0-b8b2-d44cc3576bf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d486b09c-193f-4127-8ea9-3a92859541e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 654,
    "xorig": 327,
    "yorig": 321
}