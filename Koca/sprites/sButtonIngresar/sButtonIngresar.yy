{
    "id": "4543d840-0c47-41fa-9d8c-b6f6b7548172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonIngresar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa70cd86-f5bf-4387-97c6-2fd19bb02d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4543d840-0c47-41fa-9d8c-b6f6b7548172",
            "compositeImage": {
                "id": "bf55591e-4adc-4857-b52b-db4d7b932bed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa70cd86-f5bf-4387-97c6-2fd19bb02d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dceaab-f051-42dd-b0eb-6e353f61a0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa70cd86-f5bf-4387-97c6-2fd19bb02d61",
                    "LayerId": "54284413-c7fd-45f4-8e76-43f387670baf"
                },
                {
                    "id": "e5cbde5c-62bb-4303-b8a0-33f0bb98a9b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa70cd86-f5bf-4387-97c6-2fd19bb02d61",
                    "LayerId": "29b80f50-a46a-4aa1-879f-f1096820e7ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "29b80f50-a46a-4aa1-879f-f1096820e7ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4543d840-0c47-41fa-9d8c-b6f6b7548172",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "54284413-c7fd-45f4-8e76-43f387670baf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4543d840-0c47-41fa-9d8c-b6f6b7548172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 73,
    "yorig": 22
}