{
    "id": "8c1ae6e5-d1ae-4add-9956-bdd64886bfbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonPrevious",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e6e784f-283d-403e-82eb-0bdd610c4efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c1ae6e5-d1ae-4add-9956-bdd64886bfbe",
            "compositeImage": {
                "id": "4a058422-90c8-4264-8577-3ea3f063a267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e6e784f-283d-403e-82eb-0bdd610c4efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13137091-4ca1-4f51-bbc2-83282e2a09e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e6e784f-283d-403e-82eb-0bdd610c4efd",
                    "LayerId": "a666e23f-21e1-4e4d-8430-2e9910641630"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "a666e23f-21e1-4e4d-8430-2e9910641630",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c1ae6e5-d1ae-4add-9956-bdd64886bfbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 29,
    "yorig": 29
}