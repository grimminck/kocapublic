{
    "id": "98c9e475-59b9-4592-a609-ea6f011d30d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlechaAbajo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdfecb0b-7569-4877-8ce3-666a556b549f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
            "compositeImage": {
                "id": "5fa60ff9-3b51-41ab-8b83-594a0aaccc3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdfecb0b-7569-4877-8ce3-666a556b549f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1063e67-1fbe-4375-a117-3edc4925061b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdfecb0b-7569-4877-8ce3-666a556b549f",
                    "LayerId": "ed9db6b4-a50b-48eb-a69a-fa9e2dfe6042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "ed9db6b4-a50b-48eb-a69a-fa9e2dfe6042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c9e475-59b9-4592-a609-ea6f011d30d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 45,
    "yorig": 45
}