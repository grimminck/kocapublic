{
    "id": "fe5b6117-3071-4489-ad1f-a6a5eca6c831",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCalendarBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "990bbd37-a0bf-4967-9092-1bd67f43e4bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5b6117-3071-4489-ad1f-a6a5eca6c831",
            "compositeImage": {
                "id": "9ecda4a0-c6d5-43ce-bec5-4303ddfcb50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "990bbd37-a0bf-4967-9092-1bd67f43e4bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8888b2-a625-45c2-be55-7dd4f5df2261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "990bbd37-a0bf-4967-9092-1bd67f43e4bf",
                    "LayerId": "3877c6bd-e7ef-4716-81fb-494075720be8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "3877c6bd-e7ef-4716-81fb-494075720be8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe5b6117-3071-4489-ad1f-a6a5eca6c831",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 2,
    "yorig": 2
}