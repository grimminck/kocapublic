{
    "id": "a8b12165-888c-4719-9eeb-3933675dc032",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite127",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b87f693e-f011-4327-b1a5-6beeaac6cc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b12165-888c-4719-9eeb-3933675dc032",
            "compositeImage": {
                "id": "4756f1c3-0706-431c-ae45-ffe8acef11c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b87f693e-f011-4327-b1a5-6beeaac6cc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1bb9c7-c834-4913-ad2c-ef6015955430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b87f693e-f011-4327-b1a5-6beeaac6cc9f",
                    "LayerId": "c03e494d-dccc-417f-bf68-81819da6e5dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 79,
    "layers": [
        {
            "id": "c03e494d-dccc-417f-bf68-81819da6e5dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b12165-888c-4719-9eeb-3933675dc032",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 39
}