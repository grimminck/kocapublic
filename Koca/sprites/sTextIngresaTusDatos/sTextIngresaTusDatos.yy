{
    "id": "3322e05f-e22c-403b-a7c1-1e72f44633ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextIngresaTusDatos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 58,
    "bbox_right": 208,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd902a87-6c3a-442c-ae95-24eb713cdb4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3322e05f-e22c-403b-a7c1-1e72f44633ee",
            "compositeImage": {
                "id": "48b46b30-8dc2-482f-8ae3-cfb528f763c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd902a87-6c3a-442c-ae95-24eb713cdb4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b30b4b9-65a4-4b93-a4ea-42589140eed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd902a87-6c3a-442c-ae95-24eb713cdb4a",
                    "LayerId": "309b9138-c06a-468c-bb92-a9592090b4df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "309b9138-c06a-468c-bb92-a9592090b4df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3322e05f-e22c-403b-a7c1-1e72f44633ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}