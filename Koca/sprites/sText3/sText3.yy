{
    "id": "606b3c17-b76c-4a5b-9ea3-0876a7f9dbdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 59,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8820b202-0025-4f31-a67f-643c92a3f866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "606b3c17-b76c-4a5b-9ea3-0876a7f9dbdd",
            "compositeImage": {
                "id": "46419eb9-c0c4-4965-a1eb-e7371a3b378e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8820b202-0025-4f31-a67f-643c92a3f866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0cff55-a173-4471-a9d4-064d0da980a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8820b202-0025-4f31-a67f-643c92a3f866",
                    "LayerId": "d516e8ab-bca1-4732-9d15-d8da3b78dd5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d516e8ab-bca1-4732-9d15-d8da3b78dd5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "606b3c17-b76c-4a5b-9ea3-0876a7f9dbdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}