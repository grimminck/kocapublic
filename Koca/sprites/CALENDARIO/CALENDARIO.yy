{
    "id": "2c9e65eb-e6ca-4073-8c99-f530deac6f82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CALENDARIO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "616d379d-3821-4f54-bca4-b6f6ca1715d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c9e65eb-e6ca-4073-8c99-f530deac6f82",
            "compositeImage": {
                "id": "48262295-6f69-4c06-b9e2-44d4e14a7236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "616d379d-3821-4f54-bca4-b6f6ca1715d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d3c9582-a6a0-4015-a28e-b71db98b1268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "616d379d-3821-4f54-bca4-b6f6ca1715d6",
                    "LayerId": "ddf4509d-92e2-4906-9b88-b633a3d3b07f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "ddf4509d-92e2-4906-9b88-b633a3d3b07f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c9e65eb-e6ca-4073-8c99-f530deac6f82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 37
}