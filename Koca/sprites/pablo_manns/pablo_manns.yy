{
    "id": "9974b301-7182-4edd-a9b6-f94f8bf239d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pablo_manns",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 556,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b1fed75-72a7-45d7-abdc-ededdf2c5004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9974b301-7182-4edd-a9b6-f94f8bf239d4",
            "compositeImage": {
                "id": "16cab489-c383-4f89-9656-824bf7624656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1fed75-72a7-45d7-abdc-ededdf2c5004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2527ae96-863e-4188-9abd-69be755e9870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1fed75-72a7-45d7-abdc-ededdf2c5004",
                    "LayerId": "62aadecb-ceaa-4d72-abc6-15b7b400ae60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "62aadecb-ceaa-4d72-abc6-15b7b400ae60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9974b301-7182-4edd-a9b6-f94f8bf239d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 557,
    "xorig": 237,
    "yorig": 660
}