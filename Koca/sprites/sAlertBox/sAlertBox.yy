{
    "id": "13a09d3e-ba25-4345-8ba6-2bd8b4bcce19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAlertBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cf61d14-445f-4725-bfcf-66ae856394d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13a09d3e-ba25-4345-8ba6-2bd8b4bcce19",
            "compositeImage": {
                "id": "808981cf-cefb-4035-8c99-6a24796896bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf61d14-445f-4725-bfcf-66ae856394d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df908301-0a0e-4388-8625-e662d32de14f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf61d14-445f-4725-bfcf-66ae856394d4",
                    "LayerId": "6aa47c6e-276b-4c8a-a8e5-c1f1b4346253"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6aa47c6e-276b-4c8a-a8e5-c1f1b4346253",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13a09d3e-ba25-4345-8ba6-2bd8b4bcce19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}