{
    "id": "048f6b5d-c5f4-42ab-9711-f476caf96a1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextPedirTurno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 77,
    "bbox_right": 239,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e117b202-e563-4b41-903c-7ded21cee66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "048f6b5d-c5f4-42ab-9711-f476caf96a1c",
            "compositeImage": {
                "id": "87f0227c-2477-4d55-9ee8-4dbcbd39199f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e117b202-e563-4b41-903c-7ded21cee66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3158f8fc-db43-47e4-b8f8-58561e796225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e117b202-e563-4b41-903c-7ded21cee66f",
                    "LayerId": "d4e6adab-942d-4011-955a-d03d81cb58d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d4e6adab-942d-4011-955a-d03d81cb58d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "048f6b5d-c5f4-42ab-9711-f476caf96a1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 157,
    "yorig": 33
}