{
    "id": "36867ba7-5761-438d-9d34-dfae81945a57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonOfrecerDia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd984fae-1173-45d0-b26e-6d435ba62f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36867ba7-5761-438d-9d34-dfae81945a57",
            "compositeImage": {
                "id": "28aec3ec-4fdf-4bee-8846-55a8275c45a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd984fae-1173-45d0-b26e-6d435ba62f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa620d3-6692-487e-a74e-e60b2bf4406f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd984fae-1173-45d0-b26e-6d435ba62f4c",
                    "LayerId": "c482bf52-3669-425f-9985-f27d542c522a"
                },
                {
                    "id": "895b0b99-d7b3-4161-85f0-e6bf5bf43d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd984fae-1173-45d0-b26e-6d435ba62f4c",
                    "LayerId": "7e6e3104-7104-4839-a4c0-e10e3e43b5df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7e6e3104-7104-4839-a4c0-e10e3e43b5df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36867ba7-5761-438d-9d34-dfae81945a57",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c482bf52-3669-425f-9985-f27d542c522a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36867ba7-5761-438d-9d34-dfae81945a57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 12
}