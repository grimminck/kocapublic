{
    "id": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "a_sprite_default",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7aee6c01-ffb9-40af-81f2-09060d0db64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
            "compositeImage": {
                "id": "5e69ebfb-33ad-4791-91ef-88d44d6aa72a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aee6c01-ffb9-40af-81f2-09060d0db64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d436f58-10d0-4de0-9d54-ad0076f4d790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aee6c01-ffb9-40af-81f2-09060d0db64b",
                    "LayerId": "ac3ed250-cce0-49db-8f8d-c9c158eb68a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac3ed250-cce0-49db-8f8d-c9c158eb68a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e99d9ef-5ee2-4783-ac14-f585f6391fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}