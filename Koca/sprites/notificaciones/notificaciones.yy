{
    "id": "334f40c4-2b84-44b2-bcc1-7f1f130aed38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "notificaciones",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 150,
    "bbox_left": 0,
    "bbox_right": 118,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e45ea9cd-2796-4f49-916a-1201ab0bb665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334f40c4-2b84-44b2-bcc1-7f1f130aed38",
            "compositeImage": {
                "id": "f116f50a-ca97-48d0-bd12-18581d18a1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45ea9cd-2796-4f49-916a-1201ab0bb665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf37432a-65c2-44ad-a53b-868f0623bac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45ea9cd-2796-4f49-916a-1201ab0bb665",
                    "LayerId": "23b225a0-90fa-4631-8dd1-9682cf506380"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 151,
    "layers": [
        {
            "id": "23b225a0-90fa-4631-8dd1-9682cf506380",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334f40c4-2b84-44b2-bcc1-7f1f130aed38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 119,
    "xorig": 0,
    "yorig": 0
}