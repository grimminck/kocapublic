{
    "id": "3940719b-1bad-47f6-b451-59191c4052ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MI_PERFIL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0c8bd65-38b8-4779-86e2-1b3dbb933a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3940719b-1bad-47f6-b451-59191c4052ca",
            "compositeImage": {
                "id": "05121fb1-d0b6-4ed0-b757-22a51831ba50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c8bd65-38b8-4779-86e2-1b3dbb933a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51034f3f-e3b7-4e20-a2f2-d8ca2d5a77d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c8bd65-38b8-4779-86e2-1b3dbb933a6f",
                    "LayerId": "09d532f7-affc-478f-9ef1-c8f3086ae3a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "09d532f7-affc-478f-9ef1-c8f3086ae3a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3940719b-1bad-47f6-b451-59191c4052ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 0,
    "yorig": 0
}