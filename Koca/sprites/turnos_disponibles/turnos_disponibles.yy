{
    "id": "6103a9cd-6b83-4dee-9014-3c895e2511a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turnos_disponibles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 491,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16c6b51a-518e-4266-bf4b-7f7b2eb0a402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6103a9cd-6b83-4dee-9014-3c895e2511a2",
            "compositeImage": {
                "id": "42912b07-0d04-4b0d-ad6d-4e9113204fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c6b51a-518e-4266-bf4b-7f7b2eb0a402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead9a7ff-fa0a-45b6-962b-3fd087ee881d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c6b51a-518e-4266-bf4b-7f7b2eb0a402",
                    "LayerId": "2e10ecaa-9b7f-4bba-91de-15aeff3ab74c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "2e10ecaa-9b7f-4bba-91de-15aeff3ab74c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6103a9cd-6b83-4dee-9014-3c895e2511a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 492,
    "xorig": 0,
    "yorig": 0
}