{
    "id": "a97cc61c-5ee0-42b6-8486-9deae7d08610",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dar_turno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b5df529-ba10-4c4e-b01b-955718852534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a97cc61c-5ee0-42b6-8486-9deae7d08610",
            "compositeImage": {
                "id": "922e1e7a-17da-494c-9904-d9562d2ae64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5df529-ba10-4c4e-b01b-955718852534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7224f913-a835-450f-9e15-c4cc5050201a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5df529-ba10-4c4e-b01b-955718852534",
                    "LayerId": "302d3bfa-c305-4f00-9a6f-66525b1341ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 226,
    "layers": [
        {
            "id": "302d3bfa-c305-4f00-9a6f-66525b1341ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a97cc61c-5ee0-42b6-8486-9deae7d08610",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 243,
    "xorig": 108,
    "yorig": 111
}