{
    "id": "71362b46-4284-4c60-9be5-0b23ff24344f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTestButton1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3be306a-9dc7-4531-8f62-35698c152206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71362b46-4284-4c60-9be5-0b23ff24344f",
            "compositeImage": {
                "id": "e5a80a84-1f82-4887-9a66-8caef71da9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3be306a-9dc7-4531-8f62-35698c152206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7446bb6-c0ac-4b82-a7f9-ba2b09962a2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3be306a-9dc7-4531-8f62-35698c152206",
                    "LayerId": "ac7c9c12-005f-4ef5-8d6b-298895d3a383"
                },
                {
                    "id": "59b34cb2-f3cf-4cf8-a751-490ef6cb4bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3be306a-9dc7-4531-8f62-35698c152206",
                    "LayerId": "13522779-0fb3-47ed-902d-ab2b2593853b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "ac7c9c12-005f-4ef5-8d6b-298895d3a383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71362b46-4284-4c60-9be5-0b23ff24344f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "13522779-0fb3-47ed-902d-ab2b2593853b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71362b46-4284-4c60-9be5-0b23ff24344f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 113,
    "yorig": 116
}