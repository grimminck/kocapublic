{
    "id": "7d009e76-5616-44e4-b9d3-649a6255b9e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ab34395-488a-406c-8f55-7ae8ff5e6f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d009e76-5616-44e4-b9d3-649a6255b9e0",
            "compositeImage": {
                "id": "d3ab1fdf-c2b4-4080-9fd0-7ea93d1ae189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab34395-488a-406c-8f55-7ae8ff5e6f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f262b93-da10-438e-9a9e-8f12f42e00c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab34395-488a-406c-8f55-7ae8ff5e6f48",
                    "LayerId": "13f7f427-e7de-47d3-8d01-071dad897905"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "13f7f427-e7de-47d3-8d01-071dad897905",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d009e76-5616-44e4-b9d3-649a6255b9e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 22
}