{
    "id": "adb60f34-ee82-4b62-a71d-ed5301e7e699",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonOfrecerConfirmar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1e4593d-b443-4bd3-84b9-3608a6e8f311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb60f34-ee82-4b62-a71d-ed5301e7e699",
            "compositeImage": {
                "id": "7310ab12-df75-4bca-bcd0-7b1b972a447d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e4593d-b443-4bd3-84b9-3608a6e8f311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82ab336-8c3c-491d-9bd3-0e1ec6cbbfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e4593d-b443-4bd3-84b9-3608a6e8f311",
                    "LayerId": "52398945-243d-44fe-8ab4-bada03a8f91c"
                },
                {
                    "id": "139b3f49-fe88-49ad-8cd3-15684e60267b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e4593d-b443-4bd3-84b9-3608a6e8f311",
                    "LayerId": "63fb0b03-4a24-4720-a6bf-e6fc0c050366"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "52398945-243d-44fe-8ab4-bada03a8f91c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb60f34-ee82-4b62-a71d-ed5301e7e699",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "63fb0b03-4a24-4720-a6bf-e6fc0c050366",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb60f34-ee82-4b62-a71d-ed5301e7e699",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 100,
    "yorig": 44
}