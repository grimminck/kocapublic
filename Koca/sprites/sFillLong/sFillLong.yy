{
    "id": "49fde1f9-e51a-4743-add4-641d5b9337a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFillLong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9cfe907-bf49-4c7f-9ce0-55df525a23d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49fde1f9-e51a-4743-add4-641d5b9337a5",
            "compositeImage": {
                "id": "9ba7cded-1f6f-4a5f-b5f3-8d94432b96f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9cfe907-bf49-4c7f-9ce0-55df525a23d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5535170-6a7a-485b-82c1-34c8b2741bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9cfe907-bf49-4c7f-9ce0-55df525a23d6",
                    "LayerId": "9167f54a-0ff6-4444-a2ba-8befd279ae6d"
                },
                {
                    "id": "2487bb6c-e00e-459f-ad75-495122224c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9cfe907-bf49-4c7f-9ce0-55df525a23d6",
                    "LayerId": "58e8c3a5-096c-47ef-96ce-bf8f52180c69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "9167f54a-0ff6-4444-a2ba-8befd279ae6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49fde1f9-e51a-4743-add4-641d5b9337a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "58e8c3a5-096c-47ef-96ce-bf8f52180c69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49fde1f9-e51a-4743-add4-641d5b9337a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 28,
    "yorig": 25
}