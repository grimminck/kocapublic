{
    "id": "68ed3682-707a-491d-8944-90fd3db368ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketPagado",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 94,
    "bbox_right": 323,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a307d666-0db7-4afb-a4ea-016f80e5a75f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ed3682-707a-491d-8944-90fd3db368ef",
            "compositeImage": {
                "id": "750be556-301a-4c12-9a45-86dbc1e6ac03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a307d666-0db7-4afb-a4ea-016f80e5a75f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d2a802-2cef-4117-acff-dd3ae7535778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a307d666-0db7-4afb-a4ea-016f80e5a75f",
                    "LayerId": "33082a45-f115-492d-bfd0-05a6dc0b61d6"
                },
                {
                    "id": "9f0c4f92-3223-439e-a693-3b3223551310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a307d666-0db7-4afb-a4ea-016f80e5a75f",
                    "LayerId": "8b5cb60f-00c5-4387-8152-ea8874b99d1e"
                },
                {
                    "id": "e88def47-bbc6-4b3b-ac11-200448278721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a307d666-0db7-4afb-a4ea-016f80e5a75f",
                    "LayerId": "e8bde7f7-4696-4d1b-98d4-0899e7c47d12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "33082a45-f115-492d-bfd0-05a6dc0b61d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ed3682-707a-491d-8944-90fd3db368ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 53,
            "visible": true
        },
        {
            "id": "8b5cb60f-00c5-4387-8152-ea8874b99d1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ed3682-707a-491d-8944-90fd3db368ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 51,
            "visible": true
        },
        {
            "id": "e8bde7f7-4696-4d1b-98d4-0899e7c47d12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ed3682-707a-491d-8944-90fd3db368ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 26,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}