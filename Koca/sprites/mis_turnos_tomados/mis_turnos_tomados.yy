{
    "id": "4d426d21-a932-44fe-a668-f650e0c635d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mis_turnos_tomados",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 891,
    "bbox_left": 0,
    "bbox_right": 719,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8072bef9-b3bc-4c7a-9dea-2a20a12cab22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d426d21-a932-44fe-a668-f650e0c635d2",
            "compositeImage": {
                "id": "7de99f0a-bc33-415e-bf68-9fa9913711e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8072bef9-b3bc-4c7a-9dea-2a20a12cab22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20f9b1c-4ddb-415a-aee7-344ab0179232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8072bef9-b3bc-4c7a-9dea-2a20a12cab22",
                    "LayerId": "c11f4af4-2024-4b54-9396-bf49bf4787e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 892,
    "layers": [
        {
            "id": "c11f4af4-2024-4b54-9396-bf49bf4787e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d426d21-a932-44fe-a668-f650e0c635d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 720,
    "xorig": 0,
    "yorig": 0
}