{
    "id": "4b42d83a-d337-4c4b-bffe-7040d987cebb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 258,
    "bbox_left": 0,
    "bbox_right": 297,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26975756-cd50-4398-9c58-37ac3ac4b449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b42d83a-d337-4c4b-bffe-7040d987cebb",
            "compositeImage": {
                "id": "16ce9140-fa0c-4ec7-bc8e-00f897766065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26975756-cd50-4398-9c58-37ac3ac4b449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec2fc71-284e-414a-836f-07fc9e028b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26975756-cd50-4398-9c58-37ac3ac4b449",
                    "LayerId": "1db2ad5d-84e5-4e3c-87c8-eae1ee3b99bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 259,
    "layers": [
        {
            "id": "1db2ad5d-84e5-4e3c-87c8-eae1ee3b99bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b42d83a-d337-4c4b-bffe-7040d987cebb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 298,
    "xorig": 0,
    "yorig": 0
}