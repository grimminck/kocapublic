{
    "id": "d74cdd25-5553-4943-bd8e-14e3591b0fc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonSquareBigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ab54877-c225-40b3-be81-f2faf9871f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74cdd25-5553-4943-bd8e-14e3591b0fc8",
            "compositeImage": {
                "id": "3669883b-f71e-44ce-9c80-38b591fe8a2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab54877-c225-40b3-be81-f2faf9871f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3bc6a3b-6858-403f-83d3-ee91d10445a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab54877-c225-40b3-be81-f2faf9871f70",
                    "LayerId": "aa562d9c-f4a7-45e8-9769-61df1afedd11"
                },
                {
                    "id": "c174d90b-e01a-44dd-b717-330caaf94ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab54877-c225-40b3-be81-f2faf9871f70",
                    "LayerId": "41d753b9-bbd1-4356-b372-47b93d7e0677"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "aa562d9c-f4a7-45e8-9769-61df1afedd11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d74cdd25-5553-4943-bd8e-14e3591b0fc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "41d753b9-bbd1-4356-b372-47b93d7e0677",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d74cdd25-5553-4943-bd8e-14e3591b0fc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}