{
    "id": "83882a84-2786-4657-ae53-e7202ed8c4eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSinConexion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c07ca3fd-b0bd-4d4d-87eb-df6d3e8a69cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83882a84-2786-4657-ae53-e7202ed8c4eb",
            "compositeImage": {
                "id": "f1b149b6-7821-47cf-819d-caa4a43bfb04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c07ca3fd-b0bd-4d4d-87eb-df6d3e8a69cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "305381e1-4c60-49af-8c19-773736736925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c07ca3fd-b0bd-4d4d-87eb-df6d3e8a69cd",
                    "LayerId": "a0b8a099-60ff-4a50-a407-dbad88df1559"
                },
                {
                    "id": "bace586b-1f92-422f-9c60-5c90882022fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c07ca3fd-b0bd-4d4d-87eb-df6d3e8a69cd",
                    "LayerId": "c69f9acd-5c51-46fd-a82e-6aa8e70ebbd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "a0b8a099-60ff-4a50-a407-dbad88df1559",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83882a84-2786-4657-ae53-e7202ed8c4eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c69f9acd-5c51-46fd-a82e-6aa8e70ebbd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83882a84-2786-4657-ae53-e7202ed8c4eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 66,
    "yorig": 21
}