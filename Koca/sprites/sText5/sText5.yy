{
    "id": "fcba6af1-2844-46b7-9e64-847b9c03d8ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 12,
    "bbox_right": 77,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1a0cd8c-01a0-4911-8f5d-30587dfa9a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcba6af1-2844-46b7-9e64-847b9c03d8ea",
            "compositeImage": {
                "id": "3c167d6d-6c3b-444c-a166-bd36dc338afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a0cd8c-01a0-4911-8f5d-30587dfa9a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84555f0b-8db3-4059-8c20-e63ae8bb83f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a0cd8c-01a0-4911-8f5d-30587dfa9a92",
                    "LayerId": "1f028a41-57f0-4881-b6ee-914561f8cea4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1f028a41-57f0-4881-b6ee-914561f8cea4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcba6af1-2844-46b7-9e64-847b9c03d8ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}