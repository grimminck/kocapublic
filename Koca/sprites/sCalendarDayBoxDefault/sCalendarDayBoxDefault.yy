{
    "id": "0f618753-9cf8-43d1-a2f8-71a2ea34931e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCalendarDayBoxDefault",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b738b12f-5ccc-4a5a-b703-ee79e412dbd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f618753-9cf8-43d1-a2f8-71a2ea34931e",
            "compositeImage": {
                "id": "b71f803e-a146-407a-be19-53d9fb1c6dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b738b12f-5ccc-4a5a-b703-ee79e412dbd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaeff8cc-590f-431d-a64b-43d0be572a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b738b12f-5ccc-4a5a-b703-ee79e412dbd9",
                    "LayerId": "7afe9eca-b0c2-4d6b-bb0b-af2851cc4ca1"
                },
                {
                    "id": "f35efef4-014e-427a-be7e-aae9c63fa93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b738b12f-5ccc-4a5a-b703-ee79e412dbd9",
                    "LayerId": "2bd73e36-67ca-4859-92e1-c2ff202a9f06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7afe9eca-b0c2-4d6b-bb0b-af2851cc4ca1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f618753-9cf8-43d1-a2f8-71a2ea34931e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2bd73e36-67ca-4859-92e1-c2ff202a9f06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f618753-9cf8-43d1-a2f8-71a2ea34931e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 12
}