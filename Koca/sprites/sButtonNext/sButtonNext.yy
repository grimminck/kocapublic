{
    "id": "8331f90d-09ad-4222-a768-23ec9acde2d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonNext",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85662127-147f-41bd-834f-5a8fdf8baa3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8331f90d-09ad-4222-a768-23ec9acde2d0",
            "compositeImage": {
                "id": "8ddb90f0-501d-42f5-bf79-57d9f2485907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85662127-147f-41bd-834f-5a8fdf8baa3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c7fa60-9b27-4038-a2b5-230e237a96b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85662127-147f-41bd-834f-5a8fdf8baa3e",
                    "LayerId": "3cf17c41-7f4a-447f-89a4-a1142a772039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "3cf17c41-7f4a-447f-89a4-a1142a772039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8331f90d-09ad-4222-a768-23ec9acde2d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 29,
    "yorig": 29
}