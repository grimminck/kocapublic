{
    "id": "38d48599-8d5a-4d25-a9f9-409d69a6d866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketReady",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 279,
    "bbox_right": 424,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99602b67-4430-4835-a419-78bee29a700b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d48599-8d5a-4d25-a9f9-409d69a6d866",
            "compositeImage": {
                "id": "47fc99e0-ece3-4ffa-8880-daecf4382d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99602b67-4430-4835-a419-78bee29a700b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac678e2-8ed9-49bb-b60d-1fb97431f1d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99602b67-4430-4835-a419-78bee29a700b",
                    "LayerId": "27f4cf45-4f6b-48ac-9e1e-8f4590a5748d"
                },
                {
                    "id": "280abc9f-704b-49a5-b705-51d14264e174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99602b67-4430-4835-a419-78bee29a700b",
                    "LayerId": "e7391cde-bee6-40ae-8a9f-955f4260ef50"
                },
                {
                    "id": "d8b0344b-126a-44da-8ef9-26cd825cc987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99602b67-4430-4835-a419-78bee29a700b",
                    "LayerId": "6f94e985-c013-4754-add8-ab8cb6d4ad0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "27f4cf45-4f6b-48ac-9e1e-8f4590a5748d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d48599-8d5a-4d25-a9f9-409d69a6d866",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e7391cde-bee6-40ae-8a9f-955f4260ef50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d48599-8d5a-4d25-a9f9-409d69a6d866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6f94e985-c013-4754-add8-ab8cb6d4ad0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d48599-8d5a-4d25-a9f9-409d69a6d866",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}