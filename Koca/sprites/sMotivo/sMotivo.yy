{
    "id": "25c61d94-601e-4d5b-a066-d4a10477c70e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMotivo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 437,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f20cb28-271e-45cf-ad77-c323951a3430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25c61d94-601e-4d5b-a066-d4a10477c70e",
            "compositeImage": {
                "id": "3d385ce2-02ac-4a9d-8421-d3d13cd43a00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f20cb28-271e-45cf-ad77-c323951a3430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a320ff5-9ac2-4bb0-817d-cda34bd16154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f20cb28-271e-45cf-ad77-c323951a3430",
                    "LayerId": "16ade97f-ce81-4f3e-b6b4-42e3ccdea3d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "16ade97f-ce81-4f3e-b6b4-42e3ccdea3d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25c61d94-601e-4d5b-a066-d4a10477c70e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 438,
    "xorig": 101,
    "yorig": 29
}