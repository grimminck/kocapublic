{
    "id": "d6f89899-dd9f-49c0-8194-95a19625dfed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketSelloPagar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 279,
    "bbox_right": 424,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1418c801-0f15-4d51-b01c-38fcf47c6461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f89899-dd9f-49c0-8194-95a19625dfed",
            "compositeImage": {
                "id": "d37997f9-33b4-4464-8ee3-b8e0e258d740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1418c801-0f15-4d51-b01c-38fcf47c6461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b86cc83-c33f-4cad-a274-f6a440e7aff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1418c801-0f15-4d51-b01c-38fcf47c6461",
                    "LayerId": "23910da8-6798-45a5-83d2-6700b8298046"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "23910da8-6798-45a5-83d2-6700b8298046",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6f89899-dd9f-49c0-8194-95a19625dfed",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}