{
    "id": "0e6990a8-97e5-401a-bf56-8f2cb4e0dd4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dias",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 437,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a464326-4e62-4d3b-9883-ed39fe876027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6990a8-97e5-401a-bf56-8f2cb4e0dd4d",
            "compositeImage": {
                "id": "e753c971-4089-44b3-b4e4-ec55161dd9b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a464326-4e62-4d3b-9883-ed39fe876027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b0cd92-27f4-4708-b353-6c2ced4ce129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a464326-4e62-4d3b-9883-ed39fe876027",
                    "LayerId": "e73814dc-928e-4e67-9c49-80681801ec4f"
                },
                {
                    "id": "9e87c457-648c-48cd-968d-61dc2bd5e24c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a464326-4e62-4d3b-9883-ed39fe876027",
                    "LayerId": "3a7c8e6c-28c9-429c-9349-b05277240d8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "3a7c8e6c-28c9-429c-9349-b05277240d8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6990a8-97e5-401a-bf56-8f2cb4e0dd4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e73814dc-928e-4e67-9c49-80681801ec4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6990a8-97e5-401a-bf56-8f2cb4e0dd4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 438,
    "xorig": 62,
    "yorig": 30
}