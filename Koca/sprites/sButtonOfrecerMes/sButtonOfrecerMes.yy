{
    "id": "c4188e5c-905b-4b20-8dac-b7b6267f491c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonOfrecerMes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ef3ef31-1dbf-4d90-a46a-046b1f6a3d69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4188e5c-905b-4b20-8dac-b7b6267f491c",
            "compositeImage": {
                "id": "03cae1f3-9e4d-43d5-8ef9-2a9c4e882825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef3ef31-1dbf-4d90-a46a-046b1f6a3d69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c76cc8-8fe0-439e-bd18-75b0096eb667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef3ef31-1dbf-4d90-a46a-046b1f6a3d69",
                    "LayerId": "888e85fc-d10d-442a-91b8-9377c6c114a5"
                },
                {
                    "id": "fb621e26-cf4b-4d15-9c7b-6c52c5a5a16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef3ef31-1dbf-4d90-a46a-046b1f6a3d69",
                    "LayerId": "f4f1d9d8-184f-4210-b3be-2c969618e3dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "888e85fc-d10d-442a-91b8-9377c6c114a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4188e5c-905b-4b20-8dac-b7b6267f491c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f4f1d9d8-184f-4210-b3be-2c969618e3dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4188e5c-905b-4b20-8dac-b7b6267f491c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 26,
    "yorig": 12
}