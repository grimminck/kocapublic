{
    "id": "071bdbed-6ef5-4ce6-8b35-76b163012223",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 9,
    "bbox_right": 46,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "851d5e83-e948-4175-9b67-ed4490b6c8e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "071bdbed-6ef5-4ce6-8b35-76b163012223",
            "compositeImage": {
                "id": "55a32c35-415a-47e2-b906-e19e2ff318a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "851d5e83-e948-4175-9b67-ed4490b6c8e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8aee6ab-aefa-4dc6-a211-1905a1dfd39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "851d5e83-e948-4175-9b67-ed4490b6c8e1",
                    "LayerId": "eade2e3c-6cf3-4232-b956-4a7b13e510a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eade2e3c-6cf3-4232-b956-4a7b13e510a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "071bdbed-6ef5-4ce6-8b35-76b163012223",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}