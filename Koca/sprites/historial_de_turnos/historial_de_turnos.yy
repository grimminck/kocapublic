{
    "id": "1ec4efa7-1069-4ad5-80a3-a6de55db02c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "historial_de_turnos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 0,
    "bbox_right": 124,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65fc95d5-ffab-4c12-a201-d9e1393d5788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec4efa7-1069-4ad5-80a3-a6de55db02c0",
            "compositeImage": {
                "id": "22a4b382-7409-4a6d-9f59-5e5b5b5a8352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65fc95d5-ffab-4c12-a201-d9e1393d5788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293ae2ec-0b18-4851-855c-d398c411b3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65fc95d5-ffab-4c12-a201-d9e1393d5788",
                    "LayerId": "abfefde0-fa8c-4fc0-ba90-4aa97483ae6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 153,
    "layers": [
        {
            "id": "abfefde0-fa8c-4fc0-ba90-4aa97483ae6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ec4efa7-1069-4ad5-80a3-a6de55db02c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 0,
    "yorig": 0
}