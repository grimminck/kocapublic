{
    "id": "2c49af76-f881-4e0a-b68b-a4756a2618aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonMotivo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31e8b15b-3404-4ea2-85cd-c15e74e1c57e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c49af76-f881-4e0a-b68b-a4756a2618aa",
            "compositeImage": {
                "id": "c49c5720-bf02-461d-914d-d75e561d5281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e8b15b-3404-4ea2-85cd-c15e74e1c57e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e5e4bb-9e1a-4e7d-a8eb-38a5db1bcee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e8b15b-3404-4ea2-85cd-c15e74e1c57e",
                    "LayerId": "eb07824d-f0ec-4252-a945-a59fbd185737"
                },
                {
                    "id": "75037f7d-d509-4c71-8206-6ecc13e6389a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e8b15b-3404-4ea2-85cd-c15e74e1c57e",
                    "LayerId": "02c5e0c6-fc0d-435b-8218-00fef7f30dbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eb07824d-f0ec-4252-a945-a59fbd185737",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c49af76-f881-4e0a-b68b-a4756a2618aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "02c5e0c6-fc0d-435b-8218-00fef7f30dbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c49af76-f881-4e0a-b68b-a4756a2618aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 25,
    "yorig": 12
}