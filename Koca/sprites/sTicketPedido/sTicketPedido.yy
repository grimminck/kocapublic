{
    "id": "f5f8c8a8-6f8a-4fe6-83f1-85d4c8919628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketPedido",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 94,
    "bbox_right": 323,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c0f4118-4479-4647-b9be-e868f013c703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5f8c8a8-6f8a-4fe6-83f1-85d4c8919628",
            "compositeImage": {
                "id": "70851769-da42-42f9-b8a0-2c3875919a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0f4118-4479-4647-b9be-e868f013c703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c12075-8b3f-4999-af5b-a9e62b14520d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0f4118-4479-4647-b9be-e868f013c703",
                    "LayerId": "f7b87cf5-6cfc-40c2-b466-a75394ce34f1"
                },
                {
                    "id": "3cc8cba7-49c5-453f-9c67-e951834187b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0f4118-4479-4647-b9be-e868f013c703",
                    "LayerId": "45fbf400-57fe-404e-86d7-c90166feb577"
                },
                {
                    "id": "1a31bd78-b568-4033-8a6d-1b9683100d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0f4118-4479-4647-b9be-e868f013c703",
                    "LayerId": "6505305a-394f-467d-9fc5-ed53ced43c5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "f7b87cf5-6cfc-40c2-b466-a75394ce34f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5f8c8a8-6f8a-4fe6-83f1-85d4c8919628",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 42,
            "visible": true
        },
        {
            "id": "45fbf400-57fe-404e-86d7-c90166feb577",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5f8c8a8-6f8a-4fe6-83f1-85d4c8919628",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 30,
            "visible": true
        },
        {
            "id": "6505305a-394f-467d-9fc5-ed53ced43c5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5f8c8a8-6f8a-4fe6-83f1-85d4c8919628",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 45,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}