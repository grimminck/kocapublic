{
    "id": "741b7d9b-dbd7-4e5b-b212-da738d44a798",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "a_sprite_default1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f517827-3829-4e90-b3b3-42d9cfe0218d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "741b7d9b-dbd7-4e5b-b212-da738d44a798",
            "compositeImage": {
                "id": "18db58fb-f2f8-4f18-b548-7c0be2c6a88d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f517827-3829-4e90-b3b3-42d9cfe0218d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a02fd6-f376-481a-af34-ccadd38c218f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f517827-3829-4e90-b3b3-42d9cfe0218d",
                    "LayerId": "51ffc67e-0281-4e8a-a234-624777be5807"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "51ffc67e-0281-4e8a-a234-624777be5807",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "741b7d9b-dbd7-4e5b-b212-da738d44a798",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}