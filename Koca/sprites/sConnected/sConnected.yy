{
    "id": "458252ec-7843-494f-b185-5d87ffb77029",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConnected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16ad5738-3e0f-46dc-9c70-21c3fb9d91b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458252ec-7843-494f-b185-5d87ffb77029",
            "compositeImage": {
                "id": "1a5d7862-dd2f-45d8-956c-3694d2770f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16ad5738-3e0f-46dc-9c70-21c3fb9d91b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b6f7d0-0310-402a-b3f7-c19f80a89acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16ad5738-3e0f-46dc-9c70-21c3fb9d91b9",
                    "LayerId": "ec98009b-49a0-4da3-a144-fc0280d0fc6c"
                }
            ]
        },
        {
            "id": "5e0c8d61-984e-4c29-a00f-5797712811b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458252ec-7843-494f-b185-5d87ffb77029",
            "compositeImage": {
                "id": "fe6f98bb-9921-48be-bf8a-8ed135325bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e0c8d61-984e-4c29-a00f-5797712811b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d303c99f-8478-4a64-a202-e3b0f7af1109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e0c8d61-984e-4c29-a00f-5797712811b5",
                    "LayerId": "ec98009b-49a0-4da3-a144-fc0280d0fc6c"
                }
            ]
        },
        {
            "id": "4f8cb654-343e-405b-8edf-81d5fae4e7b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458252ec-7843-494f-b185-5d87ffb77029",
            "compositeImage": {
                "id": "56ca7685-a1da-458f-991e-d8e7c819cc81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8cb654-343e-405b-8edf-81d5fae4e7b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c3ec2b-fc5d-4a16-8e50-7880cd94209f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8cb654-343e-405b-8edf-81d5fae4e7b9",
                    "LayerId": "ec98009b-49a0-4da3-a144-fc0280d0fc6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ec98009b-49a0-4da3-a144-fc0280d0fc6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "458252ec-7843-494f-b185-5d87ffb77029",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}