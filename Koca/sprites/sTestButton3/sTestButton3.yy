{
    "id": "ce65cc1d-af7b-48eb-8466-5893a0a82de0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTestButton3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73129a1b-ec22-4b73-aac2-17384b2241de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce65cc1d-af7b-48eb-8466-5893a0a82de0",
            "compositeImage": {
                "id": "a402bd7b-f8b9-4236-8ea4-254f32fb32e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73129a1b-ec22-4b73-aac2-17384b2241de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c10a5e1-6e89-4522-9d36-fef698bd423a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73129a1b-ec22-4b73-aac2-17384b2241de",
                    "LayerId": "d15aba06-504b-464f-b38d-db107209a7ee"
                },
                {
                    "id": "4a22833d-a6d5-4969-bba0-230109686dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73129a1b-ec22-4b73-aac2-17384b2241de",
                    "LayerId": "fb82e915-b48e-4407-b8db-c6b7de77c2ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "d15aba06-504b-464f-b38d-db107209a7ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce65cc1d-af7b-48eb-8466-5893a0a82de0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fb82e915-b48e-4407-b8db-c6b7de77c2ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce65cc1d-af7b-48eb-8466-5893a0a82de0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 113,
    "yorig": 116
}