{
    "id": "68f732db-3b57-4e88-a464-3a3ff6e61a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArrowLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 21,
    "bbox_right": 117,
    "bbox_top": 44,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36569f2b-b27e-451d-99d1-edf20c27f6bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68f732db-3b57-4e88-a464-3a3ff6e61a5c",
            "compositeImage": {
                "id": "4780bfec-a9be-4906-838c-f3915721a9c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36569f2b-b27e-451d-99d1-edf20c27f6bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6233c94b-ba2e-41e2-8232-e6df3ae5f6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36569f2b-b27e-451d-99d1-edf20c27f6bd",
                    "LayerId": "aa8c8e89-1b1e-4286-9591-9cd504f67119"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aa8c8e89-1b1e-4286-9591-9cd504f67119",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68f732db-3b57-4e88-a464-3a3ff6e61a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}