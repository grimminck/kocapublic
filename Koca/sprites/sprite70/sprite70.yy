{
    "id": "4ae9c308-1cf8-4c2a-8a5a-74d9ae686c7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite70",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dc1341b-de9b-4ad1-aa2b-9c73d9643bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae9c308-1cf8-4c2a-8a5a-74d9ae686c7a",
            "compositeImage": {
                "id": "63b4da9d-561c-49af-8248-45165c0f8813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc1341b-de9b-4ad1-aa2b-9c73d9643bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0cd6287-8fae-4f94-a1de-e7b66cd8fcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc1341b-de9b-4ad1-aa2b-9c73d9643bd6",
                    "LayerId": "988a1544-6f8d-429b-ae50-fe287f9c9c77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 102,
    "layers": [
        {
            "id": "988a1544-6f8d-429b-ae50-fe287f9c9c77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ae9c308-1cf8-4c2a-8a5a-74d9ae686c7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 51,
    "yorig": 51
}