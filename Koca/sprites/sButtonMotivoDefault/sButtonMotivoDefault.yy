{
    "id": "3420ed7d-9e37-4814-a27e-0d3c3fae7002",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonMotivoDefault",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ef20305-b3f2-4f8d-b78f-495ef95cfb80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3420ed7d-9e37-4814-a27e-0d3c3fae7002",
            "compositeImage": {
                "id": "f1e2d326-437f-403c-a139-bf727aa7ff29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ef20305-b3f2-4f8d-b78f-495ef95cfb80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324837f0-53f9-4642-bdeb-22d1c71f142f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ef20305-b3f2-4f8d-b78f-495ef95cfb80",
                    "LayerId": "4c3ffa4c-bb3c-4745-9ee1-b71012f38165"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4c3ffa4c-bb3c-4745-9ee1-b71012f38165",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3420ed7d-9e37-4814-a27e-0d3c3fae7002",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 5,
    "yorig": 13
}