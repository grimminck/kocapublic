{
    "id": "b6a9200d-79a5-4615-8751-da6b0d10efc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonReturn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95f9473e-b91d-48bb-9166-76bd7689bc9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6a9200d-79a5-4615-8751-da6b0d10efc5",
            "compositeImage": {
                "id": "7e17372a-4ac6-4be5-8ed1-41a9a7d364bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95f9473e-b91d-48bb-9166-76bd7689bc9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7fdb14-7560-4bb6-9c6e-5fb949792d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f9473e-b91d-48bb-9166-76bd7689bc9d",
                    "LayerId": "aef0a992-382b-46a5-b0fe-4646560e0424"
                },
                {
                    "id": "fe789d41-59c3-465b-a20c-b0c145a1ff27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f9473e-b91d-48bb-9166-76bd7689bc9d",
                    "LayerId": "4c18b3ad-1f42-4fa4-83e7-9a257a68ea3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aef0a992-382b-46a5-b0fe-4646560e0424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6a9200d-79a5-4615-8751-da6b0d10efc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4c18b3ad-1f42-4fa4-83e7-9a257a68ea3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6a9200d-79a5-4615-8751-da6b0d10efc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 34
}