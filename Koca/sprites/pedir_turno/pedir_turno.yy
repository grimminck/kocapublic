{
    "id": "d9f4f0e4-be02-48fd-abfa-57dac07a4fb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pedir_turno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 220,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "675eb6e1-4c94-4471-a4bd-afa2a6e4f27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f4f0e4-be02-48fd-abfa-57dac07a4fb4",
            "compositeImage": {
                "id": "01136a54-3c77-4d38-a420-bda09fe7d2fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675eb6e1-4c94-4471-a4bd-afa2a6e4f27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d11e7406-9278-4218-9fc1-f3710327cf44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675eb6e1-4c94-4471-a4bd-afa2a6e4f27b",
                    "LayerId": "79fd1294-9b6a-487e-855f-6bad2624fe22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 234,
    "layers": [
        {
            "id": "79fd1294-9b6a-487e-855f-6bad2624fe22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9f4f0e4-be02-48fd-abfa-57dac07a4fb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 243,
    "xorig": 111,
    "yorig": 111
}