{
    "id": "b52479a7-710a-43bc-84a9-8388d9e2581f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "signo_exclamacion_celeste",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27d93706-f1d1-409c-9092-1bee39c5772d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b52479a7-710a-43bc-84a9-8388d9e2581f",
            "compositeImage": {
                "id": "39c83ef3-6f41-4e1d-9555-7e0a6ee58c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d93706-f1d1-409c-9092-1bee39c5772d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88b289ff-eddd-4613-8ae3-7dcba29d198a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d93706-f1d1-409c-9092-1bee39c5772d",
                    "LayerId": "e5173c03-58b2-4517-9deb-3175997d3274"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "e5173c03-58b2-4517-9deb-3175997d3274",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b52479a7-710a-43bc-84a9-8388d9e2581f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}