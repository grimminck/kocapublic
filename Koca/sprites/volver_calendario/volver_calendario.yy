{
    "id": "a7bd95d2-6eea-4074-a2ba-7f361323d122",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "volver_calendario",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 217,
    "bbox_left": 0,
    "bbox_right": 217,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3bc0a73-783e-4861-a003-c10ff7f01211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7bd95d2-6eea-4074-a2ba-7f361323d122",
            "compositeImage": {
                "id": "1fa552f7-52e8-4b0e-8bb3-058070ecffb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bc0a73-783e-4861-a003-c10ff7f01211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4509049b-1f92-4837-886f-52a1c41e9681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bc0a73-783e-4861-a003-c10ff7f01211",
                    "LayerId": "a577e2f7-e06f-48a1-84cf-3447ebd8d804"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 218,
    "layers": [
        {
            "id": "a577e2f7-e06f-48a1-84cf-3447ebd8d804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7bd95d2-6eea-4074-a2ba-7f361323d122",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 218,
    "xorig": 109,
    "yorig": 109
}