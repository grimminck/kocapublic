{
    "id": "bacf1224-cb80-464f-b104-f71d6d5346d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "signo_exclamacionfuxia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 94,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1e2bb65-fae4-4a37-8e87-59db6f3b7386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bacf1224-cb80-464f-b104-f71d6d5346d9",
            "compositeImage": {
                "id": "4a016f10-905d-43a3-b55b-e871ca6a4bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e2bb65-fae4-4a37-8e87-59db6f3b7386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d628bae9-05f7-4ae7-9fa6-be91e234017b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e2bb65-fae4-4a37-8e87-59db6f3b7386",
                    "LayerId": "3e3bbeb4-6b03-4544-9664-b6818a5ac3b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "3e3bbeb4-6b03-4544-9664-b6818a5ac3b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bacf1224-cb80-464f-b104-f71d6d5346d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 47,
    "yorig": 47
}