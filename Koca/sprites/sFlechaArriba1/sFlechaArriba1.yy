{
    "id": "94c44984-5c38-4f65-9231-c901474a8733",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlechaArriba1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d2beb5b-78d6-41e0-94df-89f9fbaab185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94c44984-5c38-4f65-9231-c901474a8733",
            "compositeImage": {
                "id": "39c7d9c8-0b30-4344-92e9-a202fa339f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2beb5b-78d6-41e0-94df-89f9fbaab185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecfb3e9e-57d3-4b1f-93a5-3903f64218d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2beb5b-78d6-41e0-94df-89f9fbaab185",
                    "LayerId": "54fea97a-3709-46f1-a4a3-46b4e83e5066"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "54fea97a-3709-46f1-a4a3-46b4e83e5066",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94c44984-5c38-4f65-9231-c901474a8733",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 45,
    "yorig": 45
}