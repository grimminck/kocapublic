{
    "id": "1e0a6f04-fac8-4c14-bc35-d1d65e20f397",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonPersonal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "178480bd-06c7-40d6-a472-4943fa41fff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0a6f04-fac8-4c14-bc35-d1d65e20f397",
            "compositeImage": {
                "id": "d2d99f27-b398-49ef-9d25-d4dcedf80d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178480bd-06c7-40d6-a472-4943fa41fff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8a94de-acc7-4d9b-93c5-6984dc3e0d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178480bd-06c7-40d6-a472-4943fa41fff0",
                    "LayerId": "0eb3f8c6-bdc4-4e4d-ab53-6115c0b70ab9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0eb3f8c6-bdc4-4e4d-ab53-6115c0b70ab9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e0a6f04-fac8-4c14-bc35-d1d65e20f397",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 62,
    "yorig": 70
}