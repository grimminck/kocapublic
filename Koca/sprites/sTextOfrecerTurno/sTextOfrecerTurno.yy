{
    "id": "4f9cefa7-5ef3-4b63-bbd5-6e6acdfa7cfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextOfrecerTurno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 73,
    "bbox_right": 276,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fe63eed-82f9-43f3-98ec-304262f1b19c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9cefa7-5ef3-4b63-bbd5-6e6acdfa7cfb",
            "compositeImage": {
                "id": "337ac2d2-ea32-40aa-ac96-b41a3b96e1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe63eed-82f9-43f3-98ec-304262f1b19c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0edb8ebe-b8b3-4dac-9de8-91dc174661c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe63eed-82f9-43f3-98ec-304262f1b19c",
                    "LayerId": "c97c8c3e-b145-43fb-b652-bfc2fee46c86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c97c8c3e-b145-43fb-b652-bfc2fee46c86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f9cefa7-5ef3-4b63-bbd5-6e6acdfa7cfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 168,
    "yorig": 35
}