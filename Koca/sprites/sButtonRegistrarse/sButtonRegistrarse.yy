{
    "id": "ff8cc488-0f30-4ab2-aff9-0a58360e2d1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonRegistrarse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ff69149-6efd-4686-9bb5-12de0f0159d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff8cc488-0f30-4ab2-aff9-0a58360e2d1d",
            "compositeImage": {
                "id": "04369511-b7aa-4637-bc23-687b48c558be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff69149-6efd-4686-9bb5-12de0f0159d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c4c89cd-68fa-446c-917c-6a4e678534d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff69149-6efd-4686-9bb5-12de0f0159d6",
                    "LayerId": "d8c365c6-d999-4523-86f5-026c6ef44a80"
                },
                {
                    "id": "337809bb-8949-42e1-920f-fd4d2bdaedc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff69149-6efd-4686-9bb5-12de0f0159d6",
                    "LayerId": "e50b615f-d26a-4703-ba33-f17c97f2d36e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "d8c365c6-d999-4523-86f5-026c6ef44a80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff8cc488-0f30-4ab2-aff9-0a58360e2d1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e50b615f-d26a-4703-ba33-f17c97f2d36e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff8cc488-0f30-4ab2-aff9-0a58360e2d1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 73,
    "yorig": 22
}