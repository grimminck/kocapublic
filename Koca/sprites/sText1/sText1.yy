{
    "id": "d0ff2892-44eb-4654-b1f9-37e17e582254",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 8,
    "bbox_right": 49,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8eac4041-a09e-480c-b5ee-77939fed854c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0ff2892-44eb-4654-b1f9-37e17e582254",
            "compositeImage": {
                "id": "97e4d632-097a-4341-a9c1-419c916d5147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eac4041-a09e-480c-b5ee-77939fed854c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93031273-7148-43f5-855d-153c3fc4edd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eac4041-a09e-480c-b5ee-77939fed854c",
                    "LayerId": "6a127525-a3a2-4a62-adc1-6b1b5ff38eb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6a127525-a3a2-4a62-adc1-6b1b5ff38eb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0ff2892-44eb-4654-b1f9-37e17e582254",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}