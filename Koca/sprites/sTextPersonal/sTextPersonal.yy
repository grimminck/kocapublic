{
    "id": "d7b76796-35d3-4d2c-8ad9-61b1f00a0c29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextPersonal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 89,
    "bbox_right": 219,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17a22560-d842-4c4f-a236-05f9eb485151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7b76796-35d3-4d2c-8ad9-61b1f00a0c29",
            "compositeImage": {
                "id": "6eaa885f-5dcc-4ecb-a817-eb9f49a704f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a22560-d842-4c4f-a236-05f9eb485151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a66543-d8ba-4d8b-988b-f8da440f51ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a22560-d842-4c4f-a236-05f9eb485151",
                    "LayerId": "3572aeef-f724-4514-8efc-64a135ef8deb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3572aeef-f724-4514-8efc-64a135ef8deb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7b76796-35d3-4d2c-8ad9-61b1f00a0c29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 157,
    "yorig": 33
}