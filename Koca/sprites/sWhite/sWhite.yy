{
    "id": "eb76099a-5251-4f0a-b315-0e10733f0e77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cc70a71-d9fc-45ba-8f89-24fde19434a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb76099a-5251-4f0a-b315-0e10733f0e77",
            "compositeImage": {
                "id": "cbb802be-159a-4c4d-a73a-0d0a03c3d6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc70a71-d9fc-45ba-8f89-24fde19434a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961f567a-75e3-474e-8c4f-c37e08d98c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc70a71-d9fc-45ba-8f89-24fde19434a2",
                    "LayerId": "5f898ec5-705f-4916-9b64-bfa4fb9600c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5f898ec5-705f-4916-9b64-bfa4fb9600c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb76099a-5251-4f0a-b315-0e10733f0e77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}