{
    "id": "3812f5ed-2b08-4329-b429-6a502b8181bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sModoMaestro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10470f1b-a76e-4052-878a-edc5bd53e877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3812f5ed-2b08-4329-b429-6a502b8181bd",
            "compositeImage": {
                "id": "82ea3477-0774-4b0b-9ef9-0c763d4be4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10470f1b-a76e-4052-878a-edc5bd53e877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0c4f0b-29ca-415d-95a7-fc88dcbb193d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10470f1b-a76e-4052-878a-edc5bd53e877",
                    "LayerId": "750b387f-f8dd-49aa-95ff-066dc3a9aed0"
                },
                {
                    "id": "f0c3a27d-b880-4db5-b914-333aa576a8a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10470f1b-a76e-4052-878a-edc5bd53e877",
                    "LayerId": "c183ebd8-17c8-4e9a-9f49-6f1074554332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "c183ebd8-17c8-4e9a-9f49-6f1074554332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3812f5ed-2b08-4329-b429-6a502b8181bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "750b387f-f8dd-49aa-95ff-066dc3a9aed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3812f5ed-2b08-4329-b429-6a502b8181bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 66,
    "yorig": 21
}