{
    "id": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 0,
    "bbox_right": 422,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4192ca01-2710-46f9-9582-89bc9f92da79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
            "compositeImage": {
                "id": "d1fc077f-6f19-433e-99be-2284572eb717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4192ca01-2710-46f9-9582-89bc9f92da79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe805528-fc84-4f29-ad4e-920fb294725b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4192ca01-2710-46f9-9582-89bc9f92da79",
                    "LayerId": "6acd606d-c7a8-434e-922e-3f9dba220e60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 152,
    "layers": [
        {
            "id": "6acd606d-c7a8-434e-922e-3f9dba220e60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e7ec13a-6fc2-423f-bad7-84b989fbbeec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 423,
    "xorig": 0,
    "yorig": 0
}