{
    "id": "e3cff8f4-46f8-4fbb-b5ea-6ce3b3925548",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArrowRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 22,
    "bbox_right": 113,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "071e23ad-ad34-4a37-af3c-ffe835f3afc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3cff8f4-46f8-4fbb-b5ea-6ce3b3925548",
            "compositeImage": {
                "id": "171960a1-4838-49a2-a154-ede115703b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "071e23ad-ad34-4a37-af3c-ffe835f3afc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7d99e9-6e78-4991-a214-7964033c5707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "071e23ad-ad34-4a37-af3c-ffe835f3afc0",
                    "LayerId": "9ebe5d11-b9aa-419c-9247-cd79c33ff3b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9ebe5d11-b9aa-419c-9247-cd79c33ff3b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3cff8f4-46f8-4fbb-b5ea-6ce3b3925548",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}