{
    "id": "e9283584-fa6c-4315-ac0a-db5f9ab16053",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1151",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 206,
    "bbox_left": 58,
    "bbox_right": 194,
    "bbox_top": 71,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30d5f9d0-a921-4b9b-bdcf-8cd3a35cfaba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9283584-fa6c-4315-ac0a-db5f9ab16053",
            "compositeImage": {
                "id": "4e05c6db-26fe-46fd-a65b-fb90c9ff326a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d5f9d0-a921-4b9b-bdcf-8cd3a35cfaba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671d7f92-f96e-4d8a-bd83-3348923ef7b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d5f9d0-a921-4b9b-bdcf-8cd3a35cfaba",
                    "LayerId": "ee4b8b88-022a-4ccc-8c96-0e48811a9f30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ee4b8b88-022a-4ccc-8c96-0e48811a9f30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9283584-fa6c-4315-ac0a-db5f9ab16053",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}