{
    "id": "05f84aab-a439-4440-a267-8ea736874756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d8d0132-f12f-49cf-b3aa-869f6bc1b619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f84aab-a439-4440-a267-8ea736874756",
            "compositeImage": {
                "id": "c6b4971d-49f0-423d-8e92-b66254190da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8d0132-f12f-49cf-b3aa-869f6bc1b619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c1ecaaa-cde0-4371-bfda-bbd02ea1fa36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8d0132-f12f-49cf-b3aa-869f6bc1b619",
                    "LayerId": "76c5e0d9-e893-4c07-a934-5fd494201d2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "76c5e0d9-e893-4c07-a934-5fd494201d2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f84aab-a439-4440-a267-8ea736874756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 34
}