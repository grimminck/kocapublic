{
    "id": "db0b0cb4-76ea-4b80-9a44-e1943a587e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTestButton2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36a5cb40-5a45-41f1-8bde-f27d74cde875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0b0cb4-76ea-4b80-9a44-e1943a587e9e",
            "compositeImage": {
                "id": "01efae5f-a343-42d3-9cc8-ef922387b9d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a5cb40-5a45-41f1-8bde-f27d74cde875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ae475e-5abf-4456-94db-7d0967955744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a5cb40-5a45-41f1-8bde-f27d74cde875",
                    "LayerId": "c8027aa5-8361-48c2-a234-5fe0b55400b2"
                },
                {
                    "id": "8d9bb03d-5d5f-42b1-9514-507e3e4ae290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a5cb40-5a45-41f1-8bde-f27d74cde875",
                    "LayerId": "e35e3f13-e914-4e3d-a83f-af5cc8e8e364"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "c8027aa5-8361-48c2-a234-5fe0b55400b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0b0cb4-76ea-4b80-9a44-e1943a587e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e35e3f13-e914-4e3d-a83f-af5cc8e8e364",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0b0cb4-76ea-4b80-9a44-e1943a587e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 113,
    "yorig": 116
}