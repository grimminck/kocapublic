{
    "id": "eb1645d1-9cad-47fa-823f-b182ad42b867",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dias1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f63a20ca-9be1-4d26-b9af-fb440cd63c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1645d1-9cad-47fa-823f-b182ad42b867",
            "compositeImage": {
                "id": "785713dc-9e1b-4eda-8258-a71bcf49bd0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63a20ca-9be1-4d26-b9af-fb440cd63c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96504870-40b0-495f-8b39-e6c1d5482cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63a20ca-9be1-4d26-b9af-fb440cd63c67",
                    "LayerId": "e5bd39fc-9562-45e4-857b-3ef64b3a156c"
                },
                {
                    "id": "2b9c1b66-becf-4546-9b3a-e8715586bb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63a20ca-9be1-4d26-b9af-fb440cd63c67",
                    "LayerId": "bff3cae0-ff07-4115-ba32-3acae9618875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "e5bd39fc-9562-45e4-857b-3ef64b3a156c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb1645d1-9cad-47fa-823f-b182ad42b867",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bff3cae0-ff07-4115-ba32-3acae9618875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb1645d1-9cad-47fa-823f-b182ad42b867",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 84,
    "yorig": 36
}