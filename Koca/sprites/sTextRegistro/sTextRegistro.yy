{
    "id": "8c24e7ac-bb93-44b4-aa6c-3331b3d2a8a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextRegistro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 65,
    "bbox_right": 280,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8232a21f-5c05-45dd-8173-ed808c8ec5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c24e7ac-bb93-44b4-aa6c-3331b3d2a8a1",
            "compositeImage": {
                "id": "55904c31-480e-481c-8e4c-db589e719886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8232a21f-5c05-45dd-8173-ed808c8ec5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18305c1-23e3-43b8-b2c9-7fe1ed8672da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8232a21f-5c05-45dd-8173-ed808c8ec5a2",
                    "LayerId": "f5dac1fd-8ffc-413b-9c5b-63ab4c37aad3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f5dac1fd-8ffc-413b-9c5b-63ab4c37aad3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c24e7ac-bb93-44b4-aa6c-3331b3d2a8a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 168,
    "yorig": 35
}