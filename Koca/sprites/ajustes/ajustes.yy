{
    "id": "0b238d1c-ae6c-4805-b02b-3fa0eb6d74f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ajustes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc5332fb-22dd-4445-85aa-f352ebd520c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b238d1c-ae6c-4805-b02b-3fa0eb6d74f1",
            "compositeImage": {
                "id": "55661c8e-215e-4573-8708-716c10b27ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5332fb-22dd-4445-85aa-f352ebd520c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8690e63f-9ae2-4d30-8d73-39917d2e20db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5332fb-22dd-4445-85aa-f352ebd520c5",
                    "LayerId": "cd50be9e-b381-4cd6-bff6-182992780561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "cd50be9e-b381-4cd6-bff6-182992780561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b238d1c-ae6c-4805-b02b-3fa0eb6d74f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 33
}