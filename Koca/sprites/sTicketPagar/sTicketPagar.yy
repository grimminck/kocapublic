{
    "id": "3bdcf15e-f77e-4256-800e-cf9c9e770737",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTicketPagar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 279,
    "bbox_right": 424,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fea23c1-b8c5-4226-81d5-014ae6266cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bdcf15e-f77e-4256-800e-cf9c9e770737",
            "compositeImage": {
                "id": "9c0b2a6a-62b9-4e53-abab-9251f096724a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fea23c1-b8c5-4226-81d5-014ae6266cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116160dc-c264-4c68-92b6-74dc4fd027b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fea23c1-b8c5-4226-81d5-014ae6266cb9",
                    "LayerId": "08848f94-3b41-4faa-b5b1-1314bf9ee172"
                },
                {
                    "id": "686fafcb-672d-4b2a-8e48-9463cce6c9de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fea23c1-b8c5-4226-81d5-014ae6266cb9",
                    "LayerId": "e3b1413f-2f0a-4ffc-b1ac-8fef8135e784"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "08848f94-3b41-4faa-b5b1-1314bf9ee172",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bdcf15e-f77e-4256-800e-cf9c9e770737",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e3b1413f-2f0a-4ffc-b1ac-8fef8135e784",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bdcf15e-f77e-4256-800e-cf9c9e770737",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}