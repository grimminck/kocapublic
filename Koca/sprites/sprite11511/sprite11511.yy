{
    "id": "ba9387f1-14dc-43b5-8c5b-c37a54337a5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11511",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 206,
    "bbox_left": 58,
    "bbox_right": 194,
    "bbox_top": 71,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c49df9e4-7747-45dd-a419-10aa81316df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9387f1-14dc-43b5-8c5b-c37a54337a5e",
            "compositeImage": {
                "id": "35b84022-7c28-4d09-90f0-cc8620f68521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49df9e4-7747-45dd-a419-10aa81316df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3fec851-0b2e-49f5-b56d-b52a022509bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49df9e4-7747-45dd-a419-10aa81316df2",
                    "LayerId": "0ff15cf6-da0e-4a15-9939-08390daf1169"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0ff15cf6-da0e-4a15-9939-08390daf1169",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba9387f1-14dc-43b5-8c5b-c37a54337a5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}