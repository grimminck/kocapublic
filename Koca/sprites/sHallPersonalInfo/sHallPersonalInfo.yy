{
    "id": "b9d16f51-39b5-4b0b-924e-489f582fb58a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHallPersonalInfo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 0,
    "bbox_right": 289,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "633f268b-eeef-4bd3-a60d-65bbbdf6d1fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9d16f51-39b5-4b0b-924e-489f582fb58a",
            "compositeImage": {
                "id": "15cc70ad-7857-430b-8c78-8ba02c848baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "633f268b-eeef-4bd3-a60d-65bbbdf6d1fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672fd790-8683-4282-9b98-f7703e68812c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "633f268b-eeef-4bd3-a60d-65bbbdf6d1fb",
                    "LayerId": "b1550be5-dcb7-42e0-b3e6-5092ebf862ac"
                },
                {
                    "id": "896b20cd-7768-4b1a-95fc-9f8879c5dbd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "633f268b-eeef-4bd3-a60d-65bbbdf6d1fb",
                    "LayerId": "ad46edc1-e5f0-457e-a97c-6f9822e3eb7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 254,
    "layers": [
        {
            "id": "ad46edc1-e5f0-457e-a97c-6f9822e3eb7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9d16f51-39b5-4b0b-924e-489f582fb58a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b1550be5-dcb7-42e0-b3e6-5092ebf862ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9d16f51-39b5-4b0b-924e-489f582fb58a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 290,
    "xorig": 229,
    "yorig": 228
}