{
    "id": "4e54e59e-fbcb-4924-acc5-bdf6ce66ebe2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOnePixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e91c8a56-4588-4669-9373-014b991548f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e54e59e-fbcb-4924-acc5-bdf6ce66ebe2",
            "compositeImage": {
                "id": "56348d7b-d7f9-4f81-b68c-91320a9374e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91c8a56-4588-4669-9373-014b991548f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c90c3b-b5c4-48a9-b706-58cef378d427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91c8a56-4588-4669-9373-014b991548f2",
                    "LayerId": "e0beac2e-a762-4ea8-9ad3-f4e627d2854e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "e0beac2e-a762-4ea8-9ad3-f4e627d2854e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e54e59e-fbcb-4924-acc5-bdf6ce66ebe2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}