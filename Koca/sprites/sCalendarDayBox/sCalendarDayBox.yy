{
    "id": "7ae14888-9a3c-45bc-8721-998de121e629",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCalendarDayBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43db385e-1fab-4471-a46c-895bd5ed0e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ae14888-9a3c-45bc-8721-998de121e629",
            "compositeImage": {
                "id": "c619cec6-c856-4900-8150-4cf8a602abbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43db385e-1fab-4471-a46c-895bd5ed0e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ccf4ae-477c-4c6d-aa4e-7005209e9bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43db385e-1fab-4471-a46c-895bd5ed0e53",
                    "LayerId": "a3bf38f8-2901-478a-b7d5-05c9e9ee4cc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a3bf38f8-2901-478a-b7d5-05c9e9ee4cc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ae14888-9a3c-45bc-8721-998de121e629",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 52,
    "xorig": 2,
    "yorig": 2
}