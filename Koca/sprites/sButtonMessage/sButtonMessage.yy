{
    "id": "5a6557b6-fd86-47a8-b9b5-e65992bdd165",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonMessage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9ec8ab1-b65d-471d-a0c2-b5569cef1eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a6557b6-fd86-47a8-b9b5-e65992bdd165",
            "compositeImage": {
                "id": "788e0a31-4528-440d-b086-056c15cd418e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ec8ab1-b65d-471d-a0c2-b5569cef1eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c298ca-76b9-4a2b-b452-f967b6566b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ec8ab1-b65d-471d-a0c2-b5569cef1eac",
                    "LayerId": "b2d0867c-e2d3-420b-b976-999dd5d27c56"
                },
                {
                    "id": "612e1350-24ca-4bcd-9d0d-5a54e2291520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ec8ab1-b65d-471d-a0c2-b5569cef1eac",
                    "LayerId": "c6e2552f-a200-41de-ab69-a2bdfba4a78d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b2d0867c-e2d3-420b-b976-999dd5d27c56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a6557b6-fd86-47a8-b9b5-e65992bdd165",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c6e2552f-a200-41de-ab69-a2bdfba4a78d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a6557b6-fd86-47a8-b9b5-e65992bdd165",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 34
}