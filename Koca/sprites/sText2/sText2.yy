{
    "id": "940952fd-a0f3-44e5-a3b3-165e1f006127",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sText2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 8,
    "bbox_right": 167,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99c56cca-8f86-4dee-9f65-d574ffbb65ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "940952fd-a0f3-44e5-a3b3-165e1f006127",
            "compositeImage": {
                "id": "a2809879-cded-48c8-9787-1538b7f5c6e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c56cca-8f86-4dee-9f65-d574ffbb65ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04dc8998-5e6d-4e7c-97cd-1e51023f5a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c56cca-8f86-4dee-9f65-d574ffbb65ba",
                    "LayerId": "369511f9-bd9b-4b0a-b96c-52d634eda71e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "369511f9-bd9b-4b0a-b96c-52d634eda71e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "940952fd-a0f3-44e5-a3b3-165e1f006127",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}