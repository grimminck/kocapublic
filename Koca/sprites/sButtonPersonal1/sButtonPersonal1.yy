{
    "id": "5a1888a0-87bf-4702-a3a2-ae99d9bd8588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonPersonal1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7c97463-1e9f-4f7c-818b-9814bd2b300c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a1888a0-87bf-4702-a3a2-ae99d9bd8588",
            "compositeImage": {
                "id": "c4a4c0c4-07fb-4b85-aae0-710d9c180122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7c97463-1e9f-4f7c-818b-9814bd2b300c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d74fb77-9907-4d93-9696-310e11872c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c97463-1e9f-4f7c-818b-9814bd2b300c",
                    "LayerId": "c1960b82-83a5-4002-a861-6f39196e931a"
                },
                {
                    "id": "44e82e69-47dd-4d06-9d08-f646618b5d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c97463-1e9f-4f7c-818b-9814bd2b300c",
                    "LayerId": "9162d2f0-a6d6-4f12-9ac8-4fbe788d1863"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9162d2f0-a6d6-4f12-9ac8-4fbe788d1863",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a1888a0-87bf-4702-a3a2-ae99d9bd8588",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c1960b82-83a5-4002-a861-6f39196e931a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a1888a0-87bf-4702-a3a2-ae99d9bd8588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}