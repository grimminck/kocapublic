{
    "id": "700674dc-296e-4db9-9658-943005d93df9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "horario",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 437,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f9d1ba8-3c06-468c-a7e9-d8b327af1ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700674dc-296e-4db9-9658-943005d93df9",
            "compositeImage": {
                "id": "253d847e-b223-484f-afb4-8ed798294005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9d1ba8-3c06-468c-a7e9-d8b327af1ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f80398-d8e2-4058-b5e3-ce1c9aaffc3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9d1ba8-3c06-468c-a7e9-d8b327af1ac0",
                    "LayerId": "755ef7d0-844a-43ad-b876-f496434a2b4f"
                },
                {
                    "id": "3f216de2-d44b-44cc-b9e1-1437f539225f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9d1ba8-3c06-468c-a7e9-d8b327af1ac0",
                    "LayerId": "b6c41f75-a521-42b0-afa0-f8482b75b7b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "b6c41f75-a521-42b0-afa0-f8482b75b7b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "700674dc-296e-4db9-9658-943005d93df9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "755ef7d0-844a-43ad-b876-f496434a2b4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "700674dc-296e-4db9-9658-943005d93df9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 438,
    "xorig": 113,
    "yorig": 27
}