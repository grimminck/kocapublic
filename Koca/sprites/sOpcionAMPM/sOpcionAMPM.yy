{
    "id": "51893aae-3db1-4098-a9be-98b281520bf9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOpcionAMPM",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fc519cb-d78e-45c2-87ef-aed346f155d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51893aae-3db1-4098-a9be-98b281520bf9",
            "compositeImage": {
                "id": "98f0e8e9-17f2-4292-83b7-560c0b68f886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc519cb-d78e-45c2-87ef-aed346f155d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5968720d-4ba4-4c7b-a9e4-c34355d099d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc519cb-d78e-45c2-87ef-aed346f155d3",
                    "LayerId": "eab9cad8-8b76-4aa5-8b2c-8cadace5c712"
                },
                {
                    "id": "c8be0658-25bc-434a-bb8e-4bb3d6166305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc519cb-d78e-45c2-87ef-aed346f155d3",
                    "LayerId": "08331376-ef4c-48d4-a006-1c814ac2f96d"
                }
            ]
        },
        {
            "id": "ca70939f-1cf0-498c-9d55-a9d5576ea115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51893aae-3db1-4098-a9be-98b281520bf9",
            "compositeImage": {
                "id": "c263f337-9535-45d1-9dd8-5f9dadb18649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca70939f-1cf0-498c-9d55-a9d5576ea115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2043c6cc-c269-4080-9b38-fc6a564a83c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca70939f-1cf0-498c-9d55-a9d5576ea115",
                    "LayerId": "08331376-ef4c-48d4-a006-1c814ac2f96d"
                },
                {
                    "id": "e2c5736e-017e-4bb1-9729-165cef2076be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca70939f-1cf0-498c-9d55-a9d5576ea115",
                    "LayerId": "eab9cad8-8b76-4aa5-8b2c-8cadace5c712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 89,
    "layers": [
        {
            "id": "eab9cad8-8b76-4aa5-8b2c-8cadace5c712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51893aae-3db1-4098-a9be-98b281520bf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "08331376-ef4c-48d4-a006-1c814ac2f96d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51893aae-3db1-4098-a9be-98b281520bf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 44
}