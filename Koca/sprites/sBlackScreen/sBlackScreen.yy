{
    "id": "754ba542-9440-48d8-8c46-b1bf2c1e2b26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlackScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60245675-ce32-4d7b-84b9-88f4520a7c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "754ba542-9440-48d8-8c46-b1bf2c1e2b26",
            "compositeImage": {
                "id": "0cc7abf1-276a-49f9-8195-31d508af6851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60245675-ce32-4d7b-84b9-88f4520a7c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ff1d596-0ac5-43e5-8373-c16f413728ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60245675-ce32-4d7b-84b9-88f4520a7c3f",
                    "LayerId": "4ee528b6-4adc-4fff-89b8-1346af907f98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4ee528b6-4adc-4fff-89b8-1346af907f98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "754ba542-9440-48d8-8c46-b1bf2c1e2b26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}