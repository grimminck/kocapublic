{
    "id": "35170adc-9da3-438f-aa6a-6e7fb83210f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "signo_exclamacion_celeste1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 345,
    "bbox_left": 0,
    "bbox_right": 345,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3be1a764-d9ff-4e4b-b56d-9ccbaee20b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35170adc-9da3-438f-aa6a-6e7fb83210f7",
            "compositeImage": {
                "id": "fd8d2bcc-3b0e-4038-9114-611b97f6febe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be1a764-d9ff-4e4b-b56d-9ccbaee20b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782ae98c-da80-4c8d-a0ce-1a9a1bdf71b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be1a764-d9ff-4e4b-b56d-9ccbaee20b77",
                    "LayerId": "d7a1e830-be7c-4ae0-bcd3-e77e28c3f0ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 346,
    "layers": [
        {
            "id": "d7a1e830-be7c-4ae0-bcd3-e77e28c3f0ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35170adc-9da3-438f-aa6a-6e7fb83210f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 346,
    "xorig": 0,
    "yorig": 0
}