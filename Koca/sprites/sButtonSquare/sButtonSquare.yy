{
    "id": "67d7f7a7-1900-49ae-a612-eb86541d6eba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4055dbb4-9359-48db-9aef-3fbca070fd41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67d7f7a7-1900-49ae-a612-eb86541d6eba",
            "compositeImage": {
                "id": "2723910b-94e8-45e0-8148-73f4b97672d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4055dbb4-9359-48db-9aef-3fbca070fd41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d62ce8b4-2040-4481-8e45-7a9e41b415c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4055dbb4-9359-48db-9aef-3fbca070fd41",
                    "LayerId": "33dfca5f-63e3-438a-a06c-cad60efb2b87"
                },
                {
                    "id": "153301b4-bec2-4884-8578-3633b0879d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4055dbb4-9359-48db-9aef-3fbca070fd41",
                    "LayerId": "de87b16f-5690-4a04-8372-7d56c7dbbdd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "33dfca5f-63e3-438a-a06c-cad60efb2b87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67d7f7a7-1900-49ae-a612-eb86541d6eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "de87b16f-5690-4a04-8372-7d56c7dbbdd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67d7f7a7-1900-49ae-a612-eb86541d6eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}