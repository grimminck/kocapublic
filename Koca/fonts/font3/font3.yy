{
    "id": "2cff1285-f126-4ed5-abd3-ce6e3a2ab3fd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font3",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Imminent Massacre",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f154f24a-63db-442d-ab56-6c56651133f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 190,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b1739d29-0258-4895-991a-db39fd3bb0c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 190,
                "offset": 39,
                "shift": 99,
                "w": 21,
                "x": 154,
                "y": 386
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "25bcc54f-8b9e-480c-9ada-0a45e054bdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 190,
                "offset": 22,
                "shift": 99,
                "w": 55,
                "x": 97,
                "y": 386
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dd56abef-f711-4e74-937c-bf38efdc4be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 190,
                "offset": 3,
                "shift": 99,
                "w": 93,
                "x": 2,
                "y": 386
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fd892c8d-9858-4f8e-8992-316a33a247fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 190,
                "offset": 14,
                "shift": 99,
                "w": 71,
                "x": 1963,
                "y": 194
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "71cc4212-913e-452e-8bf0-06fc5b33af40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 190,
                "offset": 0,
                "shift": 99,
                "w": 100,
                "x": 1861,
                "y": 194
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3df97517-0b2c-4f05-883d-fcf08bbb03b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 190,
                "offset": 4,
                "shift": 99,
                "w": 95,
                "x": 1764,
                "y": 194
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "138bb934-d671-4637-bc3c-dbd2531063f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 190,
                "offset": 39,
                "shift": 99,
                "w": 21,
                "x": 1741,
                "y": 194
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "be27db8d-0a56-4c2a-987f-d8d4aa562f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 190,
                "offset": 23,
                "shift": 99,
                "w": 53,
                "x": 1686,
                "y": 194
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "88b30664-bece-42b2-a1a2-062a7ddfa394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 190,
                "offset": 23,
                "shift": 99,
                "w": 53,
                "x": 1631,
                "y": 194
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8527ca1d-1be0-46a3-9362-e0f9806e06dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 190,
                "offset": 10,
                "shift": 99,
                "w": 79,
                "x": 177,
                "y": 386
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "509769fc-2df5-4a17-aa08-1d3f8c3e0342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1554,
                "y": 194
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dc367aed-2aac-45e6-a1cb-a2ea3b2006d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 190,
                "offset": 35,
                "shift": 99,
                "w": 29,
                "x": 1449,
                "y": 194
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a907706d-ccfb-447d-9e53-44d738ec597b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 190,
                "offset": 22,
                "shift": 99,
                "w": 55,
                "x": 1392,
                "y": 194
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "69dfdada-f387-44dc-a30e-d9d051b5391f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 190,
                "offset": 37,
                "shift": 99,
                "w": 25,
                "x": 1365,
                "y": 194
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5c5f3ce9-19c6-4238-b62e-351a067ebfa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 190,
                "offset": 17,
                "shift": 99,
                "w": 65,
                "x": 1298,
                "y": 194
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a81e66f8-3d78-46e2-b404-239cdb83388b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 190,
                "offset": 11,
                "shift": 99,
                "w": 77,
                "x": 1219,
                "y": 194
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "aa7cbe40-094b-403c-b12e-79efd8da17b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 190,
                "offset": 18,
                "shift": 99,
                "w": 44,
                "x": 1173,
                "y": 194
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7a8f5986-282a-4816-bd1b-3f3385723be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1096,
                "y": 194
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "69b5e97e-b546-40e4-9d12-f6e6ab426f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 190,
                "offset": 10,
                "shift": 99,
                "w": 75,
                "x": 1019,
                "y": 194
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c84aaef5-17ab-42e6-8d0b-f94186128c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 190,
                "offset": 4,
                "shift": 99,
                "w": 87,
                "x": 930,
                "y": 194
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4ba8d506-387c-49cf-be69-6811ed5564ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 190,
                "offset": 13,
                "shift": 99,
                "w": 72,
                "x": 1480,
                "y": 194
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3344aa7a-c89c-4f5d-9685-bb8ce4d5fb3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 258,
                "y": 386
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ab0b65c4-9291-41c5-b324-e06bea2125c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 190,
                "offset": 11,
                "shift": 99,
                "w": 77,
                "x": 335,
                "y": 386
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "da48c545-bcc1-406e-8b7f-80968a87742d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 414,
                "y": 386
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3b3ed843-7eb3-4e52-92a3-89117b11e13c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1760,
                "y": 386
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c06f9b97-a23d-4dcd-be5d-048db730441b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 190,
                "offset": 39,
                "shift": 99,
                "w": 21,
                "x": 1737,
                "y": 386
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d789c1fd-6ed1-4c24-9e33-2bd68a0dbb71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 190,
                "offset": 33,
                "shift": 99,
                "w": 28,
                "x": 1707,
                "y": 386
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fc3a6ed8-4c8a-486a-90f4-0a4597977c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1630,
                "y": 386
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "66b48fca-5b16-4640-ab16-2b1e306ce107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1553,
                "y": 386
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "811dfc32-f127-46f3-a3e3-0bbee7962460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1476,
                "y": 386
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1ea24417-f202-457e-9557-d564f9f0f8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 190,
                "offset": 13,
                "shift": 99,
                "w": 73,
                "x": 1401,
                "y": 386
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0f0013eb-b2f8-44df-8cce-7a33acdec7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 190,
                "offset": 2,
                "shift": 99,
                "w": 95,
                "x": 1304,
                "y": 386
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b7580c1a-f101-4725-9dfe-1eba702a47da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 190,
                "offset": 0,
                "shift": 70,
                "w": 62,
                "x": 1240,
                "y": 386
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a01a05f7-e6aa-4ce2-8a0a-c59169b913b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 190,
                "offset": -1,
                "shift": 71,
                "w": 65,
                "x": 1173,
                "y": 386
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "88f48ae6-1171-43be-a9ba-e038df978be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 190,
                "offset": 0,
                "shift": 65,
                "w": 57,
                "x": 1114,
                "y": 386
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f35b327d-a381-44fa-8713-d614e7a6b450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 190,
                "offset": -1,
                "shift": 65,
                "w": 58,
                "x": 1054,
                "y": 386
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "af13408f-6713-4713-bc39-36311c641e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 190,
                "offset": 0,
                "shift": 72,
                "w": 64,
                "x": 988,
                "y": 386
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fa070f6d-1b0c-455b-b7e0-06dcdb7aca3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 190,
                "offset": 0,
                "shift": 71,
                "w": 63,
                "x": 923,
                "y": 386
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c92981f7-6ed3-4d92-ac3a-11e406dbd65c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 190,
                "offset": 0,
                "shift": 74,
                "w": 66,
                "x": 855,
                "y": 386
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5b6b6ed0-0258-4fb1-95aa-125a7eb8da22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 190,
                "offset": 0,
                "shift": 70,
                "w": 62,
                "x": 791,
                "y": 386
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e03bd1e3-27d2-4fa1-9ae5-454ead2b5e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 190,
                "offset": -1,
                "shift": 22,
                "w": 16,
                "x": 773,
                "y": 386
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b618d354-95a6-479e-9ad5-aa12d29b7445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 190,
                "offset": -6,
                "shift": 56,
                "w": 54,
                "x": 717,
                "y": 386
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "13f65190-293a-49aa-a039-697b3168e4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 190,
                "offset": -1,
                "shift": 77,
                "w": 70,
                "x": 645,
                "y": 386
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0017d260-2ee7-48cf-a0ca-d5842206d71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 190,
                "offset": 0,
                "shift": 72,
                "w": 64,
                "x": 579,
                "y": 386
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fef4ffb4-9996-4acc-9325-2e5d17257fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 190,
                "offset": 0,
                "shift": 93,
                "w": 86,
                "x": 491,
                "y": 386
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "af1f092c-736e-49f9-8eea-8c3c002a2589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 190,
                "offset": -1,
                "shift": 72,
                "w": 66,
                "x": 862,
                "y": 194
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3a8148d6-7cc2-4cd4-8ac4-2be4cbdc3c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 190,
                "offset": 0,
                "shift": 74,
                "w": 66,
                "x": 794,
                "y": 194
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0053c75f-911d-4018-abc1-a3bd634f39e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 190,
                "offset": 0,
                "shift": 69,
                "w": 61,
                "x": 731,
                "y": 194
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "139c5dee-c82a-4ce7-bfd4-5b4f84959815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 190,
                "offset": 0,
                "shift": 75,
                "w": 67,
                "x": 1417,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf42f890-fdc3-4775-b16d-088ac1ba999d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 190,
                "offset": 0,
                "shift": 73,
                "w": 65,
                "x": 1305,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8fcddd23-1c00-404c-b36e-4d1f3d40d6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 190,
                "offset": 1,
                "shift": 86,
                "w": 77,
                "x": 1226,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6f7c77c6-d83e-4df0-b287-33978a2df90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 190,
                "offset": 0,
                "shift": 79,
                "w": 71,
                "x": 1153,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a69cefac-cb1e-442a-91a5-b4b53a02241c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 190,
                "offset": 0,
                "shift": 76,
                "w": 68,
                "x": 1083,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9f9304cd-3dff-436f-99a7-bdaa09f5cb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 190,
                "offset": 0,
                "shift": 117,
                "w": 63,
                "x": 1018,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "177f0274-8cdd-4142-9400-cfc780defdcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 190,
                "offset": 0,
                "shift": 105,
                "w": 97,
                "x": 919,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e84cb1f6-db14-463b-bf3d-d30a66203a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 190,
                "offset": 1,
                "shift": 81,
                "w": 72,
                "x": 845,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fa9c87d2-3b19-440d-8975-51a54d1f9378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 190,
                "offset": 1,
                "shift": 80,
                "w": 71,
                "x": 772,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "08a6636d-4745-4bd0-ac58-7795e5a52347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 190,
                "offset": 0,
                "shift": 90,
                "w": 82,
                "x": 688,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d6aeeb86-0eae-4b0c-a957-1ef00f929bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 190,
                "offset": 33,
                "shift": 99,
                "w": 43,
                "x": 1372,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8a6107d3-ae01-47ea-84ff-f7caf0bdc61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 190,
                "offset": 17,
                "shift": 99,
                "w": 65,
                "x": 621,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b1a7b4cf-35b1-46ae-9ad3-d18d3762fe07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 190,
                "offset": 23,
                "shift": 99,
                "w": 43,
                "x": 523,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1f49c00b-ba4b-4ea2-8dab-cb40efd932f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 190,
                "offset": 8,
                "shift": 99,
                "w": 83,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "329edf50-986f-4e44-9b6d-02ed37193a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 190,
                "offset": -2,
                "shift": 99,
                "w": 103,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "79f3ab28-2c56-4972-9dc2-da3b54f66909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 190,
                "offset": 33,
                "shift": 99,
                "w": 33,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1132e8b8-5b15-4a7b-bddb-ebedbe68f891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 190,
                "offset": 0,
                "shift": 56,
                "w": 50,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e3a7fa3d-bfa4-47e1-9203-5a0bbe61ae41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 190,
                "offset": 0,
                "shift": 57,
                "w": 51,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4befa15d-c609-4448-8bf1-b86f4c77df60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 190,
                "offset": 0,
                "shift": 51,
                "w": 45,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b8aa6017-62e3-4738-8952-7d7e1e597abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 190,
                "offset": -1,
                "shift": 52,
                "w": 46,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "496e47b2-6995-468f-93b8-0ab3b7c092e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 190,
                "offset": 0,
                "shift": 57,
                "w": 51,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dc9485c3-2637-4127-aae1-428b59b450e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 190,
                "offset": 0,
                "shift": 57,
                "w": 51,
                "x": 568,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2f287530-7196-46a2-9f0b-6aa50b5239c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 190,
                "offset": 0,
                "shift": 59,
                "w": 53,
                "x": 1486,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e754cf6b-880d-4ed1-b2da-68d051048cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 190,
                "offset": -1,
                "shift": 56,
                "w": 51,
                "x": 58,
                "y": 194
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "59928094-48ef-4721-b586-2b5945cae467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 190,
                "offset": -1,
                "shift": 17,
                "w": 13,
                "x": 1541,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fd2550ec-a9c9-4423-9ba3-468130f1e096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 190,
                "offset": -5,
                "shift": 45,
                "w": 43,
                "x": 622,
                "y": 194
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a44972ce-3776-42d0-959b-6c20a90935a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 190,
                "offset": -1,
                "shift": 61,
                "w": 56,
                "x": 564,
                "y": 194
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2f2d24ca-5df4-46ec-970c-bd0a665ac16b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 190,
                "offset": -1,
                "shift": 57,
                "w": 52,
                "x": 510,
                "y": 194
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "89017377-1fb5-40ea-ada4-1d280fafed15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 190,
                "offset": -1,
                "shift": 74,
                "w": 69,
                "x": 439,
                "y": 194
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3d8f41ed-5fb9-4b3a-a2c0-45edb9555ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 190,
                "offset": 0,
                "shift": 59,
                "w": 52,
                "x": 385,
                "y": 194
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e3e899b2-a3ef-4dbf-8f32-4fadf7afb5a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 190,
                "offset": 0,
                "shift": 59,
                "w": 53,
                "x": 330,
                "y": 194
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "540814ba-bc3a-4bf1-9fe9-16762bb6d7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 190,
                "offset": 0,
                "shift": 54,
                "w": 48,
                "x": 280,
                "y": 194
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e791631c-9f4c-4592-9a51-fcb7fb0b5a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 190,
                "offset": 0,
                "shift": 59,
                "w": 53,
                "x": 225,
                "y": 194
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a4b9726d-cc1f-4a76-8236-c50da82e931c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 190,
                "offset": 0,
                "shift": 59,
                "w": 53,
                "x": 170,
                "y": 194
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a4ccfcdf-3dac-4129-93ae-2d7a0bf73811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 190,
                "offset": 0,
                "shift": 68,
                "w": 62,
                "x": 667,
                "y": 194
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d733fb3c-50fd-4612-91e3-702c321a2314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 190,
                "offset": -1,
                "shift": 62,
                "w": 57,
                "x": 111,
                "y": 194
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2dc037cb-af7f-485d-b06e-9d0c1cd13c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 190,
                "offset": 0,
                "shift": 60,
                "w": 54,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b05bacfe-9b17-4b1d-9872-cdd63223865c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 190,
                "offset": 0,
                "shift": 94,
                "w": 51,
                "x": 1966,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3f87f237-b05f-4b5c-b743-fa2413e42ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 190,
                "offset": 0,
                "shift": 83,
                "w": 77,
                "x": 1887,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5f81240c-b158-4669-a7ba-e6e1d4da034a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 190,
                "offset": 0,
                "shift": 64,
                "w": 57,
                "x": 1828,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0e21feac-6f01-461f-93f9-ded1636478a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 190,
                "offset": 1,
                "shift": 64,
                "w": 57,
                "x": 1769,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bb5d1468-9d84-4fcf-ab93-6d1c07596baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 190,
                "offset": 0,
                "shift": 72,
                "w": 66,
                "x": 1701,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0aeb37c9-e534-4329-a1da-9adb46cfa886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 190,
                "offset": 18,
                "shift": 99,
                "w": 63,
                "x": 1636,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "68e3a5dc-5f6b-4f51-858f-229878b7a360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 190,
                "offset": 43,
                "shift": 99,
                "w": 13,
                "x": 1621,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c42a293a-7a37-4e85-b505-2628ca256db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 190,
                "offset": 18,
                "shift": 99,
                "w": 63,
                "x": 1556,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a8dae0bd-489d-404b-9379-7d2cf38a83ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 190,
                "offset": 12,
                "shift": 99,
                "w": 75,
                "x": 1837,
                "y": 386
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "714e29b3-93b1-4cc6-a539-f78e6f7ed7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 190,
                "offset": 33,
                "shift": 160,
                "w": 94,
                "x": 1914,
                "y": 386
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 124,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}