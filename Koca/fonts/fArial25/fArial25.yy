{
    "id": "e91cc116-6ba5-43b8-834c-9345b296780f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fArial25",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b8332762-7164-4132-a5b8-440e2b61360f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "feb00046-daf5-4291-944f-c94143aa2815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 149,
                "y": 82
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9e184e44-6c39-4411-8c7e-e43b1073431b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 137,
                "y": 82
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3a02312c-8134-4f15-a310-da6cf2c978e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 117,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1548e575-30f9-4d06-bd3d-5c34c2851768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3e86213f-b20e-4417-8450-117d1a75319a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 70,
                "y": 82
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b6eeaafb-f421-446c-ad93-0d509084be53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 47,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "05a53694-4f05-449f-b97b-6724ee2e2010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 41,
                "y": 82
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a664c708-b1c6-46fb-9616-7648fe2a9bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 31,
                "y": 82
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3fc8e654-56a5-413d-9b32-c9d4a6da7d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 21,
                "y": 82
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2e1b7479-bb28-4f9d-973f-c562b94380e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "60603cd3-d4bd-48de-903d-91f70bcc93f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "01d143fd-2bd4-4204-a2ea-f75a37778aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 472,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "87a0a36e-8024-468f-bdb3-73a408e6f202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 461,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0f3b0c2f-8adb-4030-bc80-c84bf6347ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 455,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8049448f-51cb-4250-8054-6abbfc3083fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 443,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3d2043fb-03a6-4bde-a44c-994a0695dc6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 425,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2da6c8b2-97a9-4096-9735-95fcab66244c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 413,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "20ae1af9-8649-4156-afa1-97d77646ed06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 394,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6eb47caf-8c58-4e98-b561-dc7d70e9be20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 376,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f08783ff-83d0-4fbc-ad42-2c595b712280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 357,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "728755bd-bcdd-4050-87fc-8390cff0b5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 479,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6e3ab751-8e09-4380-9dc0-0aef26619d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 169,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "09f584b8-1376-4fe0-bf40-690ca8992fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 187,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "86f6e327-b8f0-4628-9032-4e623a5eb551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 205,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8fdff1a6-487c-466f-81b1-392195734392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b993f019-5751-46a5-80ab-594958f4f28c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4051e78c-e3ef-40df-a8a7-c878f44664fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 111,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2bf7efb8-243b-4bea-b75f-9aeea34c2876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 92,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bd5e3483-5e0e-4281-be2c-7da951895adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fe5e66ab-a84b-4f11-ad63-6ff8e6a31f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 54,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4c73869b-0028-4ecc-bd3f-010d139a345a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 36,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eaa9a480-48f9-4579-8798-cd0935d76781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b3485022-c524-47c4-a4bd-43bd631e5dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 465,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "861036c8-b974-4c53-9ca6-68cf6a1d494a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 444,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "496d3bb3-a5dd-4e2c-94f7-e461a41128e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 420,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cf48de77-8d45-4579-a2d8-ff4dfc93ac2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 397,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "479dd221-05ad-4f18-9d5d-ae67a0983b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 376,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e55bff4f-f093-4cf6-ac3d-34c697ee275f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 357,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e028ab62-efeb-487a-8c20-3c7c463d4bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 332,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ff6fd83f-5c54-48a7-9cc2-d870f2e8742d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 310,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4fcfa39f-8feb-4373-9ac9-266701c9f0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 304,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2826e6f6-88a9-4d00-9df2-1c11114d0295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 288,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "97496e8c-b37a-4d88-b44e-2966cbe6c3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 266,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e86810bd-633b-4467-928e-380e46d37715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 248,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b07a8cce-5344-41b0-990f-aad88957426d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 223,
                "y": 82
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "575e468f-818a-4de2-b314-9e2059d96aab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 335,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6d366004-fb8c-48c3-b655-c8fca108d130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 309,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b4de28c7-52ea-422a-8655-64f466f03ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 288,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9bd96db9-010c-4bc2-9413-23310d41de9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3c81ba29-c440-48ec-8b39-05ece3366adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d5c71aad-cc2c-4080-a169-a353c4d0bd7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "74b47a54-7d6f-444a-a90c-690f08fc309d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8f007c50-d50b-4e70-aca9-d5e5a8d26808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e5cc9688-6fb9-49fc-8f1f-ac7c4e03246e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "73047d77-934c-4afa-b400-98778a252d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "65daf8a1-6b59-4858-bd33-d4347d78b4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "25f9385d-b71f-46ed-8cb2-511fc3642119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c9ef4b9b-6e96-4f1e-b44e-e31dbd0ef80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "31b9acec-9de2-4d67-b8d9-8d0cf4e1c033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d11cef7d-9417-4954-9387-c9ee07706c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5c72fdfe-e7dd-49fb-b37f-ca936459c36d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ac8ef5a3-5669-4970-9f6c-55bdcfc86007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "536549a8-4b37-4781-9a9f-c32325c9621e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "234c2862-2d5d-4c48-ac0f-f6ab4efb79d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d8d7dc51-772d-4b4b-a031-40869fb54e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b8c57bdd-4f18-4f67-b51d-362e6c463059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a61ec506-45f3-4d91-91d6-cabdcaa00435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f8231d25-07c3-400c-bbce-da812b591b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "265a5a0f-3f3a-4a70-a7b3-4914ceffeba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c220a54c-59fa-4c2f-b638-725d34c7e94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5d55c158-17db-4994-b103-16d02c03dd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2478412e-6e5d-456e-8eb9-296116662719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b7ed9a33-789b-4461-9d02-d1225c8c367e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1d0a967c-0655-46bb-8a96-ac7afd1bc00e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -2,
                "shift": 7,
                "w": 8,
                "x": 261,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b25f8224-02f5-446f-aecd-9d5efdd6ddea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1f4c4489-cd10-4911-8fc4-02db0aba33c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b3627683-305a-4a6f-a587-0538a4a7c9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 212,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ece61ae3-d69d-45df-9fbb-1a069b2471be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b098e58e-1fbe-4199-94d3-3e58c8fdec3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 176,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1abbbc7d-aea8-45ed-b1d2-6b773f7e25c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ce3bf5ca-176f-438f-a96d-74096a80e7ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4de0c1b5-518f-40d3-a9b8-8bd8076f93a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 129,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8e94e849-f217-44a0-970e-ca8fce7f80c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 271,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "29434dcf-878f-4fa4-bf5d-628456aaf3af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2b425662-f2e6-4632-a5d8-23909d6f316c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 85,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "efd8b417-6330-4091-9da4-78541675ac53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "13ef6b97-9334-47b6-af4a-b8e0071f9999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 40,
                "y": 42
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3876bca5-1c0d-4100-ba73-71ee015c2e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 21,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0a5d2deb-c9c8-45bc-b8f8-a7612f9cc935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d0aecf71-dbb0-463d-babe-2fd2d49d3933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e9fb9b99-f074-403f-bd37-1c6dbbe43582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ec25ff42-6dd3-4f6b-abc3-f55ef443f741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "54359322-0b0d-40be-890d-31bc29b919c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "eaed6163-f0b7-4c99-9ea6-b821dea31ceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 143,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c50b19b3-d81b-4d20-87b5-899844920997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 38,
                "offset": 6,
                "shift": 32,
                "w": 20,
                "x": 162,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "7b00a6d3-90b1-4785-a236-fce9c2c684b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "26e5dd7f-72da-4399-ad19-cffc33baaa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "1ae9cfc9-ba89-4e58-a90d-86670d6f5d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "5865882e-e3e1-4484-916b-5f16a7fd8495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "5fcd2ac5-2712-45b0-9742-252e29c20355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "78cdcf02-ecb7-425d-b977-9827fe9ed549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "156c1a9c-582f-46a1-a3e2-29791d859db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "c0470e15-d196-4fec-904e-23975254dccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "f968ff86-4259-49b7-8dfc-c7ea8cf4d116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "c34b0c0e-1e06-44a8-900f-fc11efb1f264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "8116165a-08b8-4dd6-8629-91b7e8129e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "456c978e-a4b5-4da6-a730-c6c3bb68223f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "3fb1458b-fa36-4650-9815-d000179934b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "617f2e3c-d0c5-4390-be17-c49c6e59e206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "71a8710a-e049-4877-9cbd-a7fb85b24646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "83b24539-bd90-495d-abcb-b17b5acd47a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "0e21451b-5efa-4b9e-a46f-0bf6883127db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "5c202601-9ff9-4ccd-9825-4116c1b7ca6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "012608d0-3664-4257-ad64-c49c598890fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "0f73593f-8d83-4c0e-b252-feb7e7f7f5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "d01c4ce5-dbca-44ec-9f69-8c2ae0dfa635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "65f2cb87-57e7-464a-b072-c54cca80ed13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "d13433ff-7bf8-4a70-8325-604d26ae2e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "c1827c7e-7528-406b-99d3-ad2fd176ac7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "a1856c9f-2a8b-4523-85e8-376a92533947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "d1555b49-b188-48da-a144-c1dae7887564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "29b11cbc-ac51-4e42-8ba0-c92039dfd87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "a87e3851-0778-4dec-9a6a-e7d3be74596a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "707e661b-c9ad-49fe-ad28-dbec33265e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "afced587-ab3e-4627-90ba-42169caf6475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "a779fdd3-a212-40b0-8cba-b7f415365f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "8d223f80-f306-4d1c-a9e2-409084d55067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "a818409e-2ce9-427c-b330-7af7f69897b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "3c8c7db2-67dd-4b8d-8a89-68edf5553e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "9714c016-9ab4-4522-a9c1-c7ee663c0597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "c0b6bb7d-d6b3-45ee-b547-cd236ab86155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "6ad6a485-e7ba-48f2-91f7-966c76ab183e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "3ddad03b-615f-460d-8b2a-394bdfd97069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "3a8adbf9-ee74-4cd9-9195-60cb90073cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "614175e3-09bb-4796-8da9-76566063a35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "e29aa558-12c2-499a-97ad-2accd399732d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "998eabba-f4e2-4c2d-8790-cf1aa5009ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "f73ce66d-6162-49a0-a983-fb1f395d2f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "e9f09d31-dd17-418a-850d-f2b8d3335a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "c9f3f5d6-0b50-4244-81bf-a835cae4c500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "81005aa9-0e92-4921-8e0f-86f90646422e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "e90afee7-d192-4958-b3e8-10d77a8ad50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "7e170c6a-17a2-4b08-81a4-43da5781e031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "c5bcc9e3-5562-4aca-b91c-b24453cddf91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "e172a696-793d-486f-95c4-a594168c76e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "24c39e60-9572-47f9-907c-2aab0f90df32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "7067b273-ef1d-48cb-a632-9cb034c1b736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "b29d5d25-54ff-4434-91d8-e9f793faf66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "aeedd5fd-faec-473b-858e-3d82f5bb081f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "8c4ce066-9d19-44d8-95da-ac83d917dee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "4197da27-c43b-4f61-86dd-6e6a0329b51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "a9356163-ba74-4838-bef0-2b3585ffb614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "a244ffab-1e64-4c72-afe2-d4993476bcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "3e35ad30-ecab-46a8-984d-4cd534565b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "f6b21251-6df1-4bf5-a683-b75dc1098d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "d88c5cc0-0bd6-4c8f-a41d-a0f9bb2525a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "51e9c4f8-948f-4262-8a35-2504593495fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "263f8299-ee9b-4fd1-a2b5-f51ea5dd72f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "3772127a-009e-4418-943b-1ffd64b034cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "eab36a6a-b8d3-47ab-b0da-68e355d95c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "9c671e65-2470-4f4c-80d8-6277255b877a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "e898c0ef-f861-4ad3-8d51-802d4a71cac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "fe57dde4-b67d-493f-bfd1-51be15fbf0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "b29bc094-1cd0-4a86-ac48-19d41a1e666c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "96b7c29d-44cc-41ed-9a59-71b49e9881b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "fd7f8d73-83db-4d4d-b120-422b4650d34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "8ca08aa3-bbf9-471e-a3c5-0e9aa94e6274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "d1bc6852-e14a-45b7-93af-ea8be61c6075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "c3f218d7-a5ba-4f8a-9377-0773e9f93538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "19455e51-71df-4687-87c0-f1e0104a69de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "eec69940-dab2-4040-b19c-150869e1270e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "3de6157f-3b2c-4f70-a397-b862f972fd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "bbf2019f-c0e9-4c08-a9a0-3789d2fd0405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "507d5a77-4e2d-4b84-879b-3f3a78273de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "cd3d0b2c-25d4-48d5-913a-efcf270eb06b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "d393ba9c-4368-4151-8165-95d17767a20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "db11ab59-2e90-4018-99b6-b96258d96f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "15f77109-f5a9-4acb-8944-cc9b6ef89e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "45163af4-986a-42f9-99d6-2c69148d2da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "3ea26313-cfa9-4fc2-a4ee-55f7f35eebcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "e02bd4ab-8c19-4c4a-a6d3-2ed39b3c51e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "c9b1ffe2-14ad-43af-8b68-c9100a134cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "c376fa7d-35d6-4aad-9ff0-2fa82d2a1712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "a74e83a0-16bc-49bf-ad6c-f8b538f205b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "dd1f95e7-0ee5-44de-a89d-61453c80f1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "2d4a6d8f-0265-40b2-bd21-4572245348fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "a573ee02-c389-4ce1-b459-3db27fe8249b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "f65e4020-d579-4446-8bd8-70f3de0d62b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "72dc1d37-5b43-4333-b3d6-2744bea275f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "b2f7dcd5-e4ad-4478-a69e-4dfa3d93ff3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "f6c246ce-9c25-4aa1-9894-71358d789e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "075feb77-40d1-42aa-a93f-235cc0fcdabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "c1dea158-a897-4620-acf8-b9a7be8f421b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "784383ca-137b-4c46-9158-d1c1699cce5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "23a517fa-d85c-4645-a47d-c41ba1674490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "c6b03118-b035-45c1-90dc-8baf0577bee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "8293a2a5-7bd3-40d8-9086-4f942a287c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "65fea54b-9a64-44af-88b5-6194c06df47a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "9df8443a-6b35-4ed0-8c9e-aa337dc6cb2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "71f816be-72bc-483b-909d-915dbb488176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "10913738-054d-415e-a7f8-062e6f367cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "05a1f38b-c114-4815-8b13-daeb8d173a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "711758f4-0ce8-49f0-adb1-6b99e4b8bc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "be7434ed-51e7-448a-b098-c5a033209c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "698e61ff-d82c-4116-b0e4-bb597c41c612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "32c8ac91-8358-4ddc-99da-e86381fb73ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "f199d112-af1e-4cca-a897-394538a07aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "03e18262-f479-4542-982f-2a50abb17387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "c58e25ff-bd5c-4fb9-a56c-60534717b005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "086dba65-a82f-4d7f-a884-05ec3da9573e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "212d709d-2237-4943-95be-cbcaaec02678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "ee00fdd4-84ac-4294-baa0-80cd3f7258b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "c3b93d58-5fff-4b5b-a5a2-2a79b460fa16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "f1830f76-7bcb-4ba6-a5a8-dd6d67730a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}