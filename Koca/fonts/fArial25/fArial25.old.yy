{
    "id": "e91cc116-6ba5-43b8-834c-9345b296780f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fArial25",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "864da179-7951-4d16-a3ec-d5602a768896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "099b639e-d61b-4b45-8c24-506df51e8ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 149,
                "y": 82
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d4eddb87-71d5-450c-a9c3-4edf01003f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 137,
                "y": 82
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d63f186a-b68a-42dd-a3b0-98496d7097e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 117,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "13c5faa6-2ecd-4f47-858e-c080c3c42bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "969ae3e1-e385-46cf-b88f-b5c24763c184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 70,
                "y": 82
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c118d506-d370-4c25-8835-8d7c42fd3599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 47,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ae44cc88-7ce0-41bc-8308-45a76f5fdcfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 41,
                "y": 82
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8cd1afd9-e4d3-4c89-ae9e-7cbd49ff6bd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 31,
                "y": 82
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8b3620a0-ee34-4378-aa21-3a6835cf39e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 21,
                "y": 82
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e16d0291-0d57-45c5-8aee-04c228f99284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5b009b6b-f76e-4d86-80de-ee1d83c2be6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "078a6f60-c7a3-42d0-9464-5a309c5d07cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 472,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2d321ed5-8cc4-445b-9f79-7044f41919c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 461,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a26617ec-6527-4a5c-a640-42a3e34f85a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 455,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "53c8431d-148b-4e36-8f1e-1208ff593174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 443,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "28505146-161e-448a-9f14-e0bba60ab955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 425,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "af5e64f6-4691-4d43-ad2e-94985bf20929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 413,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "70ec82c3-63d2-49fd-a4ba-7fa2610e92d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 394,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "368d4c73-febf-406b-9b38-bd75983f0c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 376,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "867a3dee-451e-4eb9-9ab4-7c86055f5c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 357,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "29aeea4e-b111-4640-8d4b-ac96a6e39902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 479,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9684ada7-c0f6-4548-99be-592fadfef601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 169,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e51784e0-4542-445b-853f-9aef4452a647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 187,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ebee5914-d2de-400e-b8a3-d84de3a32d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 205,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3ac0a756-5fca-47b0-b32d-705f21dec766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b307e487-c6e0-48e0-8f47-5901d845e9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac63ed8f-2caa-4a53-a14b-0e275090e798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 111,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b53bfbdf-ecb3-44d8-8d25-e37b3ebe8cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 92,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "aeb290dd-8ba7-4d66-9ed5-5fa5f6f47563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8a0c45cf-5da6-4351-b26e-fa1f06f42b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 54,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "053d1972-8a40-410d-b34a-e1348c91ceed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 36,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "54ad648b-dd4c-4141-bb3a-f8b090a764fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "04850bdb-331a-49a2-8a44-6be619b0cd85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 465,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "13d9595f-0e72-44c4-8476-086d519d3ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 444,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c9be992a-30c3-410d-be66-2ceaf0c7d315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 420,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b12aea54-7586-4917-978a-f0e34b04d139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 397,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1e695b30-032b-4e77-a2f4-45f556fb10f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 376,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d6cb94ae-78d9-4927-8e1b-ad90daefaa80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 357,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0d88b26f-fece-4bec-bf58-b34aaf8ad0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 332,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eb1269c9-ee5e-4eae-9986-92181b0edada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 310,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7136684c-7936-4b20-a827-6a18afbf42f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 304,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3ca5c9c3-ec9d-45ee-b0da-a68e018a2d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 14,
                "x": 288,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "65e76242-014c-4369-96ba-f266dc2aedab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 266,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "053731bf-43c9-4042-87eb-9b769edb8921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 248,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6d6637be-69f0-422e-8ee1-cb45f0a45818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 223,
                "y": 82
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5dab6fab-ac13-49bc-8281-8cde78a966be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 335,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "19830af7-2876-42bb-83c4-7891ef24a11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 309,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c067b6cd-44fb-4949-b898-7d5da1ae457c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 288,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1f621221-bdd1-4534-ad2a-e03d2a9fa866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "895b2721-2e28-4fad-ad55-0060069031ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "64fddcc2-99d6-4367-8010-a20b0ffb9009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e12bfa14-0552-4645-b77c-1b4e67fd46f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "36b33a93-d313-4f13-b2cb-4a08483c8710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "43d723ee-687c-41b3-b5e8-351bcacf246d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f776a6da-5c7b-4f5f-b2c9-2dd3c09c475e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "96efa481-0c9f-4cc1-99b7-1addb2a9ba66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "52e18c8f-5d16-4a54-9443-471d5ca09c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3a7869d2-ff55-4978-b2f7-826bbde60ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0deef235-e7fb-46b6-b9f2-232a0ae29c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "24faf150-65a8-41af-9e9a-f2a8ce9d32b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2c374d7d-537e-4b42-b4a0-cad0c58e80ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "579c8c69-7dec-44c1-b4ba-f3b0923dde77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "84fd7f6e-5021-499a-89fc-b42236ca37d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "820d9adb-0a5e-41c7-843c-2a6735d8d986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c3e4ce96-b082-40ac-adbb-c5bdf85130f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3849ae7f-ba38-4dda-b3c1-25784de47096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f335fde0-dec0-4c91-a3b1-06f1391d3910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f65fc2b9-ed27-4ede-b842-1a9b94156549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0271d209-db63-4c61-98b7-21c6a3ab9f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9b27ab83-0986-4b2b-b711-9997e2f43080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5bcb580d-c150-4a20-96ed-a51c0893c539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9ed3677d-7259-4ce3-bbd9-1a471233153d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a912c085-ad3a-43f4-8e69-7fab9e9904fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4661c4b6-99e1-442b-9d3a-6a0770832188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -2,
                "shift": 7,
                "w": 8,
                "x": 261,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "dcc581d6-45a6-4c69-b6ac-ccbecf7d1588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c8410609-2b4c-4acc-8097-1e268379e766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 238,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "948bac11-b800-4fd7-b413-8b609cb8526b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 212,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "06a1c6a1-5851-4e7c-90b0-4f80fe103c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "82fad1a3-3c28-47e1-b60a-72103ca9d618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 176,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e28c384c-f73e-4e14-9fc4-07b246f34bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d38a1af3-2dca-4d3c-ad8b-7b4d02dd0048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "de1168d0-c101-4452-a600-88a6b99a5b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 129,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6e9faefb-9e0c-4b70-809d-65e27debc97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 271,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e25daba7-aff0-44e7-897a-6b0b208de7b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "dda3807d-6948-409f-8f17-4b60ef24a10d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 85,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d75873c9-de9a-4025-8335-8eea7aac4d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "58d39f5c-8ecb-489e-aaf0-276b279c14bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 40,
                "y": 42
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "64e25c34-7c74-49a4-b254-cfaa15e0b602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 21,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7a81d773-0611-48ec-9a08-3cbeaa8bf857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f0c1bac3-f83b-4de1-bab2-a2b4b637c16c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "2ff3f40e-f482-4c9b-a013-5d687f4c865f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "18df9a9e-0795-4ed8-b155-2fc40bfcd404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6b9dea6e-d8be-4bcc-bc36-36f9b55ea5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0646e5d4-dbb4-47b5-8f9c-c92e635c0a5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 143,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "54795343-3e60-4bf4-ad16-af5c95b4647e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 38,
                "offset": 6,
                "shift": 32,
                "w": 20,
                "x": 162,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "549d7f81-583e-40c1-9ad6-8429f70e9c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "4c3e264a-7235-4485-8740-f03c9e13f6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "44525ffb-7aee-4314-81ab-e70d3ea49bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "b96fe2f1-a60e-4b8a-8be4-7f528819dcca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "8c814b09-04df-4cad-bac3-8d964f5e14ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "bf9b14e5-5b88-4309-8999-a2c4a82cb26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "c1a07d72-d2af-4d5b-88e8-1898db55f998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "e4eb5c6a-1525-4511-a537-fad7973bf909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "2758d1cf-ca79-4821-9808-7599f1f650dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "80b002a3-ee6c-469b-8248-cb49af8da2f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "48e995e6-4686-4a9b-aabd-aadee485a1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "b160f979-952f-4a8b-a8db-602fa2145280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "59432614-ff30-4ac0-b2cd-8f0c79d58a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "011a0055-28a4-4b00-8e5e-5fdb28c18b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "8f7171d5-5541-44b8-90c2-216f6fdb6c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "fdefcab8-87a2-486c-9954-6ee5f21a7b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "604a8bbd-84e7-4d97-a8aa-5c00a30b9f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "fb2b1469-dc84-4bd4-ab26-cfbaf62cd080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "4721a092-48d1-482e-865f-57e782722f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "a3b2f178-1adf-47cc-a49d-ba349077accc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "1d6ef3f4-0dcc-48d3-a7e6-4725a6ea1bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "97370359-fafd-4c2a-a30f-aaea1c47673b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "2cb818cd-f48f-4d16-b385-c2a0104b2a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "c31b74eb-6c4e-4d97-9eef-5e35b3a996c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "ddd0a41d-d706-4491-8dde-6bb50edf7969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "0fb24b02-64d8-4d6f-bd40-01ee7a9719b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "f0705c1a-c2b2-4bce-a44e-550a07a5f5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "6fd08599-ce61-4283-b521-660637f19323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "ab83ada4-07fe-494a-a2f4-4cc97bcd4b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "6a21da9e-469c-4890-87bf-c374bb114607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "e42f8dd7-6d0d-477b-a508-5bdfd7cdafcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "6169545e-3fcc-4bb5-b291-ea551b38974e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "8535de4e-5bfe-4f6a-b5ea-13cdeb33d6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "01ddb833-6aa7-4671-a8ea-9c3e331cb8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "4e3fe90a-0cf7-4f48-aef4-c6b58dcd2182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "cef72065-195e-4268-b793-9207c755852d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "4317520a-207f-49d1-b074-d7b7e4a53a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "8f96234c-5bec-415e-9a17-1be775ba43f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "b8e32f48-19e8-4f47-b79f-b27307dd2a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "fede3b1b-2171-460f-ad13-a3de41422521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "1a8037fc-d00a-43bd-b720-4c6acc0a3b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "73c25b67-837b-4bbf-9aa0-b5e77dc25425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "73c9120f-1cfa-46bd-b534-f0cd6b763eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "9538c6bd-ffba-40c5-b2bb-e81973fed2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "ec27d760-7f7e-43ce-a0d6-45c5341ae6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "e7477a7f-5826-4983-9d13-0b43376a9f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "1c9462e5-0aa3-484a-9623-3c7e8f600e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "ec1b7c9b-efbc-4ea5-b5e9-2b4b9ea782f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "0358c77a-8a5f-47fe-a3b2-bb6ec4d76d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "6bbebbd4-5898-455e-957a-1db0c240e408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "de3f2287-7874-4b54-9578-d92414ac764a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "58c71ab1-3a16-4495-bb70-0988d6f1b767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "d7911057-dd0a-4913-bf97-b2dd6cecc6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "6649e4a5-2d82-4fbf-bf12-ba70578b1160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "21208bd9-28da-4012-ab44-9c002a31eef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "23a012a5-bfd6-47ca-9914-d7129ebe5560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "1d32b4d9-7223-4e05-ae52-24e9c9e4e176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "f0afe77b-77b1-41e3-9129-4e0cfb80fd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "e4358d6a-8c2d-42b4-84a1-69339578f719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "64e46e02-f8a7-4768-908d-682b8d235808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "0fe7c057-1931-499d-b42c-896bd0e1cc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "ddc06544-912d-4501-b781-d5a438cf37ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "36513a69-b129-4c05-9198-59c3af8259dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "dc9e69e0-94c4-460b-a905-dc02840c6a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "f6c65091-5d74-4408-a9a1-bf1dd1f8cfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "28b6ff0f-2f59-4aed-914b-06d0386b0748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "3cbc9b4e-7de7-43d1-8571-71b68ec79794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "dcf3321d-045b-44bc-afe3-95ca7b0214ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "13708e7a-39e9-46d0-a08b-44decf527aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "0dacab66-75d8-4094-a5ef-2b909111b037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "c957e974-ec15-4259-a1a6-ce769aebcb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "b1e09f66-7286-4fda-a5cb-962030faf94c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "542ff788-4aa0-42c9-9640-46c709bc7f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "b4c4fcc5-6612-4cef-a941-94f907eb11a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "c5130c2e-2361-4122-b8d5-4d9b5ab67c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "a5b86b0c-188a-4925-862c-90f9193b35f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "6db1d1aa-215b-46cf-93ff-915e8488f48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "682858f6-6aea-4710-ad47-626c0532f70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "01c6540f-7740-4d08-bebc-d3441efbebc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "2dfe0795-2ab3-4949-92d0-cd7c8f7354a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "ddbf77a0-d0f2-4c9a-bbe3-799eb0070752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "ab4f0468-b7a3-4a79-85d6-bfeac1b2bbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "bdc88725-11b7-4b8e-b7ce-0d0860215804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "a4d94c47-f732-41f4-a57a-e902a12d7320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "3a627854-8029-4776-a640-ffd973ca01f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "7ffbd17e-f69d-4d8b-bc60-8ede5d643bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "23c1d231-33da-46bc-aac2-3c2ed1124a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "c0a3695d-a27e-4c6d-8f47-c7c821d92031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "815eee0b-5554-4140-a398-540b08874999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "bb9df90c-d309-493b-9e46-74b47e2a4de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "57ff5b52-a8ed-40cb-853c-46c2711d8e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "b325b554-1353-48ed-af3f-158fd39d6d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "fd2c1828-345a-414d-90d0-dfbb9f55e770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "51849f5c-dda4-4678-a209-42aadeb61cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "a0eb3c31-a5e8-42f6-970d-0bb73b1841d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "0cfea823-bfeb-4f86-9cc4-52226f94a6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "e562ed24-4d8b-4c33-a1f7-1ef96b5cfa8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "8b1819e2-dbec-461f-8524-b445a32f160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "2ecd6b83-1ca4-4cc6-b730-f63f4b88f40b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "2e35eb56-4126-4d81-aa97-3082238d2f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "6247f107-b4ff-4169-b8c0-dafa75c036b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "e8d5083d-65e7-47ea-8a3f-e849f1e076b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "a1e52ea6-4cd4-42c6-818d-4f297b7aaee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "b4265d5d-ca02-44a1-831c-bdb17ed22829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "7a4b143c-0ced-4d1b-aa7b-bd25e3dc5b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "86ee023c-6eba-4bb8-be03-35feadf7be5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "a3900c42-b785-4af4-a79e-46969f22fa03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "69d06f70-f732-437b-8b2f-02dfef9c4ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "8f890267-b73f-4f7b-8017-ca51d559afaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "d2ecced9-0163-4e83-8bf9-65c748a51af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "e3f73497-310d-4553-ac65-dbf2384a1234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "053d0657-af1c-497c-b568-5fac88ed5811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "783de930-07f1-44bc-96bf-ce171501b5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "d7c18978-4a19-4633-9a49-3100c2131091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "34298c7d-3207-40da-a672-e8bddfc45ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "dc416a40-9c5a-4326-acda-502846481e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "e74ff731-1009-4630-845a-c3026876cce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "3bea08a0-2126-412a-9517-4943b05b958d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "a1d1c9c8-912d-441f-ac35-92578fee28e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}