{
    "id": "9be13259-5bc7-40c4-98ca-7183069ca5c4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fArial18",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d232eb27-3e6b-45ff-99cb-121b8d604446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3248cb58-2ca4-4d79-8484-e2671b8238be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 160,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f8b4b536-bc33-4e69-8530-87fef0678a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "47cdb88e-c7e5-4b46-a235-ad63f0e0b373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 135,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "997277e7-ee77-47fb-a3a9-1a35645538de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 120,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bac43ffb-a5de-4f5a-ba7d-a3f65471234e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 99,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "411a84d9-1754-49d4-90b3-01cec47ddd1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 82,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f020edc0-62dd-48e8-b1b3-03b205d29328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 77,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f9225d52-822a-4708-aba9-4738f825d3d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 68,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "56cf208d-abfa-4504-b6d1-2600810e3b44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 59,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ae87f996-5335-4450-8984-868eee7558af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fc879972-45d8-4fae-8d21-3466414da737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 45,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4643f2e0-2203-4b63-9c4c-093385790b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 26,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d791157c-0849-4084-aca1-7b55a2130ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 16,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "563b660e-9ef4-4b37-a570-c5f244046e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 11,
                "y": 92
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "412cc2df-a13a-4e63-8dfd-823cfd1324d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f1f15597-045f-4752-9795-dbc2fcd5a82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 233,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dacaff0b-cac4-4271-a491-546c47caf56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 7,
                "x": 224,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "58886a30-8456-4cf5-9c11-4faa14045b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 209,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c04ff48e-72da-40fd-9df3-3ede355bda2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 195,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9cbbadc9-73a6-4491-9905-63627a906db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 180,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7e044d2c-0a0c-4f8b-9a7b-b46ed7197e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "49e5f1c0-d9f2-4800-9fd0-078b1ddf8f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 176,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "16696f52-37ab-4412-bee9-1ef103712993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 191,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "dfe63b87-9173-4220-a307-9c64d240a4bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 205,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2be41307-beba-46db-bc20-701d78cd6b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 26,
                "y": 152
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "46892ddc-5ef4-451d-89f8-dc93ba1f4367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 21,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b1276318-cb37-4279-bfb5-36d57d70cc4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 16,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "301044a7-0b39-4e70-9cb1-7d9ede16d709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "064acd94-5a0e-48b4-97bb-d0691f1830b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 229,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "46ab1127-d506-4eb9-91bf-7c11517f3c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 215,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "25599e0b-4ccd-437b-a27e-efc31f0b531d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 201,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "44b3e899-8840-4021-92b9-80acc5629c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 176,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c1f2a3ce-026c-49ed-9a23-108a9b47ddaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 156,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7473ec62-8d4e-4a2e-b01b-2224234ef075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 140,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "25b6256b-7f3e-47e0-9112-91a1b4cc5c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 122,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f141bdee-ce75-4e1a-8c53-06b06525370d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 104,
                "y": 122
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b9cf4fc1-173f-48d4-804d-a7b0b8156e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 88,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "870ee29d-77dc-4825-b9e4-a5b5f7b4c9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 73,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3993870a-45d2-4e05-a36a-d2e7e2df9da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 54,
                "y": 122
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "54e882c1-baac-426d-9115-10f0f68b2167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 37,
                "y": 122
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b910d629-207d-4f1c-8ad8-ddb0eb557c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 32,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "589eef65-6e92-407a-a98e-2e9710e58fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 19,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "26e605c6-eb74-406f-85f6-81d42ff3545b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fe3baf69-d7e5-414f-b147-8d7588412372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4cda8cbd-6252-48ee-8f3b-887e66a6abd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 220,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cb9bc27c-b24c-45a5-822a-3c9f370da212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 163,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ac7f73ab-4872-437e-91a7-6c1ef1144d4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 144,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9aa5bf35-54ee-4994-883d-d98ddacb2128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 128,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "79d80ff8-055c-4bfe-adf3-d88320caa155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "95180766-1e4a-4156-9b9f-956b71dd7723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 52,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "85ee54a7-0367-4419-97de-d6916d04fe72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ecfd55f2-4128-459f-aa1a-4246e936989a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 19,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ec653d5e-5f96-48f9-83e8-661ecf11d210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f2aaddad-d0ed-4bc7-a3e3-fe1a31dae06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1660e761-8b41-4e58-b8ee-407eef47c5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c3bb4eb9-b160-45c5-8881-b85670312cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7161db96-5adf-4564-ae35-783ae5d442ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "75d5f22d-7138-47fe-bdce-b2c53e447f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cae70544-2ed1-4399-87bc-cf5613f1bfe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 71,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bd2c2510-ed8f-4091-a298-f049fbc3ac1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "84eaf606-b798-41dc-8e61-3f4cca515193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "08265338-9e7c-4712-b4ab-5400554b403c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c81c0cae-8a35-49de-9f1f-ef6d6c8d34bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2a9c2a27-27b9-4670-a8c4-1831b10c3e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "34f7832c-dfab-4dbf-b18a-6b222ce88c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c74ec126-4850-445b-a4bd-5dac99ba8492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8b558eae-88cb-4384-8707-cbc25fdf0ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a377f738-3f70-4cdd-b828-53dced74922d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a8173380-8fd9-420f-a61c-a462e2a6fc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "11b1bdd9-1481-422b-a8d8-88d7294c7c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d4b62813-86ef-479f-882c-d3e262b97f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fcb9851d-62ed-41a8-99e9-247080af7189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 231,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0f11897f-56ca-4a5d-819d-31a52814c49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "285a16c3-9eaa-4483-8b82-a09eb148c480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f27ecbee-945a-4512-9606-9ee6444794a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3469dad3-9692-4569-9108-c88a7a7353ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 88,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a13c3923-caeb-4299-9812-1a8ffc71bb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dee34a6c-c245-4b07-8ba9-5ae069f70365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1ee48823-c65d-4f4c-b693-d988fabab5a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 40,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "34fd540a-df1b-4357-8e98-8ebeabb69493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "05504f54-d101-48a3-a484-68b9c457fd65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "29674914-5db0-4358-aee9-3964cc564a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "862ea612-8d4c-4f4b-b97c-51e36d683ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d9ce6188-b44b-47cd-809f-ce50cedd3cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 244,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2e30f9f3-cd2e-4db3-98d5-0cf5cc2308cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 218,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "43b00695-0fd3-46ca-bb47-79b0c65ccbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 204,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "20e4154c-d429-402f-a3cd-19f245d71845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "74ac9846-cad1-40c3-8e23-17e4243257a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5b0f2611-cb4a-45c4-bbb0-6c65dd1192b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 156,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a7179f59-4dae-43f1-bf0f-edf066bb3479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "04b6ffed-0133-415f-bd4c-43f6b597c8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 132,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d540a4e6-0718-40db-a99f-9ed858a23a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 127,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5dbc0ba9-6a18-47ae-9bb4-4680e60501b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 117,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4fe813d1-c27a-4e00-9b94-f111aab5c69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 40,
                "y": 152
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d0b7507c-c136-44c0-a149-5326f0b6acc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 28,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 55,
                "y": 152
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1700cf47-245d-4f28-a24a-a7e7d3ea34fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "7b7c826f-28e0-4249-bcb2-28b1b2e6ffef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "1fd60692-2fe9-4bd7-8590-a9d18a5408b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "230ef288-55a6-4f63-9678-841496b044d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "cd0b74f3-e62a-4aa7-a425-e364c1e3c34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "ee73ce18-65d6-4f4f-87bf-2a507192b7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "0a7d1e22-cea5-46f1-8c3e-8da7a414b302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "33da23dd-42b1-4bd0-bd3f-0ca65ff0faa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "27d9caa3-531a-4c45-a6cc-9d2d838578a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "6d7f8847-1bdd-40cc-ade8-27497f6d7381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "450318bd-b70d-4aa8-b9ec-d582ed807419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "fb75111c-f364-4709-a1d1-45b6fdd09adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "e2c67781-45c0-4361-9576-483444506676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c990dcbe-f18a-4a89-854a-e11f6dce8e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "7860355b-002e-455c-91a4-cdc3731fbb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "5e22e3b7-9ccc-404e-8770-6544b1716b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "bcc6f938-4973-40b9-9b1e-ecd62214513e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "57f007f5-e49e-49e2-9861-97aeb7f48349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "f9c46691-d9b2-4689-a7f8-c968d9a5bcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "0cab60c5-a093-420a-bf12-ba01efd8f0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "d7075453-b642-4e37-93aa-7bf3b64ba576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "3a03fba7-a24e-454e-be03-9bcb424f3209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "9e53dc60-3728-4295-8af0-deb2dae13e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "f29dd7bf-2507-484b-a363-62be1132b014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "512022f8-2af5-4004-abf0-3326f63caf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "87a03cd9-2136-45db-b0de-bded82820adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "01b90bb3-b7f1-41be-926b-00f7d33b4ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "d1bfb031-a918-4a95-8ff2-14b3bdc16ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "cb286fca-9141-4925-a80f-8082b9241e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "0288ffb9-dc01-48d3-9536-1bdd15404b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "bf115d05-5176-44c0-9302-1dc1eeaa2c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "01213463-0525-47d4-9a62-918fe5da16b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "6408d144-b66f-474e-9e2a-01e69db1aa09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "fd711c63-51c8-4fb1-bcb5-9db459d08b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "13f22b19-e98d-4271-88b6-b825a1874d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "f15f656d-ef06-40a9-be2b-84fddadf53bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "aa77a22d-cf35-4e0f-aea9-6874d5483e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "0f0947b3-c241-4730-8015-ca5132dbaa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "51044ef0-2d5c-425f-aced-ce824fbe0bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "12c38667-25bb-4155-97c5-b46ab0168cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "7eb25501-2455-4472-beea-19ac24ffa5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "442fe56a-1a06-4595-aa63-09ce0b1820be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "46300083-3572-4f97-8ee2-23bf55357fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "f0e1f137-8e32-4a48-926b-0208fcf63b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "dd8f4e77-dbc0-47c2-bff6-5f225e8eaccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "31f74daf-e7d8-49e6-a220-a2c2b9a2e69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "a47cf0ea-1e20-4ba1-bb74-7378ef87e3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "9874aa34-488c-4d78-b8eb-1e628244cbff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "3bec61bc-c185-443e-9890-52e8b2363457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "cf469c27-8242-4cd2-89ff-3a8cc4b20f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "2cf95bde-1695-47dc-bdc8-3c2fb39a85b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "4a325008-31fa-4e17-b5d3-d47703e69a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "696e66f1-5c06-4bca-9365-40054a60d995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "6be4bb3d-9852-43c6-9b64-2e7cd53e44b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "f50c7703-6fd8-43f5-af29-f252e0178dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "699550ed-27bb-468e-919f-af6848817b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "fae9680a-7a9c-4047-b8bf-808e8d63f529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "fe2459da-92cf-4722-a6df-070be8b6d6c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "fb2de5be-b797-4fb3-8048-6f9ef78403d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "1d2ee399-16f5-49cc-a483-abf891600680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "0ca671ec-d78e-49a6-b66c-2a9ce3da8a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "8626dbe6-ef01-4ab6-9564-8ee2872c4333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "e20c4989-241e-493e-9baf-433558c74d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "856a47e4-92d9-42f7-ad81-b1e9086eb258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "2c4f1a89-a085-4f92-8f81-793b29756e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "d1765aa0-639f-47fb-a9b6-1995426d5d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "d2b2ef7d-8281-4901-8241-faebfcb7362e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "5b540f3c-96d4-4c30-9a0a-97ed5bc06620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "7d76698d-51d0-4017-a6c3-bf2ef3fbc79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "d567be11-e592-4fcf-9be8-385140909824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "9a93f084-d56d-4322-92c3-5f3f58fdcbac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "871bb820-b79b-44e8-96a0-d17ed8a4c61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "3b723c40-b018-40a8-8514-f1fecd53e2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "9c27fe4e-8141-4c5e-acb3-67cb6b7e43d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "5bcbc11c-06af-445f-b963-8ba7e3bd8e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "92bae358-f823-4c40-aaf4-6b328112c57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "258dde4f-a899-4ddf-a79f-65d7cae82b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "44472538-7202-41ea-9163-b54a91def6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "b76b9296-3ee0-4827-9902-12c391836dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "f1e9c2a7-313b-47c2-a1ac-d02fe46429af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "7d256fee-cde8-47f5-92c8-45c8da0fa2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "f35beb8f-59f9-43c0-8210-cfa56b820193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "b8deac69-37c8-4d94-9d70-2bc611e930eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "0487522a-5fc5-41e6-a75c-9f2c71b85649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "87daaf53-0f5b-4b5f-80d1-15172c2a56e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "a9b7df2d-ad88-4de6-ae8c-3831b12ec727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "9c341462-93e9-45d5-8ca6-7048940d5e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "13903432-4ff7-452b-bae2-8dc09632dfd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}