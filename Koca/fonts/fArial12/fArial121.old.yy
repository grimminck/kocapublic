{
    "id": "dbebaddd-eb38-4ac7-b708-161560948a27",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fArial121",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "35fb8346-5c63-4fa2-972d-11b4292739bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "edd569c3-68ec-4302-ade8-72c4a499a866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 127,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c8f73da8-21b1-4f16-beaf-dca55df8aa9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d0f07c17-074d-4b0f-9184-2a3821f01593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 103,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "10aee5a4-371d-443e-9516-a7a368617a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 89,
                "y": 86
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2cfe3e90-359d-4b21-b5bc-a969086bc89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2342a64d-27d1-4054-8592-18fa831fa977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 51,
                "y": 86
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "82909795-2b9d-4dbe-8dec-d1595faacf45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bd80597d-186b-4f0b-99a0-4875959354f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 38,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "53b213df-c4e5-4e2a-85da-8baf522b4a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d02adf99-1d49-4e9e-a7fe-d33ea062a9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 133,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ff161e4a-8c57-4134-8863-d0bc9c1fdfab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4bdc9d6d-8ac9-4317-8e85-b7041172330f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 238,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "27a99e3f-ef2d-44f3-a828-04517c011aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 229,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "50ed1af8-fbdb-4dad-a306-1d0ad6f4f22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 224,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e87d3e6c-d1ef-4532-80c4-7ceec7e264fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 215,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "354e1d39-5142-4ddf-af1d-d60dd9cfcc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 201,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5e5b1364-fbc8-41c4-b4c6-aa539b88f569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 2,
                "shift": 13,
                "w": 7,
                "x": 192,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f43b95e1-450c-4909-874c-d2338aac667f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 178,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "302153d0-d6fd-4afc-a47f-8771f5cccb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 164,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0c5e29ee-6fb2-49e8-be8b-8a26d8ad981b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 150,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "75906c24-43e3-4375-bd55-86657d84ce9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "41f1fd4e-7cd0-4844-9410-9d4b639608fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 144,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "39250df5-1a21-45e5-94f5-4756dbc88c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 158,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "71faacbb-8b5f-4785-9f6f-98b67221b9b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 171,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12254c9c-5112-4b49-94a6-4f9d88a9bb6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 225,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "519f977a-ed88-41c4-ae9f-12e448312579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 220,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "768a47ed-3d07-429d-986c-ee6a68393b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 214,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3cfbe32d-cd89-4d48-a45e-48cd2883f6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 200,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fe8c87f7-31c3-48cc-ae37-35f3846ad697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 186,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0faf5246-3c0b-4f2d-a5a0-b4b8bd918c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f7a739f-2645-4344-ba84-866c2fc79ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 159,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "dbfb97a1-08b7-4dd6-864c-34317c6ff7d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 135,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "963ddefc-fc6a-4d39-8654-311fb728a7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 116,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e7ba0643-29c5-4f61-9e8a-f84b96d85f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 100,
                "y": 114
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "de6fe9d2-3b95-4930-9906-d5258be790dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 83,
                "y": 114
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ac5d47cb-9fa3-480d-9c75-995022fe7459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 66,
                "y": 114
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c396ae73-b3eb-4e03-9d81-c831de682de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 114
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "53955882-f2f6-4404-9fae-0d340745ac7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 36,
                "y": 114
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ba9f04d0-2d3e-4151-bdb9-8e2201f50d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 18,
                "y": 114
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4e3b2096-925c-47b5-9944-9e99ca8fb04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "36dd7d92-4107-4f48-9dd1-304e003a39b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 246,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "29391403-84f4-4e75-b8f5-3fbae17f6a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 234,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "67f6274a-0954-490e-bd07-336125dc32e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 217,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0a1d5e1f-b972-4a2d-93ed-f036aa8760d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 204,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2fb66af3-b5ed-44ce-a16f-4253116d2d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 185,
                "y": 86
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e4fd3cc3-f1f1-4b55-bc9e-bc069c8dc3bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 134,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7d2e85cf-ceb1-4e25-8e2c-1e53926d8bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 116,
                "y": 58
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0e900e7a-7d93-4b0a-b6da-8a4d32c8b2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 100,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d82ca735-ed20-4977-8265-3cec875919e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 60,
                "y": 30
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1d747f48-958b-4e42-94a1-0eb3ffe1748e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 34,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "38d58120-26f4-44f3-9076-51a2d5a7a0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d3bd2f59-6068-43d2-b12b-abed1e83c46b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "75e18ac5-2127-4b15-b5c4-7703994fc8bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f3208321-b091-4915-8dc3-c135c9317daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2a5587d3-729a-4044-afbc-6a1153784403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a1d21f0c-6db7-433a-9ecc-6a018c021802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f05bba1b-2ef0-4d92-96d1-d24d9c53857c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "baf57bb2-27fe-45b9-96e5-b50cc22720f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ec354311-e440-41f8-9ae1-50756e80cb1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 52,
                "y": 30
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b156dc30-6933-44d4-be4c-350f58557af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "75150faa-30f5-417a-b0bd-1047429ea3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "bd4c4e54-67bd-41ff-8a6c-d57f0c269a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "269a8c7b-a0a0-429f-81cc-cc398b940f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8caef7bc-f6c3-4c73-90fa-ad750687ddfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2b9b5969-15d6-4cde-909a-27025c102a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2d7d29ed-61df-4881-b8f7-df368fc342ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff814448-e66c-4064-87b5-8088d18e12d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "44953f48-3602-4364-9b5a-8625e43641d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1102f525-2f93-47b7-8cfa-daf19276ddeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "675b0087-247b-4bde-9c97-22ac4bf95574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6763d41c-2dcd-4898-87cf-e8171a29e621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 80,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0aa9c1b8-a7ef-49f1-bd0e-b84a0552d651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6b4804ac-116d-481f-a10a-47e47c1cf243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 94,
                "y": 30
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "68c3e6b3-5c4a-4d6d-bd5f-ee0facb13d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 79,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5a32c596-a6f5-4e2d-8813-e915c7b6edf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6ca793e0-b026-41ce-a39d-2e9af1682db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 61,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f8201817-16bf-40bf-9af5-c0af3666e4cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a4c7d1bd-a57b-4244-8a51-463f50192663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 29,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f1ff7991-bbab-4c32-a5eb-3f40203fa4ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f692e66f-40aa-49eb-b00a-8a06c5d634ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f803e506-9bf2-4d5f-a5d6-e4bd4c87585e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 241,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8b71ac12-f9e1-432e-873c-36c10fde56d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 232,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c6988944-9be0-4748-bda5-6b9a6cc8b036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 87,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4c32fc30-7bc9-4332-848d-a4807e0ea180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 223,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "436c27ee-2437-4cbf-9068-1ddeb75e5109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 197,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "027f21cc-0bff-4f07-923d-c71392b6f471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 183,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1ee3438d-a90e-4683-bda9-c9d8fdfc1424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 164,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "35c8a012-ac19-44e1-b69b-28ce64f87bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a613ee66-6e9c-4e64-9559-469113f21177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 136,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f9b1328c-760f-464d-8f25-12079b43a368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 123,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ff647bdf-1643-4af6-bf6e-768ab9959449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 113,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "842b40e7-290f-4a63-a234-129caf557f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 109,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ba5141e1-66cd-40c1-9a29-bb828d65c2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 99,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e8f52ee7-7079-49a3-87dc-1f2fdf2d36c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 239,
                "y": 114
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d37ce684-89bb-41f5-af1e-b7cbe8315e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 14,
                "x": 2,
                "y": 142
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f051454e-18d0-448d-be83-fdb455a1936c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "ae163d4a-f1df-494c-82e2-23cc4511b257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "74cff4be-a721-4d30-ad75-61654930bd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "55452cfe-b069-483f-9b41-a8fe8d3f851d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "0fefefe5-29df-4b86-b889-7f8f7502b341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "28230f87-471a-4384-8dbb-0329ff88d4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "9d1d8b7e-1ec2-4892-88c2-d84a7332761d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "27d73a87-237a-4321-92ae-33d3b1fffd97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "27c51449-8be5-4616-8982-f2bfb723148b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "3998350b-3e7d-4f76-b854-eebb519eb41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ac5b2db5-ad89-4ab8-afe8-7973ea02752f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "0f7eb66a-5372-4bec-8473-050317da46be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "ed3aaf21-1934-43cc-ae8c-5a5aa028508f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "1bc8b51a-86fb-4ddb-8f22-bcf3abea3c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "58435a6c-c690-4106-a45f-04c43fa10f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "e42189f6-3f41-4eb1-be5b-e39b156ba592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "d264496f-fe72-475d-838b-ebe052db02fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "17073c48-491a-42e6-b56e-5cf091de32bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "0990a5e3-4398-4aac-9871-49620b83d630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "09d465b1-4777-4a7c-9466-fb2a1d14fcfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "8728f49d-fd3e-4cd7-aad0-8ba78ff10233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "7be17e75-b26c-4f2f-9f7d-c66e23ffe9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "123d0960-c4c3-4f9a-b2a6-085fc1a7a13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "ba819d89-73f5-4537-aa5b-f93dba5076be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5aa81cf5-fbb9-4bb3-871b-ffe9b1e887de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "dff6b3e5-1555-45e8-82c5-af44e77bc251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "bf2b3ab6-5041-4170-8e5c-b6674b334f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "3369776e-6854-44a0-bc60-4cb7b5b36178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "e47952b5-2b58-4cca-ad06-a9a9ee04d1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "339acef3-e213-4cf5-8417-4ee34043fa1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "c292ba3c-0902-492f-9616-0f6425dd1234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "a84df19a-e11a-49ea-abd3-0c8bd2f6fa5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "92bf7b10-f388-4e9c-88f5-183423f59038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "5961e35a-efea-4933-a7ba-deceea5572f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "390bcc01-f5d8-48d9-b939-849cb107e264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "c9d6ad31-4d77-49c6-9ace-d41f0c775094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "359a2759-b580-4c4b-8dfd-3928e82ebe32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "efe02981-15fb-4159-9663-624b6eb5b3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "a15b1a52-409c-4fb0-baed-264f554b3e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "14c7a15c-3381-44b1-95d6-d69dfcc4ccc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "b03797db-8ad2-4846-8bda-838d05786f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "924ebfdb-a6cc-4b26-b718-f9ea831c9a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "ae5e95f7-6d03-4cea-b9c8-91a3af444ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "699a5f1c-f672-4b22-ac77-bb163e8290b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "b48ab5f1-0dc5-415e-b195-46bd389b9bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "5effd25f-df62-4309-8628-2d40fdc3925d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "287d8c15-4875-437c-aaa9-a05baab96ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "6e8e5427-495a-48d9-bed9-422f99044759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "5fd0472a-b6f5-440c-80df-735fc99fb604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "0f6f039f-7184-4201-8a2a-ae9352c9e8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "794d66f1-1043-4b02-8c67-bdfaf7f7d9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "6c227be8-3183-467a-b2fb-1b70a3a00279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "3b3b0de9-3271-4a93-b2ac-819489fd5ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "6d733e38-ab0d-45f6-a2d4-0ebff5beb908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "0cbd7421-b8b2-4c07-98b7-de4623954356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "2c703749-2872-45f5-8777-26107a046651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "3f098a9c-b0ae-428c-90d1-d86384f505c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "8f580608-579f-4b9e-ba90-6d3328987da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "10b8ec4e-14d2-4fb1-9237-f4ba2327d45f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "05feaad4-b4a3-4a42-87de-a438e53165ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "55c49a95-cd75-4299-828e-35864d49e6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "35a175f5-12e3-4164-a873-b062f806543e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "a4349dfa-159a-4b77-99d1-0dfb2fd116a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a75282c3-edac-4577-bac2-ca2ff649fca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "1d84d97f-a119-427a-a6ff-afc13c2b65cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "88dfcbc4-a10a-4f70-817b-54428c0cec3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "86f8c7bc-7e99-4556-a371-2f73d86c741e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "95e425c1-f06d-48eb-9e19-97a4b538b3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "60c12260-a6c2-4313-b2ef-f1630d6fae3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "8e40db5f-5e8f-42fd-aa25-bb6490ec6587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "58942fb8-ec0a-4d58-ba9e-0f4280879ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "db7703c8-fc75-44ab-9c02-c73a855f9d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "26ccf8f4-9d33-49d5-93cb-7673d969b46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "5e3248ff-f11d-4d4c-b078-d35b39b27850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "40144a2d-ca3f-46f6-9558-f1506013e385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "3eed7854-cebe-45f6-b1a2-25523e7e264a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "1cab9015-766a-4e68-8754-24fb1685f687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "0d5ad45e-ef26-447f-85ec-e43a316c95dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "406f16b8-16a9-4055-b003-871f0a3aa741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "58dcb89e-8ae1-4b2a-80e0-7367c8477b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "8f0dc762-a6d7-470c-9340-91984f036b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "86993691-7d0a-4d84-bce4-0bf86280a6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "41209b0a-2214-41f8-99a6-4cab5efec7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "6033dcab-503e-4808-a548-c5c43f327f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "c8f5908b-5e66-44c8-a7db-50f13f9e069c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "0af40c34-909b-4a6b-8c14-9d12f8c9b269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "10812285-fa5b-44ef-a03a-a1f730282fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "07e24a59-98c3-47e1-94c3-7558f8344a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 17,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}