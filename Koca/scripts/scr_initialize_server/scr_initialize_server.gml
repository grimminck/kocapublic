var port = argument0

// Create the server
network_create_server(network_socket_tcp, port, 500)
msg("Server Created. Port: "+string(port)+" Max Clients: "+string(500))
	
// Create structures
globalvar socketsSB; socketsSB = ds_list_create_2()
globalvar socketsFasa; socketsFasa = ds_list_create_2()
globalvar socketsCV; socketsCV = ds_list_create_2()

// clientes
globalvar rutsClientesSB; rutsClientesSB = ds_list_create_2() // clientes de Salcobrand
globalvar mapSocketClienteSB; mapSocketClienteSB = ds_map_create() // clientes de Salcobrand
globalvar rutsClientesFasa; rutsClientesFasa = ds_list_create_2() // clientes de Fasa
globalvar mapSocketClienteFasa; mapSocketClienteFasa = ds_map_create() // clientes de Fasa
globalvar rutsClientesCV; rutsClientesCV = ds_list_create_2() // clientes de Cruz Verde
globalvar mapSocketClienteCV; mapSocketClienteCV = ds_map_create() // clientes de Cruz Verde