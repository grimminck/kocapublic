/// @param Rut
/// @param Clave

var rut_string = argument0
var clave_string = argument1
var _string

if !is_undefined(ds_map_find_value(serverAccountsRegistered, rut_string))
{
	if scr_check_rut_active(rut_string) 
		_string = "Cuenta ya en linea"
	else if clave_string == scr_server_get_client_data(rut_string, saveSystemInfo.clave)
		_string = "Conexion exitosa"
	else
		_string = "Clave incorrecta"
}
else _string = "Rut Invalido"

return _string