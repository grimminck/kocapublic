// Create save/load map or load it if it already exists
// Create Map of users registered on the server and their info about tickets
// Create ds map
/*	map		|	list		|		map		|				|				|
"186410521"	|	RUT			|  "186410521"	|				|				|
			|	CLAVE		|  "holiwi123*	|				|				|
			|	NOMBRE		|  "Andres G*	|				|				|
			|	CORREO		|  "as@sd.com"	|				|				|
			|	EMPRESA		|  "Salcobrand"	|				|				|
			|	...			|				|				|				|
			|	TicketsO	|		0		|	"Ticket Id"	|		1		|
			|				|				|	"Rut"		|	186410521	|
			|				|				|		...		|		...		|
			|				|		1		|	"Ticket Id"	|		4		|
			|				|				|		...		|		...		|
			|				|		...		|				|				|
			|  TicketsTEE	|		0		|	"Ticket Id"	|		5436	|
			|				|				|	"Rut"		|	186410521	|
			|				|				|		...		|		...		|
			|				|		1		|	"Ticket Id"	|		12312	|
			|				|				|		...		|		...		|
			|				|		...		|				|				|
			|	TicketsP	|		0		|	"Ticket Id"	|		46		|
			|				|				|	"Rut"		|	186410521	|
			|				|				|		...		|		...		|
			|				|		1		|	"Ticket Id"	|		76868	|
			|				|				|		...		|		...		|
			|				|		...		|				|				|
			|				|				|				|				|
"195456281"	|	...			|				|				|				|
			|				|				|				|				|
			
TicketsO = Tickets ofrecidos
TicketsTEE = tickets tomados en espera
TicketsP = tickets pagados
TicketId = Id UNICO de cada ticket existente

////////////////////////////////////////////////////////////////////////////////////////////

// Create lists for every ticket made in server history, their information, id and status
/*	list	|	map			|				|				|				|
ticket 1	|	id			|  53467		|				|				|
			|	status		|  "Com. Pagado"|				|				|
			|	empresa		|  "Ahumada"	|				|				|
			|	rut			|  "186410521"	|				|				|
			|	nombre		|  "Andres G*	|				|				|
			|hora inicial	|  "as@sd.com"	|				|				|
			|minuto inicial	|  "30"			|				|				|
			|hora final		|		...		|				|				|
			|minuto final	|				|				|				|
			|	dia			|				|				|				|
			|	mes			|				|				|				|
			|	anno		|				|				|				|
			|numeroTelefonico|				|				|				|
			|correoElectronico|				|				|				|
			|numeroLocal	|				|				|				|
			|direccionLocal	|				|				|				|
ticket 2	|	id			|  53467		|				|				|
			|	status		|  "Ofr. Pagado"|				|				|
			|	empresa		|  "Ahumada"	|				|				|
			|	rut			|  "186410521"	|				|				|
			|	nombre		|  "Andres G*	|				|				|
			|hora inicial	|  "as@sd.com"	|				|				|
			|minuto inicial	|  "30"			|				|				|
			|hora final		|		...		|				|				|
			|minuto final	|				|				|				|
			|	dia			|				|				|				|
			|	mes			|				|				|				|
			|	anno		|				|				|				|
			|numeroTelefonico|				|				|				|
			|correoElectronico|				|				|				|
			|numeroLocal	|				|				|				|
			|direccionLocal	|				|				|				|
ticket 3	|	id			|  53467		|				|				|
			|	status		|  "Reservado"	|				|				|
			|	empresa		|  "Ahumada"	|				|				|
			|	rut			|  "186410521"	|				|				|
			|	nombre		|  "Andres G*	|				|				|
			|hora inicial	|  "as@sd.com"	|				|				|
			|minuto inicial	|  "30"			|				|				|
			|hora final		|		...		|				|				|
			|minuto final	|				|				|				|
			|	dia			|				|				|				|
			|	mes			|				|				|				|
			|	anno		|				|				|				|
			|numeroTelefonico|				|				|				|
			|correoElectronico|				|				|				|
			|numeroLocal	|				|				|				|
			|direccionLocal	|				|				|				|

First map keys = Ticket unique index
Status: Pagado, Ofrecido, no disponible

*/

// Use functions
/*
ds_map_add_map
ds_map_add_list
ds_list_mark_as_map
ds_list_mark_as_list
*/

// Tickets
globalvar ticketsSBAvailable;		ticketsSBAvailable			= ds_list_create_2() // Tickets de Salcobrand
globalvar ticketsFasaAvailable;		ticketsFasaAvailable		= ds_list_create_2() // Tickets de Fasa
globalvar ticketsCVAvailable;		ticketsCVAvailable			= ds_list_create_2() // Tickets de Cruz Verde
globalvar ticketsSBUnavailable;		ticketsSBUnavailable		= ds_list_create_2() // Tickets que pasaron de fecha
globalvar ticketsFasaUnavailable;	ticketsFasaUnavailable		= ds_list_create_2() // Tickets que pasaron de fecha
globalvar ticketsCVUnavailable;		ticketsCVUnavailable		= ds_list_create_2() // Tickets que pasaron de fecha

// Creat enums
scr_create_saving_system_enum()

if client() scr_initialize_saving_system_client()
else		
{
	scr_initialize_saving_system_server()
	scr_create_default_accounts()
	//scr_create_default_tickets()
}