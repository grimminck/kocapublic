/// @function scr_server_broadcast_buffer_to_firm
/// @param buffer
/// @param farmacia
/// @description broadcasts the buffer to all players

var buffer = argument[0]
var farmacia = argument[1]

msg("Firms order: SB, Fasa, CV. From 0 - 2")
msg("broadcasting to clients of: "+string(farmacia))

var list
switch farmacia {
	case farmacias.sb:		list=socketsSB		break
	case farmacias.fasa:	list=socketsFasa	break
	case farmacias.cv:		list=socketsCV		break
}

// Go through the list
for (var i = 0; i < ds_list_size(list); i++)
{
	var _socket = list[| i]
	network_send_packet(_socket, buffer, buffer_tell(buffer))
}
