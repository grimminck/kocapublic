/// @param Farmacia

var farmacia = argument0

var a, b, list, list2
switch farmacia {
	case farmacias.sb: a="SB" b="SALCO" list=ticketsSBAvailable list2=ticketsSBUnavailable break
	case farmacias.fasa: a="Fasa" b="FASA" list=ticketsFasaAvailable list2=ticketsFasaUnavailable break
	case farmacias.cv: a="CV" b="CRUZVERDE" list=ticketsCVAvailable list2=ticketsCVUnavailable break
}

msg("--- START "+b+" TICKETS INFO ---")

var available = true
repeat(2)
{
	var c
	if available c = "available"
	else c = "unavailable"
	
	msg("List of "+b+" "+c+" tickets")
	var _size
	if available _size = ds_list_size(list)
	else _size = ds_list_size(list2)

	msg("Number of "+c+" tickets: "+string(_size))
	for(var i=0; i<_size; i++)
	{
		var ticket
		if available ticket = list[| i]
		else ticket = list2[| i]
		msg("Ticket Index: "+string(i))
		msg("Ticket Information:")
		msg("id "+string(ticket[? "id"]))
		msg("datetime "+string(ticket[? "datetime"]))
		msg("status "+string(ticket[? "status"]))
		msg("empresa "+string(ticket[? "empresa"]))
		msg("rut "+string(ticket[? "rut"]))
		msg("nombre "+string(ticket[? "nombre"]))
		msg("hora inicial "+string(ticket[? "hora inicial"]))
		msg("minuto inicial "+string(ticket[? "minuto inicial"]))
		msg("hora final "+string(ticket[? "hora final"]))
		msg("minuto final "+string(ticket[? "minuto final"]))
		msg("dia "+string(ticket[? "dia"]))
		msg("mes "+string(ticket[? "mes"]))
		msg("anno "+string(ticket[? "anno"]))
		msg("motivo "+string(ticket[? "motivo"]))
		msg("numeroTelefonico "+string(ticket[? "numeroTelefonico"]))
		msg("correoElectronico "+string(ticket[? "correoElectronico"]))
		msg("numeroLocal "+string(ticket[? "numeroLocal"]))
		msg("direccionLocal "+string(ticket[? "direccionLocal"]))
		msg("----------- Next Ticket ------------")
	
	}
	available = false
}


msg("--- END "+b+" TICKETS INFO ---")