/// @param halign
/// @param valign
/// @param font
/// @param colour
/// @description font align = fa_left

draw_set_halign(argument0)
draw_set_valign(argument1)
draw_set_font(argument2)
draw_set_colour(argument3)