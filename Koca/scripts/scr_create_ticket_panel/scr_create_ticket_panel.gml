/// @param ticket
/// @param panel
/// @param data...
/// @param ...

var ticket = argument0
var panel = argument1

with panel
{
	var xx = x
	var yy = y+150
	var panelY = y
	if panel==oPanel1Ofrecidos or panel==oPanel5 or panel==oPanel7Pedidos { xx=x-sprite_get_width(sPanel1); }
	if panel==oPanel2 { xx=x-sprite_get_width(sPanel2); }
	if panel==oPanel3 { yy=y+80; xx+=105 }
	with instance_create_depth(xx+50, yy+50+posY, depth-20, ticket)
	{
		rut = argument2
		fecha = argument3
		hora = argument4
		sector = argument5
		farmacia = argument6
		ticketId = argument7
		numTelefono = argument8
		direccion = argument9
		askerRut = argument10
		motivo = argument11
		yPositionInPanel = y-panelY-50
	}
	posY += ( sprite_get_height(sTicket) + 8 )
}