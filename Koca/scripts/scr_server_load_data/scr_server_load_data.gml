/// @param data_save_string

var saveString = argument0

var k = undefined
switch saveString
{
	case "ticket index": k=1 break
}

// If it's not defined, define it
if is_undefined(serverSaveMap[? saveString]) and (k != undefined)
{
	msg("'"+string(saveString) + "' is not defined, creating default value: " + string(k))
	serverSaveMap[? saveString] = k
	scr_save_all_general_data()
}
// If it is not defined in defualts, show warning message
else if (k == undefined) msg("WARNING: Load data value is undefined!!")

// Else return the value
else k = serverSaveMap[? saveString]

return k