/// @param rut
/// @param clave
/// @param nombre
/// @param numeroTelefonico
/// @param correoElectronico
/// @param empresa
/// @param numeroLocal
/// @param direccionLocal
/*Añadir data registro*/
var _rut = argument[0]

// Set first personal information
var list = ds_list_create_2()
for (var i = 0; i<saveSystemInfo.ticketsO; i++)
{
	list[| i] = argument[i]
}
// Create tickets offered, in wait and paid lists and dictionaries
var ticketsO, ticketsTEE, ticketsP, k

// Tickets Ofrecidos
k = saveSystemInfo.ticketsO
ticketsO = ds_list_create_2()
list[| k] = ticketsO
ds_list_mark_as_list(list, k)

// Tickets Tomados En Espera
k = saveSystemInfo.ticketsTEE
ticketsTEE = ds_list_create_2()
list[| k] = ticketsTEE
ds_list_mark_as_list(list, k)

// Tickets Pagados
k = saveSystemInfo.ticketsP
ticketsP = ds_list_create_2()
list[| k] = ticketsP
ds_list_mark_as_list(list, k)

// Save map
ds_map_add_list(serverAccountsRegistered, _rut, list)