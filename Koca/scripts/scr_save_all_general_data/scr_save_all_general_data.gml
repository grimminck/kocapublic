if client()
{
	scr_save_string_to_file("client_save_information.txt", json_encode(clientSaveMap))
	
	//var _wrapper = ds_map_create()
	//ds_map_add_list(_wrapper, "ROOT", myList)
	//scr_save_string_to_file("client_local_tickets_list_my_list.txt", json_encode(clientSaveMap))
	//ds_map_destroy(_wrapper)
	
	if instance_exists(oTicketsSave) scr_save_string_to_file("client_local_tickets_list.txt", json_encode(oTicketsSave.localTicketsMap))
}
else 
{
	scr_save_string_to_file("server_save_information.txt", json_encode(serverSaveMap))
	scr_save_string_to_file("server_accounts_registered.txt", json_encode(serverAccountsRegistered))
}

var buff1 = buffer_create(1, buffer_grow, 1)
var buff2 = buffer_create(1, buffer_grow, 1)
var buff3 = buffer_create(1, buffer_grow, 1)
var buff4 = buffer_create(1, buffer_grow, 1)
var buff5 = buffer_create(1, buffer_grow, 1)
var buff6 = buffer_create(1, buffer_grow, 1)

buffer_write(buff1, buffer_string, json_encode(ticketsSBAvailable))
buffer_save(buff1, "server_tickets_sb.txt")
	
buffer_write(buff2, buffer_string, json_encode(ticketsFasaAvailable))
buffer_save(buff2, "server_tickets_fasa.txt")
	
buffer_write(buff3, buffer_string, json_encode(ticketsCVAvailable))
buffer_save(buff3, "server_tickets_cv.txt")
	
buffer_write(buff4, buffer_string, json_encode(ticketsSBUnavailable))
buffer_save(buff4, "server_tickets_sb_unavailable.txt")
	
buffer_write(buff5, buffer_string, json_encode(ticketsFasaUnavailable))
buffer_save(buff5, "server_tickets_fasa_unavailable.txt")
	
buffer_write(buff6, buffer_string, json_encode(ticketsCVUnavailable))
buffer_save(buff6, "server_tickets_cv_unavailable.txt")
	
buffer_delete(buff1)
buffer_delete(buff2)
buffer_delete(buff3)
buffer_delete(buff4)
buffer_delete(buff5)
buffer_delete(buff6)