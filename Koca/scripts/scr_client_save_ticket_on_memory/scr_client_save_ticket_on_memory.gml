/// @param ticketId
/// @param ticketStatus
/// @description Guarda en la memoria del dispositivo, el ticket y su status para en otra 
/// @description instancia ser cargado sin conexion necesari a internet

var _ticketId = argument0
var _ticketStatus = argument1

// Add ticket to save list
with oTicketsSave
{
	ds_map_add(localTicketsMap, string(_ticketId), _ticketStatus)
}

scr_client_save_local_tickets()

///////////////////// ************************
//			
//			Diccionario - localTicketsMap
//			
//			key: id de ticket				valor: Status en string
//			key: id de ticket				valor: Status en string
//			key: id de ticket				valor: Status en string
//			key: id de ticket				valor: Status en string
//			key: id de ticket				valor: Status en string
//			key: id de ticket				.......
//
//			Se guardan tickets		"Ofreci"		"Ofrecido"		"Reservado"		"Pedido"
//									"Compre Turno"		"Vendi Turno"
//									