/// @function ds_list_delete_2
/// @param id
/// @param value
/// @description Searches for the value and deletes it from the list

var list = argument0
var value = argument1

var index = ds_list_find_index(list, value)
ds_list_delete(list, index)
