/// @description This script must be used in conjuction with scr_client_save_data_general_tickets_local
/// @description Copies the data from the save system into localTicketsMap dictionary
if file_exists("client_local_tickets_list.txt")
{
	msg("client_local_tickets_list exist, loading local tickets")
	
	with oTicketsSave
	{
		var buff = buffer_load("client_local_tickets_list.txt")
		var json = buffer_read(buff, buffer_string)
		ds_map_copy(localTicketsMap, json_decode(json))
	}
}