/// @param rut
/// @param saveSystemInfo_enum
var rut = argument0
var data_enum = argument1

var list = serverAccountsRegistered[? rut]
return list[| data_enum]