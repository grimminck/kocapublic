/// @param Rut

var rut_string = argument0
var rut = floor(real(rut_string))
var _string

if !is_undefined(ds_map_find_value(serverAccountsRegistered, rut_string)) _string = "El Rut ya esta en uso"
else if rut<30000000 or rut>400000000 _string = "Rut no es valido"
else _string = "Disponible"

return _string