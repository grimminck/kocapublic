/// @description  @function scr_server_send_buffer_back_to_client3(buffer, socket);
/// @param buffer
/// @param  socket

var buffer = argument[0]
var socket = argument[1]
network_send_packet(socket, buffer, buffer_tell(buffer))