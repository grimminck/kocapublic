var w1 = 720
var h1 = 1280

// Set final width and height depending on the device we are running
var ww, hh
if os_type==os_windows { ww=w1/1.9 hh=h1/1.9 }
if os_type==os_android { ww=w1 hh=h1 }

// Actually set window size and application surface sizing (I don't know what the fuck I'm talking about)
window_set_size(ww, hh)
if os_type==os_windows surface_resize(application_surface, ww, hh)
if os_type==os_android surface_resize(application_surface, ww, hh)