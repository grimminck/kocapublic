/// @param Farmacia

var farmacia = argument0

var a, b, list, list2, map
switch farmacia {
	case farmacias.sb: a="SB" b="SALCO" list=socketsSB list2=rutsClientesSB map=mapSocketClienteSB break	
	case farmacias.fasa: a="Fasa" b="FASA" list=socketsFasa list2=rutsClientesFasa map=mapSocketClienteFasa break	
	case farmacias.cv: a="CV" b="CRUZVERDE" list=socketsCV list2=rutsClientesCV map=mapSocketClienteCV break	
}

msg("--- START "+b+" NETWORK INFO ---")
msg("List of Sockets "+a)
var _size = ds_list_size(list)
msg("Sockets "+a+": "+string(_size))
for(var i=0; i<_size; i++)
{
	var socket = list[| i]
	msg("Socket: "+string(socket))	
	msg("Rut associated with socket: "+string(map[? socket]))
}
var _size = ds_list_size(list2)
msg("Ruts "+a+": "+string(_size))
for(var i=0; i<_size; i++)
{
	msg("Rut: "+string(list2[| i]))	
}
msg("--- END "+b+" NETWORK INFO ---")