msg("A Turn has been offered")
/// @param read

var read = argument[0]

var command = 2

if read
{
	var buffer = argument[1]
	
	// Read the information
	if client() var _ticketId = buffer_read(buffer, buffer_u64)
	else		var _ticketId = ticketIndex
	var _empresa	= buffer_read(buffer, buffer_u8)
	var _rut		= buffer_read(buffer, buffer_u32)
	var _horaI		= buffer_read(buffer, buffer_u8)
	var _minutoI	= buffer_read(buffer, buffer_u8)
	var _horaF		= buffer_read(buffer, buffer_u8)
	var _minutoF	= buffer_read(buffer, buffer_u8)
	var _dia 		= buffer_read(buffer, buffer_u8)
	var _mes 		= buffer_read(buffer, buffer_u8)
	var _anno 		= buffer_read(buffer, buffer_u16)
	var _motivo		= buffer_read(buffer, buffer_string)
	var _nombre		= buffer_read(buffer, buffer_string)
	var _numTel		= buffer_read(buffer, buffer_string)
	var _correo		= buffer_read(buffer, buffer_string)
	var _numLocal	= buffer_read(buffer, buffer_string)
	var _dirLocal	= buffer_read(buffer, buffer_string)
	
	// Client and server creates ticket
	var ticket = scr_create_ticket(_ticketId, "Ofrecido", _empresa, _rut,
						_horaI, _minutoI, _horaF, _minutoF,
						_dia, _mes, _anno,
						_nombre, _numTel, _correo, _numLocal, _dirLocal,
						_motivo)

	// If client, add to panels
	if client() 
	{
		// Offerer creates the ticket on its personal panel
		msg("am i the creator? "+string(_rut==miRut))
		if (_rut==miRut)
		{
			var _fecha = string(_dia)+"/"+string(_mes)
			var _hora = string(_horaI)+":"+string(_minutoI)+" - "+string(_horaF)+":"+string(_minutoF)
			scr_create_ticket_panel(oTicketOffered, oPanel1Ofrecidos, _rut, _fecha, _hora, "Las Condes", _empresa, _ticketId, _numTel, _dirLocal, 0, _motivo)
			scr_client_save_ticket_on_memory(_ticketId, "Ofreci")
		}
		else
		{
			scr_create_ticket_panel_3(ticket, oPanel5)
			scr_client_save_ticket_on_memory(_ticketId, "Ofrecido")
		}
	}
	
	// if server, send to firm
	else
	{
		scr_transmission_offer_ticket(false, _ticketId, _empresa, _rut,
												_horaI, _minutoI, _horaF, _minutoF,
												_dia, _mes, _anno,
												_nombre, _numTel, _correo, _numLocal, _dirLocal,
												_motivo)
	}
	
}
else if !disconnectedAccount
{
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)
	
	if client()
	{
		var horaInicial = ofrecerHoraInicial
		var horaFinal = ofrecerHoraFinal
		if !ofrecerHoraInicialAM horaInicial+=12
		if !ofrecerHoraFinalAM horaFinal+=12
		
		buffer_write(buff, buffer_u8, miFarmacia)
		buffer_write(buff, buffer_u32, miRut)
		buffer_write(buff, buffer_u8, horaInicial)
		buffer_write(buff, buffer_u8, ofrecerMinutoInicial)
		buffer_write(buff, buffer_u8, horaFinal)
		buffer_write(buff, buffer_u8, ofrecerMinutoFinal)
		buffer_write(buff, buffer_u8, ofrecerDia)
		buffer_write(buff, buffer_u8, ofrecerMes)
		buffer_write(buff, buffer_u16, ofrecerAnno)
		buffer_write(buff, buffer_string, ofrecerMotivo)
		with oNetwork {
		buffer_write(buff, buffer_string, nombre)
		buffer_write(buff, buffer_string, numeroTelefonico)
		buffer_write(buff, buffer_string, correoElectronico)
		buffer_write(buff, buffer_string, numeroLocal)
		buffer_write(buff, buffer_string, direccionLocal) }

		network_send_packet(Sock, buff, buffer_tell(buff))
	}
	else
	{
		var iOffset = 1 // Initial offset (Number of arguments to "leap")
		var args, i=0 repeat(argument_count-iOffset) { args[i] = argument[i+iOffset];  i++ }
		
		var _index = args[@ 0]
		var _empresa = args[@ 1]
		var _rut = args[@ 2]
		var _horaI = args[@ 3]
		var _minutoI = args[@ 4]
		var _horaF = args[@ 5]
		var _minutoF = args[@ 6]
		var _dia = args[@ 7]
		var _mes = args[@ 8]
		var _anno = args[@ 9]
		var _nombre = args[@ 10]
		var _numTel = args[@ 11]
		var _correo = args[@ 12]
		var _numLocal = args[@ 13]
		var _dirLocal = args[@ 14]
		var _motivo = args[@ 15]
		
		buffer_write(buff, buffer_u64, _index)
		buffer_write(buff, buffer_u8, _empresa)
		buffer_write(buff, buffer_u32, _rut)
		buffer_write(buff, buffer_u8, _horaI)
		buffer_write(buff, buffer_u8, _minutoI)
		buffer_write(buff, buffer_u8, _horaF)
		buffer_write(buff, buffer_u8, _minutoF)
		buffer_write(buff, buffer_u8, _dia)
		buffer_write(buff, buffer_u8, _mes)
		buffer_write(buff, buffer_u16, _anno)
		buffer_write(buff, buffer_string, _motivo)
		buffer_write(buff, buffer_string, _nombre)
		buffer_write(buff, buffer_string, _numTel)
		buffer_write(buff, buffer_string, _correo)
		buffer_write(buff, buffer_string, _numLocal)
		buffer_write(buff, buffer_string, _dirLocal)
		
		scr_server_broadcast_buffer_to_firm(buff, _empresa)
	}
}