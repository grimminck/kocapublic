/// @param ticket_id

var _ticketId = argument0

var size = ds_list_size(myList)
for (var i=0; i<size; i++)
{
	var tik = myList[| i]
	if tik[? "id"] == _ticketId 
		return tik
}
return -1