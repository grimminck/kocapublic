/// @param ticket
/// @param panel
/// @param ticketId
/// @description Creates a ticket on the desired panel based on the saved ones

var ticket = argument0
var panel = argument1
var _ticketId = argument2

with panel
{
	var xx = x
	var yy = y+150
	var panelY = y
	if panel==oPanel1Ofrecidos { xx=x-sprite_get_height(sPanel1); }
	if panel==oPanel3 { yy=y+80 }
	var tiket = instance_create(xx+50, yy+50+posY, ticket)
	with tiket
	{
		var tik = scr_get_ticket(_ticketId)
		
		msg_2("tik", tik)
		if tik>-1
		{
			status = tik[? "status"]
			rut = tik[? "rut"]
			fecha = string(tik[? "dia"])+"/"+string(tik[? "mes"])+"/"+string(tik[? "anno"])
			hora = string(tik[? "hora inicial"])+":"+string(tik[? "minuto inicial"])+" / "+string(tik[? "hora final"])+":"+string(tik[? "minuto final"])
			sector = "Las Condes"
			farmacia = tik[? "empresa"]
			ticketId = tik[? "id"]
			numTelefono = tik[? "numeroTelefonico"]
			direccion = tik[? "dirrecionLocal"]
			askerRut = 0
			motivo = tik[? "motivo"]
			yPositionInPanel = y-panelY-50
		}
	}
	posY += ( sprite_get_height(sTicket) + 8 )
}
return tiket