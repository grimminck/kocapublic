/// @param file_name
/// @param string
/// @description 

var fileName = argument0
var _string = argument1

var _buff = buffer_create( string_byte_length(_string) +1, buffer_fixed, 1)
buffer_write(_buff, buffer_string, _string)
buffer_save(_buff, fileName)
buffer_delete(_buff)