/// @param ticket_index_from_myList
/// @param panel

var ticket = argument0
var panel = argument1

with panel
{
	var xx = x-sprite_get_height(sPanel1)
	with instance_create_depth(xx+50, y+200+posY, depth-20, oTicketToTake)
	{
		numTelefono = ticket[? "numeroTelefonico"]
		rut = ticket[? "rut"]
		ticketId = ticket[? "id"]
		fecha = string(ticket[? "dia"])+"/"+string(ticket[? "mes"])
		hora = string(ticket[? "hora inicial"])+":"+string(ticket[? "minuto inicial"])+" - "+string(ticket[? "hora final"])+":"+string(ticket[? "minuto final"])
		direccion = ticket[? "direccionLocal"]
		numeroLocal = ticket[? "numeroLocal"]
		nombre = ticket[? "nombre"]
		status = ticket[? "status"]
		motivo = ticket[? "motivo"]
	}
	posY += ( sprite_get_height(sTicket) + 8 )
}