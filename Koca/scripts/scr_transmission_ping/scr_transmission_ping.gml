/// @param receive
/// @param buffer*
/// @param socket*

var read = argument[0]

if client()
{
	if !read and !disconnectedAccount
	{
		if waitingForPing {
			// Set when to change circle to red
			if it_was_connected { 
				scr_show_alert("Perdiendo Conexion")
				alarm[1] = room_speed*4.5
				it_was_connected = false }
			losingConnection = true
			// Set when to connect the client again
			if !clientConnecting && alarm[2]<0 { alarm[2]=room_speed*10 }
			clientConnected = false
		}
	
		waitingForPing = true
		// Send ping buffer
		network_send_packet(Sock, pingBuffer, 1)
	}
	else { waitingForPing = false }
}
// Server - Send ping buffer back to client
else {
	var buffer = argument[1]
	var socket = argument[2]
	scr_server_send_buffer_back_to_client(buffer, socket)
}