// Create alert object and set its text
if instance_exists(oAlertMessage) with oAlertMessage instance_destroy()
with instance_create_depth(room_width/2, room_height/2, -1000, oAlertMessage)
	text = argument0