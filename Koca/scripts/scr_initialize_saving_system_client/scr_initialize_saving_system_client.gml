globalvar clientSaveMap; clientSaveMap = ds_map_create()

if client()
{
	// Create save/load map or load it if it already exists
	if file_exists("client_save_information.txt") 
	{
		msg("Saved data file found")
		scr_load_all_client_general_data_1()
		scr_client_save_data_general("times entered", scr_client_load_data_general("times entered") + 1)
	}
	else 
	{
		msg("Save file does not exist, creating one")
		scr_client_save_data_general("times entered", 1)
	
		/*Añadir data registro*/
		scr_client_save_data_general("registered", 0)
		scr_client_save_data_general("rut", "")
		scr_client_save_data_general("password", "")
		scr_client_save_data_general("name", "")
		scr_client_save_data_general("firm", "")
		scr_client_save_data_general("email", "")
		scr_client_save_data_general("phone number", "")
		scr_client_save_data_general("shop id", "")
		scr_client_save_data_general("shop address", "")
	}
	
	msg("times entered into the app: "+string(scr_client_load_data_general("times entered")))
	
	scr_save_all_general_data()
}