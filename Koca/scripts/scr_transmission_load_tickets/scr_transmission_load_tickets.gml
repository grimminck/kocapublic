/// @param read

msg("Load tickets transmission")

var read = argument[0]

var command = 3

if read
{
	var buffer = argument[1]
	var socket = argument[2]
	
	if client()
	{
		var a,b,c,d,f,g,h,j,k,l,m,n,o,p,q,r
		var listSize = buffer_read(buffer, buffer_u32)
		for (var i = 0; i<listSize; i++)
		{
			a=buffer_read(buffer, buffer_u64)
			b=buffer_read(buffer, buffer_string)
			c=buffer_read(buffer, buffer_u8)
			d=buffer_read(buffer, buffer_u32)
			f=buffer_read(buffer, buffer_string)
			g=buffer_read(buffer, buffer_u8)
			h=buffer_read(buffer, buffer_u8)
			j=buffer_read(buffer, buffer_u8)
			k=buffer_read(buffer, buffer_u8)
			l=buffer_read(buffer, buffer_u16)
			m=buffer_read(buffer, buffer_u8)
			n=buffer_read(buffer, buffer_u16)
			o=buffer_read(buffer, buffer_string)
			p=buffer_read(buffer, buffer_string)
			q=buffer_read(buffer, buffer_string)
			r=buffer_read(buffer, buffer_string)
			s=buffer_read(buffer, buffer_string)
			scr_create_ticket(a,b,c,d,g,h,j,k,l,m,n,f,o,p,q,r,s)
		}
	}
	else
	{
		var _empresa = buffer_read(buffer, buffer_u8)
		
		scr_transmission_load_tickets(false, _empresa, socket)
	}
}
else if !disconnectedAccount
{
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)
	
	if client()
	{
		buffer_write(buff, buffer_u8, miFarmacia)

		network_send_packet(Sock, buff, buffer_tell(buff))
	}
	else
	{
		var _empresa = argument[1]
		var socket = argument[2]
		
		// Buscar todos los tickets de esa farmacia y enviarselos a este socket
		var ticketsONList = scr_get_firm_list_with_firm_enum(_empresa)
		var listSize = ds_list_size(ticketsONList)
		msg("Size of tickets list (sending tickets information to logged client): "+string(listSize))
	
		var ticketMap
		buffer_write(buff, buffer_u32, listSize)
		for (var i = 0; i<listSize; i++)
		{
			ticketMap = ticketsONList[| i]
			buffer_write(buff, buffer_u64, ticketMap[? "id"])
			buffer_write(buff, buffer_string, ticketMap[? "status"])
			buffer_write(buff, buffer_u8, ticketMap[? "empresa"])
			buffer_write(buff, buffer_u32, ticketMap[? "rut"])
			buffer_write(buff, buffer_string, ticketMap[? "nombre"])
			buffer_write(buff, buffer_u8, ticketMap[? "hora inicial"])
			buffer_write(buff, buffer_u8, ticketMap[? "minuto inicial"])
			buffer_write(buff, buffer_u8, ticketMap[? "hora final"])
			buffer_write(buff, buffer_u8, ticketMap[? "minuto final"])
			buffer_write(buff, buffer_u16, ticketMap[? "dia"])
			buffer_write(buff, buffer_u8, ticketMap[? "mes"])
			buffer_write(buff, buffer_u16, ticketMap[? "anno"])
			buffer_write(buff, buffer_string, ticketMap[? "numeroTelefonico"])
			buffer_write(buff, buffer_string, ticketMap[? "correoElectronico"])
			buffer_write(buff, buffer_string, ticketMap[? "numeroLocal"])
			buffer_write(buff, buffer_string, ticketMap[? "direccionLocal"])
			buffer_write(buff, buffer_string, ticketMap[? "motivo"])
		}
	
		network_send_packet(socket, buff, buffer_tell(buff))
	}
}