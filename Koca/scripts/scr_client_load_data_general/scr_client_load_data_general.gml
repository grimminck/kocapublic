/// @param data_save_string
/// @description Attempt to load a string from the save system
/// If it does not exist* inside the save system, add it 

var saveString = argument0

var k = undefined
switch saveString
{
	case "times entered": k=1 break
	case "registered": k=0 break
	case "rut": k="" break
	case "password": k="" break
	case "name": k="" break
	case "firm": k="" break
	case "email": k="" break
	case "phone number": k="" break
	case "shop id": k="" break
	case "shop address": k="" break
}

// If it's not defined, define it
if is_undefined(clientSaveMap[? saveString]) and (k != undefined)
{
	msg("'"+string(saveString) + "' is not defined, creating default value: " + string(k))
	clientSaveMap[? saveString] = k
	scr_save_all_general_data()
}
// If it is not defined in defualts, show warning message
else if (k == undefined) msg("WARNING: Load data value is undefined!!")

// Else return the value
else k = clientSaveMap[? saveString]

return k

/// * If it does not exist: Lo unico que falta es que permita crear la variable cualquiera sea su nombre, dándole su tipo (String, Real)