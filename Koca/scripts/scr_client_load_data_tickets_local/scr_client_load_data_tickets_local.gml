/// @desc Load tickets on panels

// Recorre diccionario de tickets locales
var _ticketId = ds_map_find_first(localTicketsMap)
repeat(ds_map_size(localTicketsMap))
{
	var _status = localTicketsMap[? _ticketId]
			
	msg_2("_ticketId", _ticketId)
	msg_2("_status", _status)
	
	_ticketId = floor(real(_ticketId))
	// Agrega al panel indicado
	switch _status
	{
		case "Ofreci": scr_create_ticket_panel_2(oTicketOffered, oPanel1Ofrecidos, _ticketId) break
		case "Ofreci pagado": scr_create_ticket_panel_2(oTicketOffered, oPanel1Ofrecidos, _ticketId) break
		
		case "Reservado pagado":	scr_create_ticket_panel_2(oTicketAskedPedidos, oPanel7Pedidos, _ticketId)
									scr_create_ticket_panel_2(oTicketAsked, oPanel3, _ticketId) break
		case "Reservado":			scr_create_ticket_panel_2(oTicketAskedPedidos, oPanel7Pedidos, _ticketId)
									scr_create_ticket_panel_2(oTicketAsked, oPanel3, _ticketId) break
									
		case "Pedido": scr_create_ticket_panel_2(oTicketAsked, oPanel3, _ticketId) break
		
		case "Ofrecido": scr_create_ticket_panel_2(oTicketToTake, oPanel5, _ticketId) break
	}
			
	_ticketId = string(_ticketId)
	_ticketId = ds_map_find_next(localTicketsMap, _ticketId)
}