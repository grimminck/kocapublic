/// @function ds_list_create_2
/// @description Create, clears and returns the list

var list = ds_list_create()
ds_list_clear(list)
return list