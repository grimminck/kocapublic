/// @param rut
/// @param farmacia
msg(" --- START Get socket with rut --- ")

var _rut = argument0
msg_real("rut: ", _rut)
if is_real(_rut) _rut = string(_rut)
var _farmacia = argument1

var socket = undefined
var _socketsList
var _rutsList

switch _farmacia
{
	case farmacias.sb:		_socketsList = socketsSB	_rutsList = rutsClientesSB break
	case farmacias.fasa:	_socketsList = socketsFasa	_rutsList = rutsClientesFasa break
	case farmacias.cv:		_socketsList = socketsCV	_rutsList = rutsClientesCV break
}

var listIndex = ds_list_find_index(_rutsList, _rut)
msg("listIndex: "+string(listIndex))
socket = _socketsList[| listIndex]

msg("Returning socket: "+string(socket))
msg(" --- END Get socket with rut --- ")
return socket
