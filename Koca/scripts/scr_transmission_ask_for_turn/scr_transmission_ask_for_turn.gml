/// @param read

var read = argument[0]

var command = 4

if read
{
	var buffer = argument[1]
	
	var _rut = buffer_read(buffer, buffer_string)
	var _fecha = buffer_read(buffer, buffer_string)
	var _hora = buffer_read(buffer, buffer_string)
	var _sector = buffer_read(buffer, buffer_string)
	var _ticketId = buffer_read(buffer, buffer_u64)
	var _farmacia = buffer_read(buffer, buffer_u8)
	var _numTelefono = buffer_read(buffer, buffer_string)
	var _direccion = buffer_read(buffer, buffer_string)
	var _askerRut = buffer_read(buffer, buffer_string)
	var _motivo = buffer_read(buffer, buffer_string)
	
	if client()
	{
		if _rut==miRut
		{
			// A client has requested to take your turn
			scr_show_alert("Te han pedido un turno")
			// Activate alarm button if not active
			if !instance_exists(oButtonConfirmTurnRequest) 
				instance_create_depth(550, 60, -20, oButtonConfirmTurnRequest)
		
			scr_create_ticket_panel(oTicketToConfirm, oPanel2, _rut, _fecha, _hora, _sector, _farmacia, _ticketId, _numTelefono, _direccion, _askerRut, _motivo)
		}
		else
		{
			// A client has requested to take your turn
			scr_show_alert("Pediste un turno exitosamente")
		
			scr_create_ticket_panel(oTicketAsked, oPanel3, _rut, _fecha, _hora, _sector, _farmacia, _ticketId, _numTelefono, _direccion, _askerRut, _motivo)
			scr_create_ticket_panel(oTicketAskedPedidos, oPanel7Pedidos, _rut, _fecha, _hora, _sector, _farmacia, _ticketId, _numTelefono, _direccion, _askerRut, _motivo)
			scr_client_save_ticket_on_memory(_ticketId, "Pedido")
			// Buscar el ticket y cambiar su status a Pedido
			scr_client_change_ticket_status(_ticketId, "Pedido")
			// Disminuir en 1 la cantidad de tickets disponibles
			oUserController.ticketsAvailable--
		}
	}
	else
	{
		// Mandar el mensaje al creador del ticket
		var destSocket = scr_get_socket_with_rut(_rut, _farmacia)
		if !is_undefined(destSocket) network_send_packet(destSocket, buffer, buffer_tell(buffer))
		else msg("Turn asked for someone offline, rut: "+_rut)
		
		// Mandar el mensaje al que pidio el ticket
		var destSocket = scr_get_socket_with_rut(_askerRut, _farmacia)
		if !is_undefined(destSocket) network_send_packet(destSocket, buffer, buffer_tell(buffer))
	}

}
else if !disconnectedAccount
{
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)
	
	if client()
	{
		buffer_write(buff, buffer_string, rut)
		buffer_write(buff, buffer_string, fecha)
		buffer_write(buff, buffer_string, hora)
		buffer_write(buff, buffer_string, sector)
		buffer_write(buff, buffer_u64, ticketId)
		buffer_write(buff, buffer_u8, miFarmacia)
		buffer_write(buff, buffer_string, numTelefono)
		buffer_write(buff, buffer_string, direccion)
		buffer_write(buff, buffer_string, miRut)
		buffer_write(buff, buffer_string, motivo)
		
		network_send_packet(Sock, buff, buffer_tell(buff))
	}
}