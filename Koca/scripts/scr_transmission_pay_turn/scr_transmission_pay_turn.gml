/// @param read

var read = argument[0]

var command = 6

if read
{
	var buffer = argument[1]

	msg("Pay turn")

	var _rut = buffer_read(buffer, buffer_string)
	var _fecha = buffer_read(buffer, buffer_string)
	var _hora = buffer_read(buffer, buffer_string)
	var _sector = buffer_read(buffer, buffer_string)
	var _ticketId = buffer_read(buffer, buffer_u64)
	var _farmacia = buffer_read(buffer, buffer_u8)
	var _numTelefono = buffer_read(buffer, buffer_string)
	var _direccion = buffer_read(buffer, buffer_string)
	var _askerRut = buffer_read(buffer, buffer_string)
	var _motivo = buffer_read(buffer, buffer_string)
	
	if client()
	{
		// Yo ofreci el turno
		if (miRut==_rut)
		{
			// A client has paid a turn that you offered
			scr_show_alert("Te han pagado un turno")
		
			// Cambiar estado del ticket a pagado			
			with oTicketOffered if _ticketId==ticketId
				status = "Ofreci pagado"
				
			scr_client_save_ticket_on_memory(_ticketId, "Ofreci pagado")
			scr_client_change_ticket_status(_ticketId, "Ofreci pagado")
		}
		// Yo pedí el turno
		else
		{
			msg("Turno pagado exitosamente")
			
			with oTicketAsked if _ticketId==ticketId
				status = "Reservado pagado"
			
			scr_client_save_ticket_on_memory(_ticketId, "Reservado pagado")
			scr_client_change_ticket_status(_ticketId, "Reservado pagado")
			
			// Borra boton si no quedan tickets por pagar
			var i = 0
			with oTicketAsked
			{
				if status=="Reservado" i++
			}
			if i==0 instance_destroy(oButtonPayTurnAlert)
		}
	}
	else
	{
		// Mandar el mensaje al socket destino
		var destSocket = scr_get_socket_with_rut(_rut, _farmacia)
		if !is_undefined(destSocket) network_send_packet(destSocket, buffer, buffer_tell(buffer))
		else msg("Turn paid for someone offline, rut: "+_rut)
		
		var destSocket = scr_get_socket_with_rut(_askerRut, _farmacia)
		if !is_undefined(destSocket) network_send_packet(destSocket, buffer, buffer_tell(buffer))
	}
}
else if !disconnectedAccount
{
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)
	
	if client()
	{		
		buffer_write(buff, buffer_string, rut)
		buffer_write(buff, buffer_string, fecha)
		buffer_write(buff, buffer_string, hora)
		buffer_write(buff, buffer_string, sector)
		buffer_write(buff, buffer_u64, ticketId)
		buffer_write(buff, buffer_u8, miFarmacia)
		buffer_write(buff, buffer_string, numTelefono)
		buffer_write(buff, buffer_string, direccion)
		buffer_write(buff, buffer_string, askerRut)
		buffer_write(buff, buffer_string, motivo)
		
		network_send_packet(Sock, buff, buffer_tell(buff))
		
	}
	else {}
}