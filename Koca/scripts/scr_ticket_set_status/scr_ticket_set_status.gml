/// @param farmacia_enu
/// @param ticket_id
/// @param status

var farmacia_enum = argument0
var ticketId = argument1
var status = argument2

var firm_list = scr_get_firm_list_with_firm_enum(farmacia_enum)

var list_size = ds_list_size(firm_list)
for (var i = 0; i<list_size; i++)
{
	var ticket = firm_list[| i]
	
	if ticket[? "id"] == ticketId
	{
		msg("Set ticket ("+string(ticketId)+") status to: "+status)
		ticket[? "status"] = status
	}
}