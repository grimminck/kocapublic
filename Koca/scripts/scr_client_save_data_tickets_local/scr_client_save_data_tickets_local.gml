/// @description Save all local tickets data stored in the client


var buff1 = buffer_create(1, buffer_grow, 1)

buffer_write(buff1, buffer_string, json_encode(localTicketsMap))

buffer_save(buff1, "client_local_tickets_list.txt")

buffer_delete(buff1)

msg("Saved local tickets information")