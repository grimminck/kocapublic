globalvar serverSaveMap;				serverSaveMap = ds_map_create()
globalvar serverAccountsRegistered;		serverAccountsRegistered = ds_map_create()
	
// File exists
if file_exists("server_save_information.txt") {
	msg("Saved data file found")
	scr_load_all_server_general_data()
}
// File does not exist yet
else {
	msg("Save file does not exist, creating default")
	serverSaveMap[? "Ticket Index"] = ticketIndex
	scr_save_all_general_data()
}