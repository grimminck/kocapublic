/// @param ticketId
/// @param status

var _tickId = argument0
var _status = argument1

// My List = Lista de todos los tickets juntos
var size = ds_list_size(myList)
for (var i=0; i<size; i++)
{
	var ticket = myList[| i]
	
	if ticket[? "id"] == _tickId
		ticket[? "status"] = _status
}
scr_save_all_general_data()

// Cambiar status en local tickets y guardar
// Recorre diccionario de tickets locales
with oTicketsSave
{
	var _ticketId = ds_map_find_first(localTicketsMap)
	repeat(ds_map_size(localTicketsMap))
	{
		if _ticketId==_tickId localTicketsMap[? _ticketId]=_status
		_ticketId = ds_map_find_next(localTicketsMap, _ticketId)
	}
}
scr_client_save_local_tickets()