/// @param read

var read = argument[0]

var command = 1
	
if read
{
	var buffer = argument[1]
	var _socket = argument[2]

	msg("receiving login buffer")

	// Add to ON list
	if server()
	{
		var _rut = buffer_read(buffer, buffer_string)
		var _clave = buffer_read(buffer, buffer_string)
			
		msg_real("rut", _rut)
		msg_real("clave", _clave)
			
		var _login_string = scr_check_rut_and_password(_rut, _clave)
		
		msg(_login_string)
			
		if _login_string=="Conexion exitosa"
		{
			// Get firm
			var _empresa = scr_server_get_client_data(_rut, saveSystemInfo.empresa)
			// Message
			msg("Client logged in, rut: "+string(_rut))
			msg("Client logged in with firm: "+string(_empresa))
			// Add to important structures
			scr_add_client_to_structures(_empresa, _rut, _socket)
		}
			
		scr_transmission_login(false, _login_string, _rut, _socket)
	}
	else {
		var _login_string = buffer_read(buffer, buffer_string)
	
		if _login_string=="Conexion exitosa"
		{
			msg(_login_string)
		
			// Read reminder of information
			var _rut =					buffer_read(buffer, buffer_string)
			var _clave =				buffer_read(buffer, buffer_string)
			var _nombre =				buffer_read(buffer, buffer_string)	
			var _numeroTelefonico =		buffer_read(buffer, buffer_string)	
			var _correoElectronico =	buffer_read(buffer, buffer_string)	
			var _empresa =				buffer_read(buffer, buffer_string)
			var _numeroLocal =			buffer_read(buffer, buffer_string)
			var _direccionLocal =		buffer_read(buffer, buffer_string)
		
			if room==rmLobby scr_change_room(rmHall, 1)
			scr_client_set_account(_rut, _clave, _nombre, _numeroTelefonico, _correoElectronico, _empresa, _numeroLocal, _direccionLocal)
			// Ask for tickets
			scr_transmission_load_tickets(false)
			// Load second part of general data
			scr_load_all_client_general_data_2(scr_client_get_firm())
		}
		else scr_show_alert(_login_string)
		
	}
}
else if !disconnectedAccount
{
	/*Añadir data registro*/
	// Write the buffer
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)

	if client() 
	{
		var _rut = argument[1]
		var _clave = argument[2]
		
		buffer_write(buff, buffer_string, _rut)
		buffer_write(buff, buffer_string, _clave)
	
		// Send buffer
		network_send_packet(Sock, buff, buffer_tell(buff))
	}
	else 
	{
		var _login_string = argument[1]
		var _rut = argument[2]
		var socket = argument[3]
		
		buffer_write(buff, buffer_string, _login_string)
	
		// Write client information
		if _login_string=="Conexion exitosa" {
			for(var i = 0; i<=saveSystemInfo.direccionLocal; i++) // Manda datos del cliente hasta direccion del local
				buffer_write(buff, buffer_string, scr_server_get_client_data(_rut, i))
		}
	
		network_send_packet(socket, buff, buffer_tell(buff))
	}
}