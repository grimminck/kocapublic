/// @param read

var read = argument[0]

var command = 0
	
if read
{
	var buffer = argument[1]
	var _socket = argument[2]
	msg("Receiving registering buffer")
	/*Añadir data registro*/
	if server()
	{
		var _rut = buffer_read(buffer, buffer_string)
		var _clave = buffer_read(buffer, buffer_string)
		var _nombre = buffer_read(buffer, buffer_string)	
		var _numeroTelefonico = buffer_read(buffer, buffer_string)	
		var _correoElectronico = buffer_read(buffer, buffer_string)	
		var _empresa = buffer_read(buffer, buffer_string)
		var _numeroLocal = buffer_read(buffer, buffer_string)
		var _direccionLocal = buffer_read(buffer, buffer_string)
			
		var _reg_string = scr_check_rut(_rut)
			
		if _reg_string=="Disponible" scr_server_register_account(_rut, _clave, _nombre, _correoElectronico, _numeroTelefonico, _empresa, _numeroLocal, _direccionLocal)
			
		scr_transmission_register(false, _reg_string, _socket)
	}
	else {
		var _reg_string = buffer_read(buffer, buffer_string)
		if _reg_string=="Disponible" {
			msg("Registro Completado")
			if gRememberLogin scr_client_save_data_general("registered", 1)
			scr_transmission_login(false, rut, clave)
			scr_save_all_general_data()
			scr_change_room(rmHall, 0)
		}
		else scr_show_alert(_reg_string)
	}
}
else if !disconnectedAccount
{
	buffer_seek(buff, buffer_seek_start, 0)
	buffer_write(buff, buffer_u16, command)
	
	if client()
	{		
		oNetwork.rut = registroRut
		oNetwork.clave = registroClave
		
		/*Añadir data registro*/
		// Write the buffer
		buffer_write(buff, buffer_string, registroRut)
		buffer_write(buff, buffer_string, registroClave)
		buffer_write(buff, buffer_string, registroNombre)
		buffer_write(buff, buffer_string, registroNumeroTel)
		buffer_write(buff, buffer_string, registroCorreo)
		buffer_write(buff, buffer_string, registroFarmacia)
		buffer_write(buff, buffer_string, registroNumeroLocal)
		buffer_write(buff, buffer_string, registroDireccionLocal)

		// Send buffer
		network_send_packet(Sock, buff, buffer_tell(buff))
	}
	else
	{
		/// @param available
		/// @param socket
		var available = argument[1]
		var socket = argument[2]

		buffer_write(buff, buffer_string, available)

		// Send buffer
		network_send_packet(socket, buff, buffer_tell(buff))
	}
	
}