with oPanel2
{
	var a = sprite_get_height(sTicket) + 8
	// Disminuir posY de panel
	posY -= a
	// Si posY queda en 0, eliminar botón de atención
	if posY == 0 
	{
		locked = false
		instance_destroy(oButtonConfirmTurnRequest)
	}
}

var _y = y
with oTicketToConfirm 
{
	if (y > _y)	y -= a
}