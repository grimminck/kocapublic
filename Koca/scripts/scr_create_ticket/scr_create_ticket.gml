// Add ticket and all its information to the selected list
/// @param ticketId
/// @param status
/// @param empresa
/// @param rut
/// @param hora_inicial
/// @param minuto_inicial
/// @param hora_final
/// @param minuto_final
/// @param dia
/// @param mes
/// @param año
/// @param ...

var _ticketId	= argument[0]
var _status		= argument[1]
var _empresa	= argument[2]
var _rut		= argument[3]
var _horaI 		= argument[4]
var _minutoI 	= argument[5]
var _horaF 		= argument[6]
var _minutoF 	= argument[7]
var _dia 		= argument[8]
var _mes 		= argument[9]
var _anno 		= argument[10]
var _nombre 	= argument[11]
var _numTel 	= argument[12]
var _correo 	= argument[13]
var _numLocal 	= argument[14]
var _dirLocal 	= argument[15]
var _motivo 	= argument[16]

msg("Comando crear ticket activado, empresa: "+string(_empresa)+". Nombre: "+string(_nombre))

var ticket = ds_map_create()

// Uses order of ticketInfo enum
ds_map_add(ticket, "id", _ticketId)
ds_map_add(ticket, "datetime", date_create_datetime(_anno, _mes, _dia, _horaI, _minutoI, 0))
ds_map_add(ticket, "status", _status)
ds_map_add(ticket, "empresa", _empresa)
ds_map_add(ticket, "rut", _rut)
ds_map_add(ticket, "nombre", _nombre)
ds_map_add(ticket, "hora inicial", _horaI)
ds_map_add(ticket, "minuto inicial", _minutoI)
ds_map_add(ticket, "hora final", _horaF)
ds_map_add(ticket, "minuto final", _minutoF)
ds_map_add(ticket, "dia", _dia)
ds_map_add(ticket, "mes", _mes)
ds_map_add(ticket, "anno", _anno)
ds_map_add(ticket, "numeroTelefonico", _numTel)
ds_map_add(ticket, "correoElectronico", _correo)
ds_map_add(ticket, "numeroLocal", _numLocal)
ds_map_add(ticket, "direccionLocal", _dirLocal)
ds_map_add(ticket, "motivo", _motivo)

var ticketList = scr_get_firm_list_with_firm_enum(_empresa)

// Add to list and mark as map
// Compara fecha y hora para tener la lista ordenada de tickets
var listSize = ds_list_size(ticketList)
if listSize==0
{
	ds_list_add(ticketList, ticket)
}
else
{
	var ticketToCompare
	for (var i = 0; i<listSize; i++)
	{
		ticketToCompare = ticketList[| i]
		// Bubble sort
		if date_compare_datetime(ticket[? "datetime"], ticketToCompare[? "datetime"]) == -1
		{
			ds_list_insert(ticketList, i, ticket)
			break
		}
		// Add to last if bubble sort ended
		if i==listSize-1 ds_list_add(ticketList, ticket)
	}
}
ds_list_mark_as_map(ticketList, ds_list_find_index(ticketList, ticket))

// Message
msg("Ticket created: "+string(_dia)+"/"+string(_mes)+"/"+string(_anno)+" from "+string(_horaI)+":"+string(_minutoI)+" until "+string(_horaF)+":"+string(_minutoF)+". Ticket number: "+string(_ticketId))

if server() ticketIndex++
else
{
	// Añadir a tickets disponibles si no eres el dueño del ticket
	if (_rut!=miRut) 
	{
		if instance_exists(oTurnosDisponibles) oTurnosDisponibles.number++
		oUserController.ticketsAvailable++
	}
}

scr_save_all_general_data()

return ticket