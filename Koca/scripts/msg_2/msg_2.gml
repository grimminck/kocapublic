/// @description  @function msg(text)
/// @param text
/// @param value
/// @description uses show_debug_message to send a text, a shorter way to use the function

show_debug_message(argument0 + ": "+string(argument1))
