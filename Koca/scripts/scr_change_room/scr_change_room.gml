/// @param room
/// @param seconds_to_change

var rmIndex = argument0
var _timer = argument1

with instance_create_layer(0, 0, "Instances", oBlackScreen)
{
	destinationRoom = rmIndex
	if destinationRoom == rmEnd game_end()
	timer = 120 - _timer*60
}