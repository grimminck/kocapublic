/*

// Cambio de resolución aplicación android
usar display_set_gui_size al cambiar de resolución la aplicación para que todo se mantenga igual...
* Si no usa GUI no es necesario

// Como saber qué plataforma uso?
os_type: Contiene el valor de la plataforma para el que el juego se creó y se compiló
os_type = os_windows, os_android, os_macosx, os_ios (ipad, ipod), os_ps3, os_ps4, os_xboxone, etc...

// Depth for GUI* objects
GUI = -1		// The first GUI
GUI2 = -2		// GUI to interact with the first (Animations)
second GUI = -3	// GUI of panels that go on top of the app (Not stackable panels)
second GUI = -4	// GUI of buttons on panels
third GUI = -5	// Buttons of confirmation
commands = -6	// oControllerCommands
* GUI: Graphical User Interface is considered when it's not just words and buttons, but also symbols, art, color combinations and such

// Notifica al usuario android o IOS de algo
push_local_notification()

// Recibir un arreglo de argumentos ordenados
var iOffset = 1 // Initial offset (Number of arguments to "leap")
var args, i=0 repeat(argument_count-iOffset) { args[i] = argument[i+iOffset];  i++ }

*/
