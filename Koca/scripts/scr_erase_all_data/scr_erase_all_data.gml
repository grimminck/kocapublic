msg("ERASING ALL DATA - Version: "+string(appVersion)+". Last update on memory system: "+gLastUpdate)


file_delete("client_save_information.txt")
file_delete("client_local_tickets_list.txt")



file_delete("server_save_information.txt")
file_delete("server_accounts_registered.txt")

file_delete("server_tickets_sb.txt")
file_delete("server_tickets_fasa.txt")
file_delete("server_tickets_cv.txt")
file_delete("server_tickets_sb_unavailable.txt")
file_delete("server_tickets_fasa_unavailable.txt")
file_delete("server_tickets_cv_unavailable.txt")

msg("DELETED ALL FILES")