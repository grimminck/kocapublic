// Fecha

globalvar gMinuto;		gMinuto = floor(date_get_minute(date_current_datetime()))
globalvar gHora;		gHora = floor(date_get_hour(date_current_datetime()))
globalvar gDia;			gDia = floor(date_get_day(date_current_datetime()))
globalvar gMes;			gMes = floor(date_get_month(date_current_datetime()))
globalvar gAnno;		gAnno = floor(date_get_year(date_current_datetime()))

globalvar disconnectedAccount;	disconnectedAccount = false

if client() 
{
	globalvar masterAccount;		masterAccount = false

	globalvar ofreciendoTurnos;		ofreciendoTurnos = false
	globalvar ofrecerDia;			ofrecerDia = 0
	globalvar ofrecerMes;			ofrecerMes = 0
	globalvar ofrecerAnno;			ofrecerAnno = 0
	globalvar ofrecerHoraInicial;	ofrecerHoraInicial = 0
	globalvar ofrecerMinutoInicial;	ofrecerMinutoInicial = 0
	globalvar ofrecerHoraInicialAM;	ofrecerHoraInicialAM = false
	globalvar ofrecerHoraFinal;		ofrecerHoraFinal = 0
	globalvar ofrecerMinutoFinal;	ofrecerMinutoFinal = 0
	globalvar ofrecerHoraFinalAM;	ofrecerHoraFinalAM = false
	globalvar ofrecerMotivo;		ofrecerMotivo = ""
	
			{
				var d = date_current_datetime()
				ofrecerAnno = date_get_year(d)
				ofrecerDia = gDia
				ofrecerHoraInicialAM = false
				ofrecerHoraFinalAM = false
				ofrecerHoraFinal = date_get_hour(date_inc_hour(d, 6))
				if ofrecerHoraFinal > 12 { ofrecerHoraFinal-=12; ofrecerHoraFinalAM=true }
				ofrecerHoraInicial = date_get_hour(d)
				if ofrecerHoraInicial > 12 { ofrecerHoraInicial-=12; ofrecerHoraInicialAM=true }
				ofrecerMes = date_get_month(d)
				ofrecerMinutoFinal = 0
				ofrecerMinutoInicial = 0
				ofrecerMotivo = "Otro"
			}

	globalvar mouseLastX;			mouseLastX = 0
	globalvar mouseLastY;			mouseLastY = 0
	globalvar mouseLastPressedX;	mouseLastPressedX = 0
	globalvar mouseLastPressedY;	mouseLastPressedY = 0
	globalvar mouseLastReleasedX;	mouseLastReleasedX = 0
	globalvar mouseLastReleasedY;	mouseLastReleasedY = 0

	globalvar myList;

	globalvar gRememberLogin;		gRememberLogin = true

	globalvar lastTicketIdCreated;							// To tell server from where to receive tickets

	globalvar mesRueda;				mesRueda = scr_get_month_name(gMes)
	globalvar diaRueda;				diaRueda = gDia
	globalvar diaRuedaContador;		diaRuedaContador = gDia
	
	globalvar frontPanelActive;		frontPanelActive = false
}

else
{
	globalvar ticketIndex;	ticketIndex = 1
}

msg("Es el anno "+string(gAnno)+", "+string(gDia)+" de "+scr_get_month_name(gMes)+" a las "+string(gHora)+":"+string(gMinuto))