/// @param port
/// @param ip

var port = argument0
var ip = argument1

// Set configurations
network_set_config(network_config_connect_timeout, 5000)
network_set_config(network_config_use_non_blocking_socket, 1)

msg("Registered value: "+string(scr_client_load_data_general("registered")))
if scr_client_load_data_general("registered")
{
	msg("Account already registered, going right to the Hall")
	var _rut = scr_client_load_data_general("rut")
	var _clave = scr_client_load_data_general("password")
	var _nombre = scr_client_load_data_general("name")
	var _empresa = scr_client_load_data_general("firm")
	var _correo = scr_client_load_data_general("email")
	var _numTelefono = scr_client_load_data_general("phone number")
	var _numLocal = scr_client_load_data_general("shop id")
	var _dirLocal = scr_client_load_data_general("shop address")
	
	rut = _rut
	clave = _clave
	nombre = _nombre
	numeroTelefonico = _numTelefono
	correoElectronico = _correo
	empresa = _empresa
	scr_set_my_firm_and_rut()
	numeroLocal = _numLocal
	direccionLocal = _dirLocal
}
else
{
	msg("Account does not have automatic login option set")
	rut = ""
	clave = ""
	nombre = ""
	numeroTelefonico = ""
	correoElectronico = ""
	empresa = ""
	numeroLocal = ""
	direccionLocal = ""
}

// Create ping buffer
pingBuffer = buffer_create(1, buffer_fast, 1)
alarm[0] = room_speed*2
losingConnection = false
it_was_connected = false
disconnected = true
timer = 0
waitingForPing = false

// Create variables
globalvar Sock; Sock = -1
globalvar clientConnected;
globalvar clientConnecting;

// Get ip
globalvar chosenIp; chosenIp = ip
globalvar chosenPort; chosenPort = port
	
// Connect to the server
scr_network_connect()