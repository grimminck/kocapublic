/// @function scr_handle_packet
/// @param buffer
/// @param socket
/// @description Handle the packet received

// Get Buffer(Information packet) and socket(Transmission Tunnel)
var buffer = argument[0]
var socket = argument[1]

// Ping buffer
if buffer_get_size(buffer) <= 1
	scr_transmission_ping(true, buffer, socket)
	
// Normal buffer
else {
var command = buffer_read(buffer, buffer_u16)
switch(command)
{	
	// Receive registering packet
    case 0: scr_transmission_register(true, buffer, socket) break
	// Client Logs in
    case 1: scr_transmission_login(true, buffer, socket) break
	// Offer ticket transsmission
	case 2: scr_transmission_offer_ticket(true, buffer) break
	// Load previously created tickets
	case 3: scr_transmission_load_tickets(true, buffer, socket) break
	// Client asks to take a turn
	case 4: scr_transmission_ask_for_turn(true, buffer, socket) break
	// Client asks to take a turn
	case 5: scr_transmission_confirm_asked_turn(true, buffer, socket) break
	// Client pays for turn
	case 6: scr_transmission_pay_turn(true, buffer, socket) break
}
}