/// @param days

var days = argument0

// Aplicar reglas globales
// Dias con 31, 30, Febrero 28 y 29 en bisiestos
// Año bisiesto = cada cuatro años
var fDia = gDia
var fMes = gMes
var fAnno = gAnno

// Avanza un Dia y repite por numero de Dias
if days >= 1
{
	repeat(days)
	switch fMes {
		case 1: 
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 2: 
		
			// Caso Año bisiesto
			var k
			if fAnno mod 4==0 k=29
			else k=28
			if (fAnno mod 400!=0) and (fAnno mod 100==0) k=29
		
			if fDia==k { fDia=1 fMes+=1 }
			else fDia++
		
			break
		case 3: 
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 4: 
			if fDia==30 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 5: // Mayo
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 6: 
			if fDia==30 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 7: 
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 8: 
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 9: 
			if fDia==30 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 10: 
			if fDia==31 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 11: 
			if fDia==30 { fDia=1 fMes+=1 }
			else fDia++
			break
		case 12: 
			if fDia==31 {
				// Pasa de año
				fDia=1
				fMes=1
				fAnno++
			}
			else fDia++
			break
	}
}
// Going back on days
else if days <= -1
{
	repeat(abs(days))
	switch fMes {
		case 1:
			if fDia==1 
			{
				// Retrocede un año
				fDia=31
				fMes=12
				fAnno--
			}
			else fDia--
			break
		case 2:
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 3:
		
			// Caso Año bisiesto
			var k
			if fAnno mod 4==0 k=29
			else k=28
			if (fAnno mod 400!=0) and (fAnno mod 100==0) k=29
		
			if fDia==k { fDia=k fMes-=1 }
			else fDia--
		
			break
		case 4: 
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 5: // Mayo
			if fDia==1 { fDia=30 fMes-=1 }
			else fDia--
			break
		case 6: 
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 7: 
			if fDia==1 { fDia=30 fMes-=1 }
			else fDia--
			break
		case 8: // Agosto
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 9: 
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 10: 
			if fDia==1 { fDia=30 fMes-=1 }
			else fDia--
			break
		case 11: 
			if fDia==1 { fDia=31 fMes-=1 }
			else fDia--
			break
		case 12: 
			if fDia==1 { fDia=30 fMes-=1 }
			else fDia++
			break
	}
}

return date_create_datetime(fAnno, fMes, fDia, gHora, gMinuto, 0) // Crea datetime al final con todos los datos obtenidos