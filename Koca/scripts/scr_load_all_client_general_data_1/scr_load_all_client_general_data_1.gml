if client()
{
	var _buff = buffer_load("client_save_information.txt")
	var json = buffer_read(_buff, buffer_string)
	ds_map_copy(clientSaveMap, json_decode(json)) 

	buffer_delete(_buff)
	
	rut = scr_client_load_data_general("rut")
	clave = scr_client_load_data_general("password")
	nombre = scr_client_load_data_general("name")
	empresa = scr_client_load_data_general("firm")
	correoElectronico = scr_client_load_data_general("email")
	numeroTelefonico = scr_client_load_data_general("phone number")
	numeroLocal = scr_client_load_data_general("shop id")
	direccionLocal = scr_client_load_data_general("shop address")
	
	msg("Loaded data:")
	msg("rut: "+string(rut))
	msg("clave: "+string(clave))
	msg("nombre: "+string(nombre))
	msg("empresa: "+string(empresa))
	msg("correo: "+string(correoElectronico))
	msg("numTelefono: "+string(numeroTelefonico))
	msg("numLocal: "+string(numeroLocal))
	msg("dirLocal: "+string(direccionLocal))
	msg("Loaded data FINISH")
}