{
    "id": "4a481783-6012-496c-8b15-36ecdf12c448",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "GooglePlayServicesIAPExtension",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        "com.android.vending.BILLING"
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 834777342120174,
    "date": "2019-23-11 12:07:53",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "8e1a1689-272f-40f0-9022-61c4deb8baa4",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "GooglePlayServicesIAPExtension.ext",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 4,
            "order": [
                
            ],
            "origname": "extensions\\GooglePlayServicesIAPExtension.ext",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "com.yoyogames.googleplayservicesiap",
    "productID": "B71440EFE2AD842048FFF72164D464DD",
    "sourcedir": "",
    "supportedTargets": 834777342120174,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.2"
}