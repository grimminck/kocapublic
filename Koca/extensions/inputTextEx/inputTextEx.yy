{
    "id": "d8b500e1-e097-4716-8ddf-58333e93f447",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "inputTextEx",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "inputTextClass",
    "androidinject": "<activity android:name=\"${YYAndroidPackageName}.inputTextClass\" android:theme=\"@android:style\/Theme.Holo.Dialog\" \/>",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2019-19-22 05:05:40",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "fb382ae2-6239-4eb7-9cf0-0d61b8bfb5a5",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "inputTextEx.ext",
            "final": "",
            "functions": [
                {
                    "id": "29951167-acf6-4fd7-9407-e8d935f4dd6c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        1,
                        1
                    ],
                    "externalName": "showInputText",
                    "help": "showInputText(Str stringTitle, Str stringOK, Str stringCancel)",
                    "hidden": false,
                    "kind": 11,
                    "name": "showInputText",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 4,
            "order": [
                "29951167-acf6-4fd7-9407-e8d935f4dd6c"
            ],
            "origname": "extensions\\inputTextEx.ext",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "com.mattiafortunati.inputtext",
    "productID": "86868095F8488F28B85C64647F36D9CC",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}